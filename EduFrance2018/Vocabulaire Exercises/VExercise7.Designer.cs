﻿namespace EduFrance2018
{
    partial class VExercise7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise7));
            this.label4 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.Label44 = new System.Windows.Forms.Label();
            this.Label43 = new System.Windows.Forms.Label();
            this.Label42 = new System.Windows.Forms.Label();
            this.Label41 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.Label45 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.TextBox();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 20F, System.Drawing.FontStyle.Underline);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(245, 36);
            this.label4.TabIndex = 37;
            this.label4.Text = "Chassez l`intrus";
            // 
            // Label11
            // 
            this.Label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label11.AutoSize = true;
            this.Label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label11.ForeColor = System.Drawing.Color.Black;
            this.Label11.Location = new System.Drawing.Point(13, 80);
            this.Label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(61, 21);
            this.Label11.TabIndex = 38;
            this.Label11.Text = "un job";
            this.Label11.Click += new System.EventHandler(this.Label11_Click);
            this.Label11.MouseEnter += new System.EventHandler(this.Label11_MouseEnter);
            this.Label11.MouseLeave += new System.EventHandler(this.Label11_MouseLeave);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(72, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 21);
            this.label2.TabIndex = 39;
            this.label2.Text = "-";
            // 
            // Label12
            // 
            this.Label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label12.AutoSize = true;
            this.Label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label12.ForeColor = System.Drawing.Color.Black;
            this.Label12.Location = new System.Drawing.Point(85, 80);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(93, 21);
            this.Label12.TabIndex = 40;
            this.Label12.Text = "un emploi";
            this.Label12.Click += new System.EventHandler(this.Label12_Click);
            this.Label12.MouseEnter += new System.EventHandler(this.Label12_MouseEnter);
            this.Label12.MouseLeave += new System.EventHandler(this.Label12_MouseLeave);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(174, 80);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 21);
            this.label5.TabIndex = 41;
            this.label5.Text = "-";
            // 
            // Label13
            // 
            this.Label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label13.AutoSize = true;
            this.Label13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label13.ForeColor = System.Drawing.Color.Black;
            this.Label13.Location = new System.Drawing.Point(186, 80);
            this.Label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(88, 21);
            this.Label13.TabIndex = 42;
            this.Label13.Text = "un boulot";
            this.Label13.Click += new System.EventHandler(this.Label13_Click);
            this.Label13.MouseEnter += new System.EventHandler(this.Label13_MouseEnter);
            this.Label13.MouseLeave += new System.EventHandler(this.Label13_MouseLeave);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(273, 80);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 21);
            this.label7.TabIndex = 43;
            this.label7.Text = "-";
            // 
            // Label14
            // 
            this.Label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label14.AutoSize = true;
            this.Label14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label14.ForeColor = System.Drawing.Color.Black;
            this.Label14.Location = new System.Drawing.Point(287, 80);
            this.Label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(81, 21);
            this.Label14.TabIndex = 44;
            this.Label14.Text = "un stage";
            this.Label14.Click += new System.EventHandler(this.Label14_Click);
            this.Label14.MouseEnter += new System.EventHandler(this.Label14_MouseEnter);
            this.Label14.MouseLeave += new System.EventHandler(this.Label14_MouseLeave);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(364, 80);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 21);
            this.label9.TabIndex = 45;
            this.label9.Text = "-";
            // 
            // Label15
            // 
            this.Label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label15.AutoSize = true;
            this.Label15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label15.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label15.ForeColor = System.Drawing.Color.Black;
            this.Label15.Location = new System.Drawing.Point(377, 80);
            this.Label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(97, 21);
            this.Label15.TabIndex = 46;
            this.Label15.Text = "un travail";
            this.Label15.Click += new System.EventHandler(this.Label15_Click);
            this.Label15.MouseEnter += new System.EventHandler(this.Label15_MouseEnter);
            this.Label15.MouseLeave += new System.EventHandler(this.Label15_MouseLeave);
            // 
            // Label25
            // 
            this.Label25.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label25.AutoSize = true;
            this.Label25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label25.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label25.ForeColor = System.Drawing.Color.Black;
            this.Label25.Location = new System.Drawing.Point(466, 148);
            this.Label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(104, 21);
            this.Label25.TabIndex = 56;
            this.Label25.Text = "un retraité";
            this.Label25.Click += new System.EventHandler(this.Label25_Click);
            this.Label25.MouseEnter += new System.EventHandler(this.Label25_MouseEnter);
            this.Label25.MouseLeave += new System.EventHandler(this.Label25_MouseLeave);
            // 
            // Label24
            // 
            this.Label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label24.AutoSize = true;
            this.Label24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label24.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label24.ForeColor = System.Drawing.Color.Black;
            this.Label24.Location = new System.Drawing.Point(374, 148);
            this.Label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(77, 21);
            this.Label24.TabIndex = 54;
            this.Label24.Text = "un PDG";
            this.Label24.Click += new System.EventHandler(this.Label24_Click);
            this.Label24.MouseEnter += new System.EventHandler(this.Label24_MouseEnter);
            this.Label24.MouseLeave += new System.EventHandler(this.Label24_MouseLeave);
            // 
            // Label23
            // 
            this.Label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label23.AutoSize = true;
            this.Label23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label23.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label23.ForeColor = System.Drawing.Color.Black;
            this.Label23.Location = new System.Drawing.Point(272, 148);
            this.Label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(86, 21);
            this.Label23.TabIndex = 52;
            this.Label23.Text = "un cadre";
            this.Label23.Click += new System.EventHandler(this.Label23_Click);
            this.Label23.MouseEnter += new System.EventHandler(this.Label23_MouseEnter);
            this.Label23.MouseLeave += new System.EventHandler(this.Label23_MouseLeave);
            // 
            // Label22
            // 
            this.Label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label22.AutoSize = true;
            this.Label22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label22.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label22.ForeColor = System.Drawing.Color.Black;
            this.Label22.Location = new System.Drawing.Point(156, 148);
            this.Label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(95, 21);
            this.Label22.TabIndex = 50;
            this.Label22.Text = "un patron";
            this.Label22.Click += new System.EventHandler(this.Label22_Click);
            this.Label22.MouseEnter += new System.EventHandler(this.Label22_MouseEnter);
            this.Label22.MouseLeave += new System.EventHandler(this.Label22_MouseLeave);
            // 
            // Label21
            // 
            this.Label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label21.AutoSize = true;
            this.Label21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label21.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label21.ForeColor = System.Drawing.Color.Black;
            this.Label21.Location = new System.Drawing.Point(13, 148);
            this.Label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(127, 21);
            this.Label21.TabIndex = 48;
            this.Label21.Text = "un employeur";
            this.Label21.Click += new System.EventHandler(this.Label21_Click);
            this.Label21.MouseEnter += new System.EventHandler(this.Label21_MouseEnter);
            this.Label21.MouseLeave += new System.EventHandler(this.Label21_MouseLeave);
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(452, 148);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 21);
            this.label16.TabIndex = 55;
            this.label16.Text = "-";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(355, 148);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 21);
            this.label17.TabIndex = 53;
            this.label17.Text = "-";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label18.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(252, 148);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 21);
            this.label18.TabIndex = 51;
            this.label18.Text = "-";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label19.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(139, 148);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 21);
            this.label19.TabIndex = 49;
            this.label19.Text = "-";
            // 
            // Label34
            // 
            this.Label34.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label34.AutoSize = true;
            this.Label34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label34.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label34.ForeColor = System.Drawing.Color.Black;
            this.Label34.Location = new System.Drawing.Point(367, 220);
            this.Label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(144, 21);
            this.Label34.TabIndex = 64;
            this.Label34.Text = "un licenciement";
            this.Label34.Click += new System.EventHandler(this.Label34_Click);
            this.Label34.MouseEnter += new System.EventHandler(this.Label34_MouseEnter);
            this.Label34.MouseLeave += new System.EventHandler(this.Label34_MouseLeave);
            // 
            // Label33
            // 
            this.Label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label33.AutoSize = true;
            this.Label33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label33.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label33.ForeColor = System.Drawing.Color.Black;
            this.Label33.Location = new System.Drawing.Point(225, 213);
            this.Label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(129, 21);
            this.Label33.TabIndex = 62;
            this.Label33.Text = "une allocation";
            this.Label33.Click += new System.EventHandler(this.Label33_Click);
            this.Label33.MouseEnter += new System.EventHandler(this.Label33_MouseEnter);
            this.Label33.MouseLeave += new System.EventHandler(this.Label33_MouseLeave);
            // 
            // Label32
            // 
            this.Label32.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label32.AutoSize = true;
            this.Label32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label32.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label32.ForeColor = System.Drawing.Color.Black;
            this.Label32.Location = new System.Drawing.Point(119, 213);
            this.Label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(88, 21);
            this.Label32.TabIndex = 60;
            this.Label32.Text = "une fiche";
            this.Label32.Click += new System.EventHandler(this.Label32_Click);
            this.Label32.MouseEnter += new System.EventHandler(this.Label32_MouseEnter);
            this.Label32.MouseLeave += new System.EventHandler(this.Label32_MouseLeave);
            // 
            // Label31
            // 
            this.Label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label31.AutoSize = true;
            this.Label31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label31.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label31.ForeColor = System.Drawing.Color.Black;
            this.Label31.Location = new System.Drawing.Point(13, 220);
            this.Label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(95, 21);
            this.Label31.TabIndex = 58;
            this.Label31.Text = "un salaire";
            this.Label31.Click += new System.EventHandler(this.Label31_Click);
            this.Label31.MouseEnter += new System.EventHandler(this.Label31_MouseEnter);
            this.Label31.MouseLeave += new System.EventHandler(this.Label31_MouseLeave);
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label26.AutoSize = true;
            this.label26.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label26.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(351, 220);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 21);
            this.label26.TabIndex = 63;
            this.label26.Text = "-";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label27.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(208, 220);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 21);
            this.label27.TabIndex = 61;
            this.label27.Text = "-";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label28.AutoSize = true;
            this.label28.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label28.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(105, 221);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 21);
            this.label28.TabIndex = 59;
            this.label28.Text = "-";
            // 
            // Label44
            // 
            this.Label44.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label44.AutoSize = true;
            this.Label44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label44.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label44.ForeColor = System.Drawing.Color.Black;
            this.Label44.Location = new System.Drawing.Point(405, 295);
            this.Label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(83, 21);
            this.Label44.TabIndex = 74;
            this.Label44.Text = "un DRH";
            this.Label44.Click += new System.EventHandler(this.Label44_Click);
            this.Label44.MouseEnter += new System.EventHandler(this.Label44_MouseEnter);
            this.Label44.MouseLeave += new System.EventHandler(this.Label44_MouseLeave);
            // 
            // Label43
            // 
            this.Label43.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label43.AutoSize = true;
            this.Label43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label43.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label43.ForeColor = System.Drawing.Color.Black;
            this.Label43.Location = new System.Drawing.Point(268, 294);
            this.Label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(120, 21);
            this.Label43.TabIndex = 72;
            this.Label43.Text = "un recruteur";
            this.Label43.Click += new System.EventHandler(this.Label43_Click);
            this.Label43.MouseEnter += new System.EventHandler(this.Label43_MouseEnter);
            this.Label43.MouseLeave += new System.EventHandler(this.Label43_MouseLeave);
            // 
            // Label42
            // 
            this.Label42.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label42.AutoSize = true;
            this.Label42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label42.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label42.ForeColor = System.Drawing.Color.Black;
            this.Label42.Location = new System.Drawing.Point(139, 295);
            this.Label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(115, 21);
            this.Label42.TabIndex = 70;
            this.Label42.Text = "un entretien";
            this.Label42.Click += new System.EventHandler(this.Label42_Click);
            this.Label42.MouseEnter += new System.EventHandler(this.Label42_MouseEnter);
            this.Label42.MouseLeave += new System.EventHandler(this.Label42_MouseLeave);
            // 
            // Label41
            // 
            this.Label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label41.AutoSize = true;
            this.Label41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label41.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label41.ForeColor = System.Drawing.Color.Black;
            this.Label41.Location = new System.Drawing.Point(13, 285);
            this.Label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(111, 21);
            this.Label41.TabIndex = 68;
            this.Label41.Text = "une agencie";
            this.Label41.Click += new System.EventHandler(this.Label41_Click);
            this.Label41.MouseEnter += new System.EventHandler(this.Label41_MouseEnter);
            this.Label41.MouseLeave += new System.EventHandler(this.Label41_MouseLeave);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(388, 294);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 21);
            this.label10.TabIndex = 73;
            this.label10.Text = "-";
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label20.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(254, 294);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 21);
            this.label20.TabIndex = 71;
            this.label20.Text = "-";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label29.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(123, 295);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(15, 21);
            this.label29.TabIndex = 69;
            this.label29.Text = "-";
            // 
            // Label45
            // 
            this.Label45.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label45.AutoSize = true;
            this.Label45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label45.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label45.ForeColor = System.Drawing.Color.Black;
            this.Label45.Location = new System.Drawing.Point(506, 294);
            this.Label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label45.Name = "Label45";
            this.Label45.Size = new System.Drawing.Size(86, 21);
            this.Label45.TabIndex = 76;
            this.Label45.Text = "un BCBG";
            this.Label45.Click += new System.EventHandler(this.Label45_Click);
            this.Label45.MouseEnter += new System.EventHandler(this.Label45_MouseEnter);
            this.Label45.MouseLeave += new System.EventHandler(this.Label45_MouseLeave);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(491, 294);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 21);
            this.label1.TabIndex = 77;
            this.label1.Text = "-";
            // 
            // comboBox4
            // 
            this.comboBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox4.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox4.Location = new System.Drawing.Point(612, 288);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(166, 26);
            this.comboBox4.TabIndex = 78;
            // 
            // comboBox3
            // 
            this.comboBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox3.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox3.Location = new System.Drawing.Point(612, 213);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(166, 26);
            this.comboBox3.TabIndex = 79;
            // 
            // comboBox2
            // 
            this.comboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox2.Location = new System.Drawing.Point(612, 143);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(166, 26);
            this.comboBox2.TabIndex = 80;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox1.Location = new System.Drawing.Point(612, 75);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(166, 26);
            this.comboBox1.TabIndex = 81;
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(110, 379);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 84;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(342, 379);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 83;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(555, 379);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 82;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // VExercise7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.ClientSize = new System.Drawing.Size(790, 418);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.Label45);
            this.Controls.Add(this.Label44);
            this.Controls.Add(this.Label43);
            this.Controls.Add(this.Label42);
            this.Controls.Add(this.Label41);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Label34);
            this.Controls.Add(this.Label33);
            this.Controls.Add(this.Label32);
            this.Controls.Add(this.Label31);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.Label24);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.Label22);
            this.Controls.Add(this.Label21);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise7";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 7";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label Label15;
        private System.Windows.Forms.Label Label25;
        private System.Windows.Forms.Label Label24;
        private System.Windows.Forms.Label Label23;
        private System.Windows.Forms.Label Label22;
        private System.Windows.Forms.Label Label21;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label Label34;
        private System.Windows.Forms.Label Label33;
        private System.Windows.Forms.Label Label32;
        private System.Windows.Forms.Label Label31;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label Label44;
        private System.Windows.Forms.Label Label43;
        private System.Windows.Forms.Label Label42;
        private System.Windows.Forms.Label Label41;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label Label45;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox comboBox4;
        private System.Windows.Forms.TextBox comboBox3;
        private System.Windows.Forms.TextBox comboBox2;
        private System.Windows.Forms.TextBox comboBox1;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}