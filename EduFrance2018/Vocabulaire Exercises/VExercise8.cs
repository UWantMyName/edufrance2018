﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise8 : Form
    {
        public VExercise8()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void VExercise8_Load(object sender, EventArgs e)
        {
            Example.Image = System.Drawing.Image.FromFile("Exercise8image.jpg");
            Q4.Text += "\nintéressant ? utile ? drôle ? bizarre ? Pourquoi ?\nTrouvez un logo qui pourrait lui correspondre.";
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise9 f = new VExercise9();
            f.Show();
            this.Hide();
        }
    }
}
