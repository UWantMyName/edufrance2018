﻿namespace EduFrance2018
{
    partial class VExercise1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise1));
            this.Exercice1_Label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Q1ComboBox = new System.Windows.Forms.ComboBox();
            this.Back_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Q2ComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Q3ComboBox = new System.Windows.Forms.ComboBox();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.OpenDoc_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Exercice1_Label
            // 
            this.Exercice1_Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Exercice1_Label.AutoSize = true;
            this.Exercice1_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice1_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.Exercice1_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice1_Label.Location = new System.Drawing.Point(15, 26);
            this.Exercice1_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Exercice1_Label.Name = "Exercice1_Label";
            this.Exercice1_Label.Size = new System.Drawing.Size(570, 54);
            this.Exercice1_Label.TabIndex = 6;
            this.Exercice1_Label.Text = "Lisez attentivement le texte fourni et repondez aux \r\nquestions suivantes.";
            this.Exercice1_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 101);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 21);
            this.label1.TabIndex = 24;
            this.label1.Text = "Ce document est ";
            // 
            // Q1ComboBox
            // 
            this.Q1ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Q1ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q1ComboBox.FormattingEnabled = true;
            this.Q1ComboBox.Items.AddRange(new object[] {
            "un article de journal",
            "le résultat d\'un sondage",
            "une étude sociologique",
            "le résultat d\'un test"});
            this.Q1ComboBox.Location = new System.Drawing.Point(174, 101);
            this.Q1ComboBox.Name = "Q1ComboBox";
            this.Q1ComboBox.Size = new System.Drawing.Size(164, 25);
            this.Q1ComboBox.TabIndex = 25;
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.AutoSize = true;
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(480, 388);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 30);
            this.Back_Button.TabIndex = 26;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 158);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(239, 21);
            this.label2.TabIndex = 27;
            this.label2.Text = "On trouve ce questionnaire";
            // 
            // Q2ComboBox
            // 
            this.Q2ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Q2ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q2ComboBox.FormattingEnabled = true;
            this.Q2ComboBox.Items.AddRange(new object[] {
            "dans une revue spécialisée",
            "chez les sociologues",
            "chez les psychologues",
            "sur Internet"});
            this.Q2ComboBox.Location = new System.Drawing.Point(262, 158);
            this.Q2ComboBox.Name = "Q2ComboBox";
            this.Q2ComboBox.Size = new System.Drawing.Size(193, 25);
            this.Q2ComboBox.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 225);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(199, 21);
            this.label3.TabIndex = 29;
            this.label3.Text = "Ce document concerne";
            // 
            // Q3ComboBox
            // 
            this.Q3ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Q3ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q3ComboBox.FormattingEnabled = true;
            this.Q3ComboBox.Items.AddRange(new object[] {
            "seulement les hommes",
            "seulement les femmes",
            "les hommes et les femmes, jeunes et âgés"});
            this.Q3ComboBox.Location = new System.Drawing.Point(222, 225);
            this.Q3ComboBox.Name = "Q3ComboBox";
            this.Q3ComboBox.Size = new System.Drawing.Size(281, 25);
            this.Q3ComboBox.TabIndex = 30;
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.AutoSize = true;
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(497, 352);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 30);
            this.Check_Button.TabIndex = 32;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.AutoSize = true;
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(480, 316);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 30);
            this.Continue_Button.TabIndex = 33;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // OpenDoc_Button
            // 
            this.OpenDoc_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OpenDoc_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OpenDoc_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.OpenDoc_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.OpenDoc_Button.Location = new System.Drawing.Point(12, 356);
            this.OpenDoc_Button.Name = "OpenDoc_Button";
            this.OpenDoc_Button.Size = new System.Drawing.Size(93, 63);
            this.OpenDoc_Button.TabIndex = 23;
            this.OpenDoc_Button.Text = "Ouvres le texte";
            this.OpenDoc_Button.UseVisualStyleBackColor = true;
            this.OpenDoc_Button.Click += new System.EventHandler(this.OpenDoc_Button_Click);
            // 
            // VExercise1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(605, 431);
            this.Controls.Add(this.OpenDoc_Button);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Q3ComboBox);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Q2ComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Q1ComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Exercice1_Label);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(621, 470);
            this.Name = "VExercise1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Exercice1_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Q1ComboBox;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Q2ComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Q3ComboBox;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button OpenDoc_Button;
    }
}