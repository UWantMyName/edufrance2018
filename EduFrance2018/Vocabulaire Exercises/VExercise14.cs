﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise14 : Form
    {
        public VExercise14()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "Faux") MessageBox.Show("Il est un faute dans la première répondre.");
            else if (comboBox2.Text != "Faux") MessageBox.Show("Il est un faute dans la second répondre.");
            else if (comboBox3.Text != "Vrai") MessageBox.Show("Il est un faute dans la troisième répondre.");
            else if (comboBox4.Text != "Vrai") MessageBox.Show("Il est un faute dans la quatrième répondre.");
            else if (comboBox5.Text != "Faux") MessageBox.Show("Il est un faute dans la cinquième répondre.");
            else if (comboBox6.Text != "Vrai") MessageBox.Show("Il est un faute dans la sixième répondre.");
            else if (comboBox7.Text != "Vrai") MessageBox.Show("Il est un faute dans la septième répondre.");

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise15 f = new VExercise15();
            f.Show();
            this.Hide();
        }
    }
}
