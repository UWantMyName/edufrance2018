﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise10 : Form
    {
        public VExercise10()
        {
            InitializeComponent();
        }

        private void VExercise10_Load(object sender, EventArgs e)
        {
            Title.Text += "\ndans un essai de 100 à 120 mots.";
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise11 f = new VExercise11();
            f.Show();
            this.Hide();
        }
    }
}
