﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise1 : Form
    {
        public VExercise1()
        {
            InitializeComponent();
        }

        private void OpenDoc_Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Ex1Text.docx");
            }
            catch
            {
                MessageBox.Show("Cannot find file.");
            }
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (Q1ComboBox.Text == "le résultat d'un test") Q1ComboBox.BackColor = Color.LightGreen;
            else Q1ComboBox.BackColor = Color.Red;

            if (Q2ComboBox.Text == "sur Internet") Q2ComboBox.BackColor = Color.LightGreen;
            else Q2ComboBox.BackColor = Color.Red;

            if (Q3ComboBox.Text == "les hommes et les femmes, jeunes et âgés") Q3ComboBox.BackColor = Color.LightGreen;
            else Q3ComboBox.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise2 f = new VExercise2();
            f.Show();
            this.Hide();
        }
    }
}
