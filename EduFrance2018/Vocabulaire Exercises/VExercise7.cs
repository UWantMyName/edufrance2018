﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise7 : Form
    {
        public VExercise7()
        {
            InitializeComponent();
            Label32.Text += "\nde paye";
            Label33.Text += "\nde retraite";
            Label41.Text += "\nd'intérim";
        }

        #region Label11 Controls
        private void Label11_Click(object sender, EventArgs e)
        {
            comboBox1.Text = Label11.Text;
        }

        private void Label11_MouseEnter(object sender, EventArgs e)
        {
            Label11.ForeColor = Color.Blue;
            Label11.Font = new Font(Label11.Font, FontStyle.Bold);
        }

        private void Label11_MouseLeave(object sender, EventArgs e)
        {
            Label11.ForeColor = Color.Black;
            Label11.Font = new Font(Label11.Font, FontStyle.Regular);
        }
        #endregion

        #region Label12 Controls
        private void Label12_Click(object sender, EventArgs e)
        {
            comboBox1.Text = Label12.Text;
        }

        private void Label12_MouseEnter(object sender, EventArgs e)
        {
            Label12.ForeColor = Color.Blue;
            Label12.Font = new Font(Label12.Font, FontStyle.Bold);
        }

        private void Label12_MouseLeave(object sender, EventArgs e)
        {
            Label12.ForeColor = Color.Black;
            Label12.Font = new Font(Label12.Font, FontStyle.Regular);
        }

        #endregion

        #region Label13 Controls
        private void Label13_Click(object sender, EventArgs e)
        {
            comboBox1.Text = Label13.Text;
        }

        private void Label13_MouseEnter(object sender, EventArgs e)
        {
            Label13.ForeColor = Color.Blue;
            Label13.Font = new Font(Label13.Font, FontStyle.Bold);
        }

        private void Label13_MouseLeave(object sender, EventArgs e)
        {
            Label13.ForeColor = Color.Black;
            Label13.Font = new Font(Label13.Font, FontStyle.Regular);
        }
        #endregion

        #region Label14 Controls
        private void Label14_Click(object sender, EventArgs e)
        {
            comboBox1.Text = Label14.Text;
        }

        private void Label14_MouseEnter(object sender, EventArgs e)
        {
            Label14.ForeColor = Color.Blue;
            Label14.Font = new Font(Label14.Font, FontStyle.Bold);
        }

        private void Label14_MouseLeave(object sender, EventArgs e)
        {
            Label14.ForeColor = Color.Black;
            Label14.Font = new Font(Label14.Font, FontStyle.Regular);
        }
        #endregion

        #region Label15 Controls
        private void Label15_Click(object sender, EventArgs e)
        {
            comboBox1.Text = Label15.Text;
        }

        private void Label15_MouseEnter(object sender, EventArgs e)
        {
            Label15.ForeColor = Color.Blue;
            Label15.Font = new Font(Label15.Font, FontStyle.Bold);
        }

        private void Label15_MouseLeave(object sender, EventArgs e)
        {
            Label15.ForeColor = Color.Black;
            Label15.Font = new Font(Label15.Font, FontStyle.Regular);
        }
        #endregion

        #region Label21 Controls
        private void Label21_Click(object sender, EventArgs e)
        {
            comboBox2.Text = Label21.Text;
        }

        private void Label21_MouseEnter(object sender, EventArgs e)
        {
            Label21.ForeColor = Color.Blue;
            Label21.Font = new Font(Label21.Font, FontStyle.Bold);
        }

        private void Label21_MouseLeave(object sender, EventArgs e)
        {
            Label21.ForeColor = Color.Black;
            Label21.Font = new Font(Label21.Font, FontStyle.Regular);
        }
        #endregion

        #region Label22 Controls
        private void Label22_Click(object sender, EventArgs e)
        {
            comboBox2.Text = Label22.Text;
        }

        private void Label22_MouseEnter(object sender, EventArgs e)
        {
            Label22.ForeColor = Color.Blue;
            Label22.Font = new Font(Label22.Font, FontStyle.Bold);
        }

        private void Label22_MouseLeave(object sender, EventArgs e)
        {
            Label22.ForeColor = Color.Black;
            Label22.Font = new Font(Label22.Font, FontStyle.Regular);
        }
        #endregion

        #region Label23 Controls
        private void Label23_Click(object sender, EventArgs e)
        {
            comboBox2.Text = Label23.Text;
        }

        private void Label23_MouseEnter(object sender, EventArgs e)
        {
            Label23.ForeColor = Color.Blue;
            Label23.Font = new Font(Label23.Font, FontStyle.Bold);
        }

        private void Label23_MouseLeave(object sender, EventArgs e)
        {
            Label23.ForeColor = Color.Black;
            Label23.Font = new Font(Label23.Font, FontStyle.Regular);
        }
        #endregion

        #region Label24 Controls
        private void Label24_Click(object sender, EventArgs e)
        {
            comboBox2.Text = Label24.Text;
        }

        private void Label24_MouseEnter(object sender, EventArgs e)
        {
            Label24.ForeColor = Color.Blue;
            Label24.Font = new Font(Label24.Font, FontStyle.Bold);
        }

        private void Label24_MouseLeave(object sender, EventArgs e)
        {
            Label24.ForeColor = Color.Black;
            Label24.Font = new Font(Label24.Font, FontStyle.Regular);
        }
        #endregion

        #region Label25 Controls
        private void Label25_Click(object sender, EventArgs e)
        {
            comboBox2.Text = Label25.Text;
        }

        private void Label25_MouseEnter(object sender, EventArgs e)
        {
            Label25.ForeColor = Color.Blue;
            Label25.Font = new Font(Label24.Font, FontStyle.Bold);
        }

        private void Label25_MouseLeave(object sender, EventArgs e)
        {
            Label25.ForeColor = Color.Black;
            Label25.Font = new Font(Label25.Font, FontStyle.Regular);
        }
        #endregion

        #region Label31 Controls
        private void Label31_Click(object sender, EventArgs e)
        {
            comboBox3.Text = Label31.Text;
        }

        private void Label31_MouseEnter(object sender, EventArgs e)
        {
            Label31.ForeColor = Color.Blue;
            Label31.Font = new Font(Label31.Font, FontStyle.Bold);
        }

        private void Label31_MouseLeave(object sender, EventArgs e)
        {
            Label31.ForeColor = Color.Black;
            Label31.Font = new Font(Label31.Font, FontStyle.Regular);
        }
        #endregion

        #region Label32 Controls
        private void Label32_Click(object sender, EventArgs e)
        {
            comboBox3.Text = "une fiche de paye";
        }

        private void Label32_MouseEnter(object sender, EventArgs e)
        {
            Label32.ForeColor = Color.Blue;
            Label32.Font = new Font(Label32.Font, FontStyle.Bold);
        }

        private void Label32_MouseLeave(object sender, EventArgs e)
        {
            Label32.ForeColor = Color.Black;
            Label32.Font = new Font(Label32.Font, FontStyle.Regular);
        }
        #endregion

        #region Label33 Controls
        private void Label33_Click(object sender, EventArgs e)
        {
            comboBox3.Text = "une allocation de retraite";
        }

        private void Label33_MouseEnter(object sender, EventArgs e)
        {
            Label33.ForeColor = Color.Blue;
            Label33.Font = new Font(Label33.Font, FontStyle.Bold);
        }

        private void Label33_MouseLeave(object sender, EventArgs e)
        {
            Label33.ForeColor = Color.Black;
            Label33.Font = new Font(Label33.Font, FontStyle.Regular);
        }
        #endregion

        #region Label34 Controls
        private void Label34_Click(object sender, EventArgs e)
        {
            comboBox3.Text = Label34.Text;
        }

        private void Label34_MouseEnter(object sender, EventArgs e)
        {
            Label34.ForeColor = Color.Blue;
            Label34.Font = new Font(Label34.Font, FontStyle.Bold);
        }

        private void Label34_MouseLeave(object sender, EventArgs e)
        {
            Label34.ForeColor = Color.Black;
            Label34.Font = new Font(Label34.Font, FontStyle.Regular);
        }
        #endregion

        #region Label41 Controls
        private void Label41_Click(object sender, EventArgs e)
        {
            comboBox4.Text = "une agence d'intérim";
        }

        private void Label41_MouseEnter(object sender, EventArgs e)
        {
            Label41.ForeColor = Color.Blue;
            Label41.Font = new Font(Label41.Font, FontStyle.Bold);
        }

        private void Label41_MouseLeave(object sender, EventArgs e)
        {
            Label41.ForeColor = Color.Black;
            Label41.Font = new Font(Label41.Font, FontStyle.Regular);
        }
        #endregion

        #region Label42 Controls
        private void Label42_Click(object sender, EventArgs e)
        {
            comboBox4.Text = "un entretien d'embauche";
        }

        private void Label42_MouseEnter(object sender, EventArgs e)
        {
            Label42.ForeColor = Color.Blue;
            Label42.Font = new Font(Label42.Font, FontStyle.Bold);
        }

        private void Label42_MouseLeave(object sender, EventArgs e)
        {
            Label42.ForeColor = Color.Black;
            Label42.Font = new Font(Label42.Font, FontStyle.Regular);
        }
        #endregion

        #region Label43 Controls
        private void Label43_Click(object sender, EventArgs e)
        {
            comboBox4.Text = Label43.Text;
        }

        private void Label43_MouseEnter(object sender, EventArgs e)
        {
            Label43.ForeColor = Color.Blue;
            Label43.Font = new Font(Label43.Font, FontStyle.Bold);
        }

        private void Label43_MouseLeave(object sender, EventArgs e)
        {
            Label43.ForeColor = Color.Black;
            Label43.Font = new Font(Label43.Font, FontStyle.Regular);
        }
        #endregion

        #region Label44 Controls
        private void Label44_Click(object sender, EventArgs e)
        {
            comboBox4.Text = Label44.Text;
        }

        private void Label44_MouseEnter(object sender, EventArgs e)
        {
            Label44.ForeColor = Color.Blue;
            Label44.Font = new Font(Label44.Font, FontStyle.Bold);
        }

        private void Label44_MouseLeave(object sender, EventArgs e)
        {
            Label44.ForeColor = Color.Black;
            Label44.Font = new Font(Label41.Font, FontStyle.Regular);
        }
        #endregion

        #region Label45 Controls
        private void Label45_Click(object sender, EventArgs e)
        {
            comboBox4.Text = Label45.Text;
        }

        private void Label45_MouseEnter(object sender, EventArgs e)
        {
            Label45.ForeColor = Color.Blue;
            Label45.Font = new Font(Label45.Font, FontStyle.Bold);
        }

        private void Label45_MouseLeave(object sender, EventArgs e)
        {
            Label45.ForeColor = Color.Black;
            Label45.Font = new Font(Label45.Font, FontStyle.Regular);
        }
        #endregion

        #region Button Controls
        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != Label14.Text) comboBox1.BackColor = Color.Red;
            else comboBox1.BackColor = Color.Lime;

            if (comboBox2.Text != Label25.Text) comboBox2.BackColor = Color.Red;
            else comboBox2.BackColor = Color.Lime;

            if (comboBox3.Text != Label34.Text) comboBox3.BackColor = Color.Red;
            else comboBox3.BackColor = Color.Lime;

            if (comboBox4.Text != Label45.Text) comboBox4.BackColor = Color.Red;
            else comboBox4.BackColor = Color.Lime;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise8 f = new VExercise8();
            f.Show();
            this.Hide();
        }
        #endregion
    }
}
