﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise9 : Form
    {
        public VExercise9()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise10 f = new VExercise10();
            f.Show();
            this.Hide();
        }
    }
}
