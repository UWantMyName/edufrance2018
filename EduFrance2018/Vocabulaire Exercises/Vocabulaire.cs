﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class Vocabulaire : Form
    {
        public Vocabulaire()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            MainMenu_Form f = new MainMenu_Form();
            f.Show();
            this.Hide();
        }

        #region Ex1 Controls
        private void Exercice1_Label_Click(object sender, EventArgs e)
        {
            VExercise1 f = new VExercise1();
            f.Show();
            this.Hide();
        }

        private void Exercice1_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice1_Label.ForeColor = Color.Blue;
            Exercice1_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Bold);
        }

        private void Exercice1_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice1_Label.ForeColor = Color.Black;
            Exercice1_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex2 Controls
        private void Exercice2_Label_Click(object sender, EventArgs e)
        {
            VExercise2 f = new VExercise2();
            f.Show();
            this.Hide();
        }

        private void Exercice2_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice2_Label.ForeColor = Color.Blue;
            Exercice2_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Bold);
        }

        private void Exercice2_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice2_Label.ForeColor = Color.Black;
            Exercice2_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex3 Controls
        private void Exercice2prim_Click(object sender, EventArgs e)
        {
            VExercise3 f = new VExercise3();
            f.Show();
            this.Hide();
        }

        private void Exercice2prim_MouseEnter(object sender, EventArgs e)
        {
            Exercice2prim.ForeColor = Color.Blue;
            Exercice2prim.Font = new Font(Exercice1_Label.Font, FontStyle.Bold);
        }

        private void Exercice2prim_MouseLeave(object sender, EventArgs e)
        {
            Exercice2prim.ForeColor = Color.Black;
            Exercice2prim.Font = new Font(Exercice1_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex4 Controls
        private void Exercice3_Label_Click(object sender, EventArgs e)
        {
            VExercise4 f = new VExercise4();
            f.Show();
            this.Hide();
        }

        private void Exercice3_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice3_Label.ForeColor = Color.Blue;
            Exercice3_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Bold);
        }

        private void Exercice3_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice3_Label.ForeColor = Color.Black;
            Exercice3_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex5 Controls
        private void Exercise4_Label_Click(object sender, EventArgs e)
        {
            VExercise5 f = new VExercise5();
            f.Show();
            this.Hide();
        }

        private void Exercise4_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise4_Label.ForeColor = Color.Blue;
            Exercise4_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Bold);
        }

        private void Exercise4_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise4_Label.ForeColor = Color.Black;
            Exercise4_Label.Font = new Font(Exercice1_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex6 Controls
        private void Exercise5_Label_Click(object sender, EventArgs e)
        {
            VExercise6 f = new VExercise6();
            f.Show();
            this.Hide();
        }

        private void Exercise5_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise5_Label.ForeColor = Color.Blue;
            Exercise5_Label.Font = new Font(Exercise5_Label.Font, FontStyle.Bold);
        }

        private void Exercise5_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise5_Label.ForeColor = Color.Black;
            Exercise5_Label.Font = new Font(Exercise5_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex7 Controls
        private void Exercice7_Label_Click(object sender, EventArgs e)
        {
            VExercise7 f = new VExercise7();
            f.Show();
            this.Hide();
        }

        private void Exercice7_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice7_Label.ForeColor = Color.Blue;
            Exercice7_Label.Font = new Font(Exercice7_Label.Font, FontStyle.Bold);
        }

        private void Exercice7_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice7_Label.ForeColor = Color.Black;
            Exercice7_Label.Font = new Font(Exercice7_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex8 Controls
        private void Exercice8_Label_Click(object sender, EventArgs e)
        {
            VExercise8 f = new VExercise8();
            f.Show();
            this.Hide();
        }

        private void Exercice8_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice8_Label.ForeColor = Color.Blue;
            Exercice8_Label.Font = new Font(Exercice8_Label.Font, FontStyle.Bold);
        }

        private void Exercice8_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice8_Label.ForeColor = Color.Black;
            Exercice8_Label.Font = new Font(Exercice8_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex9 Controls
        private void Exercice9_Label_Click(object sender, EventArgs e)
        {
            VExercise9 f = new VExercise9();
            f.Show();
            this.Hide();
        }

        private void Exercice9_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice9_Label.ForeColor = Color.Blue;
            Exercice9_Label.Font = new Font(Exercice9_Label.Font, FontStyle.Bold);
        }

        private void Exercice9_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice9_Label.ForeColor = Color.Black;
            Exercice9_Label.Font = new Font(Exercice9_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex10 Controls
        private void Exercise10_Label_Click(object sender, EventArgs e)
        {
            VExercise10 f = new VExercise10();
            f.Show();
            this.Hide();
        }

        private void Exercise10_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice10_Label.ForeColor = Color.Blue;
            Exercice10_Label.Font = new Font(Exercice10_Label.Font, FontStyle.Bold);
        }

        private void Exercise10_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice10_Label.ForeColor = Color.Black;
            Exercice10_Label.Font = new Font(Exercice10_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex11 Controls
        private void Exercice11_Label_Click(object sender, EventArgs e)
        {
            VExercise11 f = new VExercise11();
            f.Show();
            this.Hide();
        }

        private void Exercice11_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice11_Label.ForeColor = Color.Blue;
            Exercice11_Label.Font = new Font(Exercice11_Label.Font, FontStyle.Bold);
        }

        private void Exercice11_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice11_Label.ForeColor = Color.Black;
            Exercice11_Label.Font = new Font(Exercice11_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex12 Controls
        private void Exercice12_Label_Click(object sender, EventArgs e)
        {
            VExercise12 f = new VExercise12();
            f.Show();
            this.Hide();
        }

        private void Exercice12_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice12_Label.ForeColor = Color.Blue;
            Exercice12_Label.Font = new Font(Exercice12_Label.Font, FontStyle.Bold);
        }

        private void Exercice12_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice12_Label.ForeColor = Color.Black;
            Exercice12_Label.Font = new Font(Exercice12_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex13 Controls
        private void Exercice13_Label_Click(object sender, EventArgs e)
        {
            VExercise13 f = new VExercise13();
            f.Show();
            this.Hide();
        }

        private void Exercice13_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice13_Label.ForeColor = Color.Blue;
            Exercice13_Label.Font = new Font(Exercice13_Label.Font, FontStyle.Bold);
        }

        private void Exercice13_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice13_Label.ForeColor = Color.Black;
            Exercice13_Label.Font = new Font(Exercice13_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex14 Controls
        private void Exercice14_Label_Click(object sender, EventArgs e)
        {
            VExercise14 f = new VExercise14();
            f.Show();
            this.Hide();
        }

        private void Exercice14_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice14_Label.ForeColor = Color.Blue;
            Exercice14_Label.Font = new Font(Exercice14_Label.Font, FontStyle.Bold);
        }

        private void Exercice14_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice14_Label.ForeColor = Color.Black;
            Exercice14_Label.Font = new Font(Exercice14_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex15 Controls
        private void Exercice15_Label_Click(object sender, EventArgs e)
        {
            VExercise15 f = new VExercise15();
            f.Show();
            this.Hide();
        }

        private void Exercice15_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice15_Label.ForeColor = Color.Blue;
            Exercice15_Label.Font = new Font(Exercice15_Label.Font, FontStyle.Bold);
        }

        private void Exercice15_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice15_Label.ForeColor = Color.Black;
            Exercice15_Label.Font = new Font(Exercice15_Label.Font, FontStyle.Regular);
        }
        #endregion

        #region Ex16 Controls
        private void Exercice16_Label_Click(object sender, EventArgs e)
        {
            VExercise16 f = new VExercise16();
            f.Show();
            this.Hide();
        }

        private void Exercice16_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercice16_Label.ForeColor = Color.Blue;
            Exercice16_Label.Font = new Font(Exercice16_Label.Font, FontStyle.Bold);
        }

        private void Exercice16_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercice16_Label.ForeColor = Color.Black;
            Exercice16_Label.Font = new Font(Exercice16_Label.Font, FontStyle.Regular);
        }
        #endregion
    }
}
