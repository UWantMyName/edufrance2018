﻿namespace EduFrance2018
{
    partial class VExercise2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise2));
            this.ILabel = new System.Windows.Forms.Label();
            this.IILabel = new System.Windows.Forms.Label();
            this.IIILabel = new System.Windows.Forms.Label();
            this.IVLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.VLabel = new System.Windows.Forms.Label();
            this.VILabel = new System.Windows.Forms.Label();
            this.VIILabel = new System.Windows.Forms.Label();
            this.VIIILabel = new System.Windows.Forms.Label();
            this.IXLabel = new System.Windows.Forms.Label();
            this.XLabel = new System.Windows.Forms.Label();
            this.XILabel = new System.Windows.Forms.Label();
            this.XIILabel = new System.Windows.Forms.Label();
            this.XIIILabel = new System.Windows.Forms.Label();
            this.XIVLabel = new System.Windows.Forms.Label();
            this.XVLabel = new System.Windows.Forms.Label();
            this.XVILabel = new System.Windows.Forms.Label();
            this.XVIILabel = new System.Windows.Forms.Label();
            this.XVIIILabel = new System.Windows.Forms.Label();
            this.BPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.CPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.DPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.EPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.FPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.GPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.HPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.OpenDoc_Button = new System.Windows.Forms.Button();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Undo_Button = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.APanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ILabel
            // 
            this.ILabel.AllowDrop = true;
            this.ILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ILabel.AutoSize = true;
            this.ILabel.BackColor = System.Drawing.Color.Aqua;
            this.ILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.ILabel.ForeColor = System.Drawing.Color.Black;
            this.ILabel.Location = new System.Drawing.Point(30, 318);
            this.ILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ILabel.Name = "ILabel";
            this.ILabel.Size = new System.Drawing.Size(102, 23);
            this.ILabel.TabIndex = 25;
            this.ILabel.Text = "journaliste";
            this.ILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ILabel_MouseDown);
            // 
            // IILabel
            // 
            this.IILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IILabel.AutoSize = true;
            this.IILabel.BackColor = System.Drawing.Color.Aqua;
            this.IILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.IILabel.ForeColor = System.Drawing.Color.Black;
            this.IILabel.Location = new System.Drawing.Point(817, 387);
            this.IILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IILabel.Name = "IILabel";
            this.IILabel.Size = new System.Drawing.Size(79, 23);
            this.IILabel.TabIndex = 26;
            this.IILabel.Text = "avocate";
            this.IILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.IILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IILabel_MouseDown);
            // 
            // IIILabel
            // 
            this.IIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IIILabel.AutoSize = true;
            this.IIILabel.BackColor = System.Drawing.Color.Aqua;
            this.IIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.IIILabel.ForeColor = System.Drawing.Color.Black;
            this.IIILabel.Location = new System.Drawing.Point(13, 452);
            this.IIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IIILabel.Name = "IIILabel";
            this.IIILabel.Size = new System.Drawing.Size(110, 44);
            this.IIILabel.TabIndex = 27;
            this.IIILabel.Text = "décoratrice\r\nd\'interieur";
            this.IIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.IIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IIILabel_MouseDown);
            // 
            // IVLabel
            // 
            this.IVLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IVLabel.AutoSize = true;
            this.IVLabel.BackColor = System.Drawing.Color.Aqua;
            this.IVLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IVLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IVLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.IVLabel.ForeColor = System.Drawing.Color.Black;
            this.IVLabel.Location = new System.Drawing.Point(64, 376);
            this.IVLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IVLabel.Name = "IVLabel";
            this.IVLabel.Size = new System.Drawing.Size(83, 23);
            this.IVLabel.TabIndex = 29;
            this.IVLabel.Text = "médecin";
            this.IVLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.IVLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IVLabel_MouseDown);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(34, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1083, 32);
            this.panel2.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 15F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(417, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(262, 27);
            this.label4.TabIndex = 32;
            this.label4.Text = "Professions choisies par";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(135, 113);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(104, 46);
            this.panel3.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(3, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 19);
            this.label7.TabIndex = 34;
            this.label7.Text = "Les hommes";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Location = new System.Drawing.Point(238, 113);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(168, 46);
            this.panel4.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(29, 24);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 19);
            this.label12.TabIndex = 36;
            this.label12.Text = "moins de 30 ans";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label8.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(4, 5);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 19);
            this.label8.TabIndex = 35;
            this.label8.Text = "Les jeunes de";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label9);
            this.panel5.Location = new System.Drawing.Point(405, 113);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(163, 46);
            this.panel5.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(4, 13);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 19);
            this.label9.TabIndex = 35;
            this.label9.Text = "Les moins de 30 ans";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label10);
            this.panel6.Location = new System.Drawing.Point(567, 113);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(152, 46);
            this.panel6.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(4, 13);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 19);
            this.label10.TabIndex = 35;
            this.label10.Text = "Les plus de 60 ans";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Location = new System.Drawing.Point(718, 113);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(154, 46);
            this.panel7.TabIndex = 38;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(35, 24);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 19);
            this.label14.TabIndex = 36;
            this.label14.Text = "de Normandie";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(4, 5);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 19);
            this.label11.TabIndex = 35;
            this.label11.Text = "Les habitants";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.label13);
            this.panel8.Location = new System.Drawing.Point(871, 113);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(123, 46);
            this.panel8.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label15.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(27, 24);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 19);
            this.label15.TabIndex = 36;
            this.label15.Text = "de Corse";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(4, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 19);
            this.label13.TabIndex = 35;
            this.label13.Text = "Les habitants";
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Location = new System.Drawing.Point(993, 113);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(124, 46);
            this.panel9.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(27, 24);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 19);
            this.label16.TabIndex = 36;
            this.label16.Text = "de Lorraine";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(4, 5);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 19);
            this.label17.TabIndex = 35;
            this.label17.Text = "Les habitants";
            // 
            // VLabel
            // 
            this.VLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.VLabel.AutoSize = true;
            this.VLabel.BackColor = System.Drawing.Color.Aqua;
            this.VLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.VLabel.ForeColor = System.Drawing.Color.Black;
            this.VLabel.Location = new System.Drawing.Point(597, 441);
            this.VLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VLabel.Name = "VLabel";
            this.VLabel.Size = new System.Drawing.Size(100, 44);
            this.VLabel.TabIndex = 42;
            this.VLabel.Text = "professeur\r\nde collège";
            this.VLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.VLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VLabel_MouseDown);
            // 
            // VILabel
            // 
            this.VILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.VILabel.AutoSize = true;
            this.VILabel.BackColor = System.Drawing.Color.Aqua;
            this.VILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.VILabel.ForeColor = System.Drawing.Color.Black;
            this.VILabel.Location = new System.Drawing.Point(859, 474);
            this.VILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VILabel.Name = "VILabel";
            this.VILabel.Size = new System.Drawing.Size(111, 44);
            this.VILabel.TabIndex = 43;
            this.VILabel.Text = "ingénieur\r\ncommercial";
            this.VILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.VILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VILabel_MouseDown);
            // 
            // VIILabel
            // 
            this.VIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.VIILabel.AutoSize = true;
            this.VIILabel.BackColor = System.Drawing.Color.Aqua;
            this.VIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.VIILabel.ForeColor = System.Drawing.Color.Black;
            this.VIILabel.Location = new System.Drawing.Point(342, 318);
            this.VIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VIILabel.Name = "VIILabel";
            this.VIILabel.Size = new System.Drawing.Size(111, 44);
            this.VIILabel.TabIndex = 44;
            this.VIILabel.Text = "attaché\r\ncommercial";
            this.VIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.VIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VIILabel_MouseDown);
            // 
            // VIIILabel
            // 
            this.VIIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.VIIILabel.AutoSize = true;
            this.VIIILabel.BackColor = System.Drawing.Color.Aqua;
            this.VIIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.VIIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VIIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.VIIILabel.ForeColor = System.Drawing.Color.Black;
            this.VIIILabel.Location = new System.Drawing.Point(187, 397);
            this.VIIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VIIILabel.Name = "VIIILabel";
            this.VIIILabel.Size = new System.Drawing.Size(109, 23);
            this.VIIILabel.TabIndex = 45;
            this.VIIILabel.Text = "vétérinaire";
            this.VIIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.VIIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VIIILabel_MouseDown);
            // 
            // IXLabel
            // 
            this.IXLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.IXLabel.AutoSize = true;
            this.IXLabel.BackColor = System.Drawing.Color.Aqua;
            this.IXLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IXLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IXLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.IXLabel.ForeColor = System.Drawing.Color.Black;
            this.IXLabel.Location = new System.Drawing.Point(960, 352);
            this.IXLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IXLabel.Name = "IXLabel";
            this.IXLabel.Size = new System.Drawing.Size(99, 23);
            this.IXLabel.TabIndex = 46;
            this.IXLabel.Text = "éducateur";
            this.IXLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.IXLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IXLabel_MouseDown);
            // 
            // XLabel
            // 
            this.XLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XLabel.AutoSize = true;
            this.XLabel.BackColor = System.Drawing.Color.Aqua;
            this.XLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XLabel.ForeColor = System.Drawing.Color.Black;
            this.XLabel.Location = new System.Drawing.Point(199, 331);
            this.XLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XLabel.Name = "XLabel";
            this.XLabel.Size = new System.Drawing.Size(85, 23);
            this.XLabel.TabIndex = 47;
            this.XLabel.Text = "pédiatre";
            this.XLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XLabel_MouseDown);
            // 
            // XILabel
            // 
            this.XILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XILabel.AutoSize = true;
            this.XILabel.BackColor = System.Drawing.Color.Aqua;
            this.XILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XILabel.ForeColor = System.Drawing.Color.Black;
            this.XILabel.Location = new System.Drawing.Point(187, 465);
            this.XILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XILabel.Name = "XILabel";
            this.XILabel.Size = new System.Drawing.Size(132, 44);
            this.XILabel.TabIndex = 48;
            this.XILabel.Text = "programmeur\r\ninformatique";
            this.XILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XILabel_MouseDown);
            // 
            // XIILabel
            // 
            this.XIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XIILabel.AutoSize = true;
            this.XIILabel.BackColor = System.Drawing.Color.Aqua;
            this.XIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XIILabel.ForeColor = System.Drawing.Color.Black;
            this.XIILabel.Location = new System.Drawing.Point(737, 465);
            this.XIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XIILabel.Name = "XIILabel";
            this.XIILabel.Size = new System.Drawing.Size(92, 44);
            this.XIILabel.TabIndex = 49;
            this.XIILabel.Text = "courtier\r\nen bourse";
            this.XIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XIILabel_MouseDown);
            // 
            // XIIILabel
            // 
            this.XIIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XIIILabel.AutoSize = true;
            this.XIIILabel.BackColor = System.Drawing.Color.Aqua;
            this.XIIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XIIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XIIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XIIILabel.ForeColor = System.Drawing.Color.Black;
            this.XIIILabel.Location = new System.Drawing.Point(361, 397);
            this.XIIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XIIILabel.Name = "XIIILabel";
            this.XIIILabel.Size = new System.Drawing.Size(178, 44);
            this.XIIILabel.TabIndex = 50;
            this.XIIILabel.Text = "directeur des\r\nresources humaines";
            this.XIIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XIIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XIIILabel_MouseDown);
            // 
            // XIVLabel
            // 
            this.XIVLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XIVLabel.AutoSize = true;
            this.XIVLabel.BackColor = System.Drawing.Color.Aqua;
            this.XIVLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XIVLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XIVLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XIVLabel.ForeColor = System.Drawing.Color.Black;
            this.XIVLabel.Location = new System.Drawing.Point(979, 408);
            this.XIVLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XIVLabel.Name = "XIVLabel";
            this.XIVLabel.Size = new System.Drawing.Size(110, 44);
            this.XIVLabel.TabIndex = 51;
            this.XIVLabel.Text = "responsable\r\ndes achats";
            this.XIVLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XIVLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XIVLabel_MouseDown);
            // 
            // XVLabel
            // 
            this.XVLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XVLabel.AutoSize = true;
            this.XVLabel.BackColor = System.Drawing.Color.Aqua;
            this.XVLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XVLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XVLabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XVLabel.ForeColor = System.Drawing.Color.Black;
            this.XVLabel.Location = new System.Drawing.Point(727, 331);
            this.XVLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XVLabel.Name = "XVLabel";
            this.XVLabel.Size = new System.Drawing.Size(116, 23);
            this.XVLabel.TabIndex = 52;
            this.XVLabel.Text = "pharmacien";
            this.XVLabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XVLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XVLabel_MouseDown);
            // 
            // XVILabel
            // 
            this.XVILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XVILabel.AutoSize = true;
            this.XVILabel.BackColor = System.Drawing.Color.Aqua;
            this.XVILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XVILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XVILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XVILabel.ForeColor = System.Drawing.Color.Black;
            this.XVILabel.Location = new System.Drawing.Point(643, 376);
            this.XVILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XVILabel.Name = "XVILabel";
            this.XVILabel.Size = new System.Drawing.Size(89, 23);
            this.XVILabel.TabIndex = 53;
            this.XVILabel.Text = "banquier";
            this.XVILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XVILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XVILabel_MouseDown);
            // 
            // XVIILabel
            // 
            this.XVIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XVIILabel.AutoSize = true;
            this.XVIILabel.BackColor = System.Drawing.Color.Aqua;
            this.XVIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XVIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XVIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XVIILabel.ForeColor = System.Drawing.Color.Black;
            this.XVIILabel.Location = new System.Drawing.Point(414, 474);
            this.XVIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XVIILabel.Name = "XVIILabel";
            this.XVIILabel.Size = new System.Drawing.Size(101, 44);
            this.XVIILabel.TabIndex = 54;
            this.XVIILabel.Text = "inspecteur\r\nde police";
            this.XVIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XVIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XVIILabel_MouseDown);
            // 
            // XVIIILabel
            // 
            this.XVIIILabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.XVIIILabel.AutoSize = true;
            this.XVIIILabel.BackColor = System.Drawing.Color.Aqua;
            this.XVIIILabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XVIIILabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.XVIIILabel.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.XVIIILabel.ForeColor = System.Drawing.Color.Black;
            this.XVIIILabel.Location = new System.Drawing.Point(555, 331);
            this.XVIIILabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.XVIIILabel.Name = "XVIIILabel";
            this.XVIIILabel.Size = new System.Drawing.Size(83, 23);
            this.XVIIILabel.TabIndex = 55;
            this.XVIIILabel.Text = "pompier";
            this.XVIIILabel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            this.XVIIILabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.XVIIILabel_MouseDown);
            // 
            // BPanel
            // 
            this.BPanel.AllowDrop = true;
            this.BPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.BPanel.BackColor = System.Drawing.SystemColors.Control;
            this.BPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BPanel.Location = new System.Drawing.Point(135, 158);
            this.BPanel.Name = "BPanel";
            this.BPanel.Size = new System.Drawing.Size(104, 139);
            this.BPanel.TabIndex = 58;
            this.BPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.BPanel_DragDrop);
            this.BPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // CPanel
            // 
            this.CPanel.AllowDrop = true;
            this.CPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CPanel.BackColor = System.Drawing.SystemColors.Control;
            this.CPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CPanel.Location = new System.Drawing.Point(238, 158);
            this.CPanel.Name = "CPanel";
            this.CPanel.Size = new System.Drawing.Size(168, 139);
            this.CPanel.TabIndex = 59;
            this.CPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.CPanel_DragDrop);
            this.CPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // DPanel
            // 
            this.DPanel.AllowDrop = true;
            this.DPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DPanel.BackColor = System.Drawing.SystemColors.Control;
            this.DPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DPanel.Location = new System.Drawing.Point(405, 158);
            this.DPanel.Name = "DPanel";
            this.DPanel.Size = new System.Drawing.Size(163, 139);
            this.DPanel.TabIndex = 60;
            this.DPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.DPanel_DragDrop);
            this.DPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // EPanel
            // 
            this.EPanel.AllowDrop = true;
            this.EPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EPanel.BackColor = System.Drawing.SystemColors.Control;
            this.EPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EPanel.Location = new System.Drawing.Point(567, 158);
            this.EPanel.Name = "EPanel";
            this.EPanel.Size = new System.Drawing.Size(152, 139);
            this.EPanel.TabIndex = 61;
            this.EPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.EPanel_DragDrop);
            this.EPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // FPanel
            // 
            this.FPanel.AllowDrop = true;
            this.FPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FPanel.BackColor = System.Drawing.SystemColors.Control;
            this.FPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FPanel.Location = new System.Drawing.Point(718, 158);
            this.FPanel.Name = "FPanel";
            this.FPanel.Size = new System.Drawing.Size(154, 139);
            this.FPanel.TabIndex = 62;
            this.FPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.FPanel_DragDrop);
            this.FPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // GPanel
            // 
            this.GPanel.AllowDrop = true;
            this.GPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.GPanel.BackColor = System.Drawing.SystemColors.Control;
            this.GPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GPanel.Location = new System.Drawing.Point(871, 158);
            this.GPanel.Name = "GPanel";
            this.GPanel.Size = new System.Drawing.Size(124, 139);
            this.GPanel.TabIndex = 63;
            this.GPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.GPanel_DragDrop);
            this.GPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // HPanel
            // 
            this.HPanel.AllowDrop = true;
            this.HPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.HPanel.BackColor = System.Drawing.SystemColors.Control;
            this.HPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPanel.Location = new System.Drawing.Point(993, 158);
            this.HPanel.Name = "HPanel";
            this.HPanel.Size = new System.Drawing.Size(124, 139);
            this.HPanel.TabIndex = 64;
            this.HPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.HPanel_DragDrop);
            this.HPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // OpenDoc_Button
            // 
            this.OpenDoc_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OpenDoc_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OpenDoc_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.OpenDoc_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.OpenDoc_Button.Location = new System.Drawing.Point(12, 571);
            this.OpenDoc_Button.Name = "OpenDoc_Button";
            this.OpenDoc_Button.Size = new System.Drawing.Size(97, 46);
            this.OpenDoc_Button.TabIndex = 65;
            this.OpenDoc_Button.Text = "Ouvres le texte";
            this.OpenDoc_Button.UseVisualStyleBackColor = true;
            this.OpenDoc_Button.Click += new System.EventHandler(this.OpenDoc_Button_Click);
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(1021, 524);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 68;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(1038, 557);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 67;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(1021, 590);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 66;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // Undo_Button
            // 
            this.Undo_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Undo_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Undo_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Undo_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Undo_Button.Location = new System.Drawing.Point(443, 571);
            this.Undo_Button.Name = "Undo_Button";
            this.Undo_Button.Size = new System.Drawing.Size(79, 46);
            this.Undo_Button.TabIndex = 69;
            this.Undo_Button.Text = "Undo";
            this.Undo_Button.UseVisualStyleBackColor = true;
            this.Undo_Button.Click += new System.EventHandler(this.Undo_Button_Click);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Title.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Location = new System.Drawing.Point(182, 9);
            this.Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(761, 54);
            this.Title.TabIndex = 33;
            this.Title.Text = "En utilisant le texte, complétez les tableaux avec les termes ci-dessous.\r\nClique" +
    "z et faites glisser les termes dans les tableaux.";
            this.Title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 19);
            this.label6.TabIndex = 33;
            this.label6.Text = "Les femmes";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(34, 113);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(102, 46);
            this.panel1.TabIndex = 30;
            // 
            // APanel
            // 
            this.APanel.AllowDrop = true;
            this.APanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.APanel.BackColor = System.Drawing.SystemColors.Control;
            this.APanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.APanel.Location = new System.Drawing.Point(34, 158);
            this.APanel.Name = "APanel";
            this.APanel.Size = new System.Drawing.Size(102, 139);
            this.APanel.TabIndex = 57;
            this.APanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.APanel_DragDrop);
            this.APanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterControl);
            // 
            // VExercise2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(1146, 629);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Undo_Button);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.OpenDoc_Button);
            this.Controls.Add(this.HPanel);
            this.Controls.Add(this.GPanel);
            this.Controls.Add(this.FPanel);
            this.Controls.Add(this.EPanel);
            this.Controls.Add(this.DPanel);
            this.Controls.Add(this.CPanel);
            this.Controls.Add(this.BPanel);
            this.Controls.Add(this.ILabel);
            this.Controls.Add(this.XVIIILabel);
            this.Controls.Add(this.XVIILabel);
            this.Controls.Add(this.XVILabel);
            this.Controls.Add(this.XVLabel);
            this.Controls.Add(this.XIVLabel);
            this.Controls.Add(this.XIIILabel);
            this.Controls.Add(this.XIILabel);
            this.Controls.Add(this.XILabel);
            this.Controls.Add(this.XLabel);
            this.Controls.Add(this.IXLabel);
            this.Controls.Add(this.VIIILabel);
            this.Controls.Add(this.VIILabel);
            this.Controls.Add(this.VILabel);
            this.Controls.Add(this.VLabel);
            this.Controls.Add(this.IVLabel);
            this.Controls.Add(this.IIILabel);
            this.Controls.Add(this.IILabel);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.APanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 2";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ILabel;
        private System.Windows.Forms.Label IILabel;
        private System.Windows.Forms.Label IIILabel;
        private System.Windows.Forms.Label IVLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label VLabel;
        private System.Windows.Forms.Label VILabel;
        private System.Windows.Forms.Label VIILabel;
        private System.Windows.Forms.Label VIIILabel;
        private System.Windows.Forms.Label IXLabel;
        private System.Windows.Forms.Label XLabel;
        private System.Windows.Forms.Label XILabel;
        private System.Windows.Forms.Label XIILabel;
        private System.Windows.Forms.Label XIIILabel;
        private System.Windows.Forms.Label XIVLabel;
        private System.Windows.Forms.Label XVLabel;
        private System.Windows.Forms.Label XVILabel;
        private System.Windows.Forms.Label XVIILabel;
        private System.Windows.Forms.Label XVIIILabel;
        private System.Windows.Forms.FlowLayoutPanel BPanel;
        private System.Windows.Forms.FlowLayoutPanel CPanel;
        private System.Windows.Forms.FlowLayoutPanel DPanel;
        private System.Windows.Forms.FlowLayoutPanel EPanel;
        private System.Windows.Forms.FlowLayoutPanel FPanel;
        private System.Windows.Forms.FlowLayoutPanel GPanel;
        private System.Windows.Forms.FlowLayoutPanel HPanel;
        private System.Windows.Forms.Button OpenDoc_Button;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button Undo_Button;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel APanel;
    }
}