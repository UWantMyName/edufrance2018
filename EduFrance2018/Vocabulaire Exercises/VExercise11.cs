﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise11 : Form
    {
        public VExercise11()
        {
            InitializeComponent();
        }

        private void VExercise11_Load(object sender, EventArgs e)
        {
            Example.Image = Image.FromFile("Ex11Image.jpg");
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (!comboBox1.Text.Contains("b)")) comboBox1.BackColor = Color.Red;
            else comboBox1.BackColor = Color.LightGreen;

            if (!comboBox2.Text.Contains("b)")) comboBox2.BackColor = Color.Red;
            else comboBox2.BackColor = Color.LightGreen;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise12 f = new VExercise12();
            f.Show();
            this.Hide();
        }
    }
}
