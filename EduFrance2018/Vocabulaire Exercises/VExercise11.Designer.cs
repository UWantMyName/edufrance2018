﻿namespace EduFrance2018
{
    partial class VExercise11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise11));
            this.Example = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Example)).BeginInit();
            this.SuspendLayout();
            // 
            // Example
            // 
            this.Example.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Example.Location = new System.Drawing.Point(447, 74);
            this.Example.Name = "Example";
            this.Example.Size = new System.Drawing.Size(341, 136);
            this.Example.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Example.TabIndex = 0;
            this.Example.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(744, 27);
            this.label4.TabIndex = 85;
            this.label4.Text = "Regardez attentivement l\'image suivante et répondez aux questions.";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "a) une affiche de film",
            "b) une publicité",
            "c) une carte postale"});
            this.comboBox1.Location = new System.Drawing.Point(18, 112);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(201, 25);
            this.comboBox1.TabIndex = 87;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 21);
            this.label1.TabIndex = 86;
            this.label1.Text = "Cette image est:";
            // 
            // comboBox2
            // 
            this.comboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "a) sur l\'importance des paysages",
            "b) sur le voyage comme moyen d\'évasion",
            "c) sur le tourisme actif"});
            this.comboBox2.Location = new System.Drawing.Point(18, 242);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(305, 25);
            this.comboBox2.TabIndex = 89;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 211);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 21);
            this.label2.TabIndex = 88;
            this.label2.Text = "Le code du message porte:";
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(675, 229);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 92;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(692, 262);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 91;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(675, 295);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 90;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // VExercise11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(800, 334);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Example);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise11";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 11";
            this.Load += new System.EventHandler(this.VExercise11_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Example)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Example;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}