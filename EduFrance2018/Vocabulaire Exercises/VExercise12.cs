﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise12 : Form
    {
        public VExercise12()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Contains("b)")) comboBox1.BackColor = Color.LightGreen;
            else comboBox1.BackColor = Color.Red;

            if (comboBox2.Text.Contains("b)")) comboBox2.BackColor = Color.LightGreen;
            else comboBox2.BackColor = Color.Red;

            if (comboBox3.Text.Contains("a)")) comboBox3.BackColor = Color.LightGreen;
            else comboBox3.BackColor = Color.Red;

            if (comboBox4.Text.Contains("c)")) comboBox4.BackColor = Color.LightGreen;
            else comboBox4.BackColor = Color.Red;

            if (comboBox5.Text.Contains("a)")) comboBox5.BackColor = Color.LightGreen;
            else comboBox5.BackColor = Color.Red;

            if (comboBox6.Text.Contains("a)")) comboBox6.BackColor = Color.LightGreen;
            else comboBox6.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise13 f = new VExercise13();
            f.Show();
            this.Hide();
        }
    }
}
