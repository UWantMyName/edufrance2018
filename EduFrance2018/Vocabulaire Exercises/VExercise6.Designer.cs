﻿namespace EduFrance2018
{
    partial class VExercise6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise6));
            this.label4 = new System.Windows.Forms.Label();
            this.L1 = new System.Windows.Forms.Label();
            this.L2 = new System.Windows.Forms.Label();
            this.L3 = new System.Windows.Forms.Label();
            this.L4 = new System.Windows.Forms.Label();
            this.L5 = new System.Windows.Forms.Label();
            this.L6 = new System.Windows.Forms.Label();
            this.L7 = new System.Windows.Forms.Label();
            this.L8 = new System.Windows.Forms.Label();
            this.L9 = new System.Windows.Forms.Label();
            this.L10 = new System.Windows.Forms.Label();
            this.L11 = new System.Windows.Forms.Label();
            this.L12 = new System.Windows.Forms.Label();
            this.L13 = new System.Windows.Forms.Label();
            this.L14 = new System.Windows.Forms.Label();
            this.L15 = new System.Windows.Forms.Label();
            this.L16 = new System.Windows.Forms.Label();
            this.L17 = new System.Windows.Forms.Label();
            this.L18 = new System.Windows.Forms.Label();
            this.L19 = new System.Windows.Forms.Label();
            this.L20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.R1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.R2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.R3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.R4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.R5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.R6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.R7 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.R8 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.R9 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.R10 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.R11 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.R12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.R13 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.R14 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.R15 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.R16 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.R17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.R18 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.R19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.R20 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.R21 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.R22 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.R23 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.R24 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.R25 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(237, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(815, 27);
            this.label4.TabIndex = 36;
            this.label4.Text = "Complétez le texte suivant avec ces termes et ces expressions thématiques : ";
            // 
            // L1
            // 
            this.L1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L1.AutoSize = true;
            this.L1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L1.ForeColor = System.Drawing.Color.Black;
            this.L1.Location = new System.Drawing.Point(64, 59);
            this.L1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(203, 19);
            this.L1.TabIndex = 52;
            this.L1.Text = "lettre de recommandation";
            this.L1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L1_MouseDown);
            // 
            // L2
            // 
            this.L2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L2.AutoSize = true;
            this.L2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L2.ForeColor = System.Drawing.Color.Black;
            this.L2.Location = new System.Drawing.Point(275, 59);
            this.L2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(75, 19);
            this.L2.TabIndex = 53;
            this.L2.Text = "mi-temps";
            this.L2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L2_MouseDown);
            // 
            // L3
            // 
            this.L3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L3.AutoSize = true;
            this.L3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L3.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L3.ForeColor = System.Drawing.Color.Black;
            this.L3.Location = new System.Drawing.Point(358, 59);
            this.L3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(98, 19);
            this.L3.TabIndex = 54;
            this.L3.Text = "retravailler";
            this.L3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L3_MouseDown);
            // 
            // L4
            // 
            this.L4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L4.AutoSize = true;
            this.L4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L4.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L4.ForeColor = System.Drawing.Color.Black;
            this.L4.Location = new System.Drawing.Point(464, 59);
            this.L4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(60, 19);
            this.L4.TabIndex = 55;
            this.L4.Text = "chinois";
            this.L4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L4_MouseDown);
            // 
            // L5
            // 
            this.L5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L5.AutoSize = true;
            this.L5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L5.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L5.ForeColor = System.Drawing.Color.Black;
            this.L5.Location = new System.Drawing.Point(562, 59);
            this.L5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L5.Name = "L5";
            this.L5.Size = new System.Drawing.Size(110, 19);
            this.L5.TabIndex = 56;
            this.L5.Text = "qualifications";
            this.L5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L5_MouseDown);
            // 
            // L6
            // 
            this.L6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L6.AutoSize = true;
            this.L6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L6.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L6.ForeColor = System.Drawing.Color.Black;
            this.L6.Location = new System.Drawing.Point(680, 59);
            this.L6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L6.Name = "L6";
            this.L6.Size = new System.Drawing.Size(79, 19);
            this.L6.TabIndex = 57;
            this.L6.Text = "employée";
            this.L6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L6_MouseDown);
            // 
            // L7
            // 
            this.L7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L7.AutoSize = true;
            this.L7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L7.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L7.ForeColor = System.Drawing.Color.Black;
            this.L7.Location = new System.Drawing.Point(767, 59);
            this.L7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L7.Name = "L7";
            this.L7.Size = new System.Drawing.Size(72, 19);
            this.L7.TabIndex = 58;
            this.L7.Text = "domaine";
            this.L7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L7_MouseDown);
            // 
            // L8
            // 
            this.L8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L8.AutoSize = true;
            this.L8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L8.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L8.ForeColor = System.Drawing.Color.Black;
            this.L8.Location = new System.Drawing.Point(847, 59);
            this.L8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L8.Name = "L8";
            this.L8.Size = new System.Drawing.Size(64, 19);
            this.L8.TabIndex = 59;
            this.L8.Text = "au pair";
            this.L8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L8_MouseDown);
            // 
            // L9
            // 
            this.L9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L9.AutoSize = true;
            this.L9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L9.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L9.ForeColor = System.Drawing.Color.Black;
            this.L9.Location = new System.Drawing.Point(938, 59);
            this.L9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L9.Name = "L9";
            this.L9.Size = new System.Drawing.Size(57, 19);
            this.L9.TabIndex = 60;
            this.L9.Text = "textile";
            this.L9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L9_MouseDown);
            // 
            // L10
            // 
            this.L10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L10.AutoSize = true;
            this.L10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L10.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L10.ForeColor = System.Drawing.Color.Black;
            this.L10.Location = new System.Drawing.Point(1003, 59);
            this.L10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L10.Name = "L10";
            this.L10.Size = new System.Drawing.Size(89, 19);
            this.L10.TabIndex = 61;
            this.L10.Text = "employeur";
            this.L10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L10_MouseDown);
            // 
            // L11
            // 
            this.L11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L11.AutoSize = true;
            this.L11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L11.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L11.ForeColor = System.Drawing.Color.Black;
            this.L11.Location = new System.Drawing.Point(1111, 59);
            this.L11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(45, 19);
            this.L11.TabIndex = 62;
            this.L11.Text = "fiche";
            this.L11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L11_MouseDown);
            // 
            // L12
            // 
            this.L12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L12.AutoSize = true;
            this.L12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L12.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L12.ForeColor = System.Drawing.Color.Black;
            this.L12.Location = new System.Drawing.Point(1164, 59);
            this.L12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(73, 19);
            this.L12.TabIndex = 63;
            this.L12.Text = "diplômes";
            this.L12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L12_MouseDown);
            // 
            // L13
            // 
            this.L13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L13.AutoSize = true;
            this.L13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L13.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L13.ForeColor = System.Drawing.Color.Black;
            this.L13.Location = new System.Drawing.Point(92, 90);
            this.L13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(85, 19);
            this.L13.TabIndex = 64;
            this.L13.Text = "entreprise";
            this.L13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L13_MouseDown);
            // 
            // L14
            // 
            this.L14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L14.AutoSize = true;
            this.L14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L14.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L14.ForeColor = System.Drawing.Color.Black;
            this.L14.Location = new System.Drawing.Point(223, 90);
            this.L14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(74, 19);
            this.L14.TabIndex = 65;
            this.L14.Text = "chômage";
            this.L14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L14_MouseDown);
            // 
            // L15
            // 
            this.L15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L15.AutoSize = true;
            this.L15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L15.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L15.ForeColor = System.Drawing.Color.Black;
            this.L15.Location = new System.Drawing.Point(342, 90);
            this.L15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L15.Name = "L15";
            this.L15.Size = new System.Drawing.Size(61, 19);
            this.L15.TabIndex = 66;
            this.L15.Text = "travail";
            this.L15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L15_MouseDown);
            // 
            // L16
            // 
            this.L16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L16.AutoSize = true;
            this.L16.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L16.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L16.ForeColor = System.Drawing.Color.Black;
            this.L16.Location = new System.Drawing.Point(450, 90);
            this.L16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L16.Name = "L16";
            this.L16.Size = new System.Drawing.Size(74, 19);
            this.L16.TabIndex = 67;
            this.L16.Text = "travaillé";
            this.L16.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L16_MouseDown);
            // 
            // L17
            // 
            this.L17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L17.AutoSize = true;
            this.L17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L17.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L17.ForeColor = System.Drawing.Color.Black;
            this.L17.Location = new System.Drawing.Point(566, 90);
            this.L17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L17.Name = "L17";
            this.L17.Size = new System.Drawing.Size(58, 19);
            this.L17.TabIndex = 68;
            this.L17.Text = "emploi";
            this.L17.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L17_MouseDown);
            // 
            // L18
            // 
            this.L18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L18.AutoSize = true;
            this.L18.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L18.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L18.ForeColor = System.Drawing.Color.Black;
            this.L18.Location = new System.Drawing.Point(663, 90);
            this.L18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L18.Name = "L18";
            this.L18.Size = new System.Drawing.Size(101, 19);
            this.L18.TabIndex = 69;
            this.L18.Text = "comptabilité";
            this.L18.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L18_MouseDown);
            // 
            // L19
            // 
            this.L19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L19.AutoSize = true;
            this.L19.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L19.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L19.ForeColor = System.Drawing.Color.Black;
            this.L19.Location = new System.Drawing.Point(804, 90);
            this.L19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L19.Name = "L19";
            this.L19.Size = new System.Drawing.Size(55, 19);
            this.L19.TabIndex = 70;
            this.L19.Text = "étudié";
            this.L19.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L19_MouseDown);
            // 
            // L20
            // 
            this.L20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L20.AutoSize = true;
            this.L20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L20.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L20.ForeColor = System.Drawing.Color.Black;
            this.L20.Location = new System.Drawing.Point(908, 90);
            this.L20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L20.Name = "L20";
            this.L20.Size = new System.Drawing.Size(170, 19);
            this.L20.TabIndex = 71;
            this.L20.Text = "entretien d\'embauche";
            this.L20.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L20_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 155);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(533, 21);
            this.label1.TabIndex = 72;
            this.label1.Text = "A: Bonjour, madame. Je suis actuellement à la recherche d\'un";
            // 
            // R1
            // 
            this.R1.AllowDrop = true;
            this.R1.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R1.Location = new System.Drawing.Point(540, 154);
            this.R1.Name = "R1";
            this.R1.Size = new System.Drawing.Size(132, 22);
            this.R1.TabIndex = 73;
            this.R1.DragDrop += new System.Windows.Forms.DragEventHandler(this.R1_DragDrop);
            this.R1.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(671, 155);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 21);
            this.label14.TabIndex = 78;
            this.label14.Text = ".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(688, 154);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(446, 21);
            this.label2.TabIndex = 79;
            this.label2.Text = "Mes enfants sont grands maintenant et je voudrais ";
            // 
            // R2
            // 
            this.R2.AllowDrop = true;
            this.R2.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R2.Location = new System.Drawing.Point(1129, 153);
            this.R2.Name = "R2";
            this.R2.Size = new System.Drawing.Size(132, 22);
            this.R2.TabIndex = 80;
            this.R2.DragDrop += new System.Windows.Forms.DragEventHandler(this.R2_DragDrop);
            this.R2.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(1268, 153);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 21);
            this.label3.TabIndex = 81;
            this.label3.Text = ".";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(6, 196);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 21);
            this.label5.TabIndex = 82;
            this.label5.Text = "B: Oui, dans quel";
            // 
            // R3
            // 
            this.R3.AllowDrop = true;
            this.R3.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R3.Location = new System.Drawing.Point(165, 195);
            this.R3.Name = "R3";
            this.R3.Size = new System.Drawing.Size(132, 22);
            this.R3.TabIndex = 83;
            this.R3.DragDrop += new System.Windows.Forms.DragEventHandler(this.R3_DragDrop);
            this.R3.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(300, 196);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(157, 21);
            this.label6.TabIndex = 84;
            this.label6.Text = "cherchez-vous un";
            // 
            // R4
            // 
            this.R4.AllowDrop = true;
            this.R4.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R4.Location = new System.Drawing.Point(464, 195);
            this.R4.Name = "R4";
            this.R4.Size = new System.Drawing.Size(132, 22);
            this.R4.TabIndex = 85;
            this.R4.DragDrop += new System.Windows.Forms.DragEventHandler(this.R4_DragDrop);
            this.R4.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(599, 196);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 21);
            this.label7.TabIndex = 86;
            this.label7.Text = "? Quelles sont vos";
            // 
            // R5
            // 
            this.R5.AllowDrop = true;
            this.R5.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R5.Location = new System.Drawing.Point(750, 195);
            this.R5.Name = "R5";
            this.R5.Size = new System.Drawing.Size(132, 22);
            this.R5.TabIndex = 87;
            this.R5.DragDrop += new System.Windows.Forms.DragEventHandler(this.R5_DragDrop);
            this.R5.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label8.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(886, 196);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 21);
            this.label8.TabIndex = 88;
            this.label8.Text = "?";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(6, 238);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(177, 21);
            this.label9.TabIndex = 89;
            this.label9.Text = "A: Après le bac, j\'ai";
            // 
            // R6
            // 
            this.R6.AllowDrop = true;
            this.R6.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R6.Location = new System.Drawing.Point(190, 237);
            this.R6.Name = "R6";
            this.R6.Size = new System.Drawing.Size(132, 22);
            this.R6.TabIndex = 90;
            this.R6.DragDrop += new System.Windows.Forms.DragEventHandler(this.R6_DragDrop);
            this.R6.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(329, 238);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 21);
            this.label10.TabIndex = 91;
            this.label10.Text = "la";
            // 
            // R7
            // 
            this.R7.AllowDrop = true;
            this.R7.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R7.Location = new System.Drawing.Point(362, 237);
            this.R7.Name = "R7";
            this.R7.Size = new System.Drawing.Size(132, 22);
            this.R7.TabIndex = 92;
            this.R7.DragDrop += new System.Windows.Forms.DragEventHandler(this.R7_DragDrop);
            this.R7.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(501, 238);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(775, 21);
            this.label11.TabIndex = 93;
            this.label11.Text = "et la gestion dans une école, pendant deux ans. Ensuite, j\'ai passé une année en " +
    "Angleterre";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(6, 280);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(156, 21);
            this.label12.TabIndex = 94;
            this.label12.Text = "comme jeune fille";
            // 
            // R8
            // 
            this.R8.AllowDrop = true;
            this.R8.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R8.Location = new System.Drawing.Point(169, 279);
            this.R8.Name = "R8";
            this.R8.Size = new System.Drawing.Size(132, 22);
            this.R8.TabIndex = 95;
            this.R8.DragDrop += new System.Windows.Forms.DragEventHandler(this.R8_DragDrop);
            this.R8.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(313, 280);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(438, 21);
            this.label13.TabIndex = 96;
            this.label13.Text = "dans une famille. Et au début de mon mariage j\'ai ";
            // 
            // R9
            // 
            this.R9.AllowDrop = true;
            this.R9.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R9.Location = new System.Drawing.Point(752, 279);
            this.R9.Name = "R9";
            this.R9.Size = new System.Drawing.Size(132, 22);
            this.R9.TabIndex = 97;
            this.R9.DragDrop += new System.Windows.Forms.DragEventHandler(this.R9_DragDrop);
            this.R9.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label15.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(896, 280);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(258, 21);
            this.label15.TabIndex = 98;
            this.label15.Text = "pendant quatre ans dans une";
            // 
            // R10
            // 
            this.R10.AllowDrop = true;
            this.R10.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R10.Location = new System.Drawing.Point(1161, 279);
            this.R10.Name = "R10";
            this.R10.Size = new System.Drawing.Size(132, 22);
            this.R10.TabIndex = 100;
            this.R10.DragDrop += new System.Windows.Forms.DragEventHandler(this.R10_DragDrop);
            this.R10.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(6, 321);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(241, 21);
            this.label17.TabIndex = 101;
            this.label17.Text = "d\'import-export, au service";
            // 
            // R11
            // 
            this.R11.AllowDrop = true;
            this.R11.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R11.Location = new System.Drawing.Point(254, 320);
            this.R11.Name = "R11";
            this.R11.Size = new System.Drawing.Size(132, 22);
            this.R11.TabIndex = 102;
            this.R11.DragDrop += new System.Windows.Forms.DragEventHandler(this.R11_DragDrop);
            this.R11.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label18.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(389, 321);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(211, 21);
            this.label18.TabIndex = 103;
            this.label18.Text = ". Je vous ai apporté une";
            // 
            // R12
            // 
            this.R12.AllowDrop = true;
            this.R12.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R12.Location = new System.Drawing.Point(607, 320);
            this.R12.Name = "R12";
            this.R12.Size = new System.Drawing.Size(132, 22);
            this.R12.TabIndex = 104;
            this.R12.DragDrop += new System.Windows.Forms.DragEventHandler(this.R12_DragDrop);
            this.R12.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label19.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(746, 321);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(138, 21);
            this.label19.TabIndex = 105;
            this.label19.Text = "de mon ancien ";
            // 
            // R13
            // 
            this.R13.AllowDrop = true;
            this.R13.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R13.Location = new System.Drawing.Point(890, 320);
            this.R13.Name = "R13";
            this.R13.Size = new System.Drawing.Size(132, 22);
            this.R13.TabIndex = 106;
            this.R13.DragDrop += new System.Windows.Forms.DragEventHandler(this.R13_DragDrop);
            this.R13.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label20.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(1029, 321);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 21);
            this.label20.TabIndex = 107;
            this.label20.Text = "et mes";
            // 
            // R14
            // 
            this.R14.AllowDrop = true;
            this.R14.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R14.Location = new System.Drawing.Point(1099, 320);
            this.R14.Name = "R14";
            this.R14.Size = new System.Drawing.Size(132, 22);
            this.R14.TabIndex = 108;
            this.R14.DragDrop += new System.Windows.Forms.DragEventHandler(this.R14_DragDrop);
            this.R14.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label21.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(1233, 321);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 21);
            this.label21.TabIndex = 109;
            this.label21.Text = ", tenez.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label22.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 401);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(190, 21);
            this.label22.TabIndex = 110;
            this.label22.Text = "actuellement, dans la";
            // 
            // R15
            // 
            this.R15.AllowDrop = true;
            this.R15.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R15.Location = new System.Drawing.Point(403, 359);
            this.R15.Name = "R15";
            this.R15.Size = new System.Drawing.Size(132, 22);
            this.R15.TabIndex = 111;
            this.R15.DragDrop += new System.Windows.Forms.DragEventHandler(this.R15_DragDrop);
            this.R15.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label23.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(542, 360);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(434, 21);
            this.label23.TabIndex = 112;
            this.label23.Text = ", j\'ai besoin de vos diplômes et de vos certificats de";
            // 
            // R16
            // 
            this.R16.AllowDrop = true;
            this.R16.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R16.Location = new System.Drawing.Point(973, 359);
            this.R16.Name = "R16";
            this.R16.Size = new System.Drawing.Size(132, 22);
            this.R16.TabIndex = 113;
            this.R16.DragDrop += new System.Windows.Forms.DragEventHandler(this.R16_DragDrop);
            this.R16.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label24.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(1112, 360);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(164, 21);
            this.label24.TabIndex = 114;
            this.label24.Text = ". Mais vous savez,";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label25.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(6, 360);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(390, 21);
            this.label25.TabIndex = 115;
            this.label25.Text = "B: Merci, madame. Vous devez remplir cette";
            // 
            // R17
            // 
            this.R17.AllowDrop = true;
            this.R17.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R17.Location = new System.Drawing.Point(203, 400);
            this.R17.Name = "R17";
            this.R17.Size = new System.Drawing.Size(132, 22);
            this.R17.TabIndex = 116;
            this.R17.DragDrop += new System.Windows.Forms.DragEventHandler(this.R17_DragDrop);
            this.R17.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label26.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(342, 402);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 21);
            this.label26.TabIndex = 117;
            this.label26.Text = ", il y a du";
            // 
            // R18
            // 
            this.R18.AllowDrop = true;
            this.R18.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R18.Location = new System.Drawing.Point(439, 400);
            this.R18.Name = "R18";
            this.R18.Size = new System.Drawing.Size(132, 22);
            this.R18.TabIndex = 118;
            this.R18.DragDrop += new System.Windows.Forms.DragEventHandler(this.R18_DragDrop);
            this.R18.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label27.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(576, 402);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 21);
            this.label27.TabIndex = 119;
            this.label27.Text = ".";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 440);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(380, 21);
            this.label16.TabIndex = 120;
            this.label16.Text = "(La femme remplit son dossier et le donne à ";
            // 
            // R19
            // 
            this.R19.AllowDrop = true;
            this.R19.BackColor = System.Drawing.SystemColors.Window;
            this.R19.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R19.Location = new System.Drawing.Point(392, 439);
            this.R19.Name = "R19";
            this.R19.Size = new System.Drawing.Size(132, 22);
            this.R19.TabIndex = 121;
            this.R19.DragDrop += new System.Windows.Forms.DragEventHandler(this.R19_DragDrop);
            this.R19.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label28.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(531, 440);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(114, 21);
            this.label28.TabIndex = 122;
            this.label28.Text = "de l`agence)";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label29.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(6, 476);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(260, 21);
            this.label29.TabIndex = 123;
            this.label29.Text = "B: Bon, vous parlez anglais et";
            // 
            // R20
            // 
            this.R20.AllowDrop = true;
            this.R20.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R20.Location = new System.Drawing.Point(273, 475);
            this.R20.Name = "R20";
            this.R20.Size = new System.Drawing.Size(132, 22);
            this.R20.TabIndex = 124;
            this.R20.DragDrop += new System.Windows.Forms.DragEventHandler(this.R20_DragDrop);
            this.R20.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label30.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(408, 476);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(18, 21);
            this.label30.TabIndex = 125;
            this.label30.Text = "?";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label31.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(6, 514);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(186, 21);
            this.label31.TabIndex = 126;
            this.label31.Text = "A: Oui, mon mari est";
            // 
            // R21
            // 
            this.R21.AllowDrop = true;
            this.R21.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R21.Location = new System.Drawing.Point(199, 513);
            this.R21.Name = "R21";
            this.R21.Size = new System.Drawing.Size(132, 22);
            this.R21.TabIndex = 127;
            this.R21.DragDrop += new System.Windows.Forms.DragEventHandler(this.R21_DragDrop);
            this.R21.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label32.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(338, 514);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(202, 21);
            this.label32.TabIndex = 128;
            this.label32.Text = ", je l\'ai appris avec lui.";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label33.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(10, 548);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(465, 21);
            this.label33.TabIndex = 129;
            this.label33.Text = "B: Écoutez, j\'ai peut-être quelque chose pour vous: une";
            // 
            // R22
            // 
            this.R22.AllowDrop = true;
            this.R22.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R22.Location = new System.Drawing.Point(482, 547);
            this.R22.Name = "R22";
            this.R22.Size = new System.Drawing.Size(132, 22);
            this.R22.TabIndex = 130;
            this.R22.DragDrop += new System.Windows.Forms.DragEventHandler(this.R22_DragDrop);
            this.R22.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label34.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(621, 548);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(30, 21);
            this.label34.TabIndex = 131;
            this.label34.Text = "de";
            // 
            // R23
            // 
            this.R23.AllowDrop = true;
            this.R23.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R23.Location = new System.Drawing.Point(658, 547);
            this.R23.Name = "R23";
            this.R23.Size = new System.Drawing.Size(132, 22);
            this.R23.TabIndex = 132;
            this.R23.DragDrop += new System.Windows.Forms.DragEventHandler(this.R23_DragDrop);
            this.R23.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label35.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(797, 548);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(436, 21);
            this.label35.TabIndex = 133;
            this.label35.Text = "vient d`ouvrir dans le quartier et ils cherchent un";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label36.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(13, 587);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(113, 21);
            this.label36.TabIndex = 134;
            this.label36.Text = "comptable à";
            // 
            // R24
            // 
            this.R24.AllowDrop = true;
            this.R24.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R24.Location = new System.Drawing.Point(133, 586);
            this.R24.Name = "R24";
            this.R24.Size = new System.Drawing.Size(132, 22);
            this.R24.TabIndex = 135;
            this.R24.DragDrop += new System.Windows.Forms.DragEventHandler(this.R24_DragDrop);
            this.R24.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label37.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(268, 587);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(417, 21);
            this.label37.TabIndex = 136;
            this.label37.Text = ". Ça vous intéresserait ? Car je pourrais fixer un";
            // 
            // R25
            // 
            this.R25.AllowDrop = true;
            this.R25.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R25.Location = new System.Drawing.Point(692, 586);
            this.R25.Name = "R25";
            this.R25.Size = new System.Drawing.Size(132, 22);
            this.R25.TabIndex = 137;
            this.R25.DragDrop += new System.Windows.Forms.DragEventHandler(this.R25_DragDrop);
            this.R25.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label38.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(826, 587);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(15, 21);
            this.label38.TabIndex = 138;
            this.label38.Text = ".";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label39.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(13, 624);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(331, 21);
            this.label39.TabIndex = 139;
            this.label39.Text = "A: Oui, c\'est parfait, je vous remercie.";
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.BackColor = System.Drawing.Color.Yellow;
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(956, 624);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 142;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = false;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.BackColor = System.Drawing.Color.Yellow;
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(1075, 624);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 141;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = false;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.BackColor = System.Drawing.Color.Yellow;
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(1177, 624);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 140;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = false;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // VExercise6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Peru;
            this.ClientSize = new System.Drawing.Size(1301, 660);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.R25);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.R24);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.R23);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.R22);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.R21);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.R20);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.R19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.R18);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.R17);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.R16);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.R15);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.R14);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.R13);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.R12);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.R11);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.R10);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.R9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.R8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.R7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.R6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.R5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.R4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.R3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.R2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.R1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.L20);
            this.Controls.Add(this.L19);
            this.Controls.Add(this.L18);
            this.Controls.Add(this.L17);
            this.Controls.Add(this.L16);
            this.Controls.Add(this.L15);
            this.Controls.Add(this.L14);
            this.Controls.Add(this.L13);
            this.Controls.Add(this.L12);
            this.Controls.Add(this.L11);
            this.Controls.Add(this.L10);
            this.Controls.Add(this.L9);
            this.Controls.Add(this.L8);
            this.Controls.Add(this.L7);
            this.Controls.Add(this.L6);
            this.Controls.Add(this.L5);
            this.Controls.Add(this.L4);
            this.Controls.Add(this.L3);
            this.Controls.Add(this.L2);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.label4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.Label L5;
        private System.Windows.Forms.Label L6;
        private System.Windows.Forms.Label L7;
        private System.Windows.Forms.Label L8;
        private System.Windows.Forms.Label L9;
        private System.Windows.Forms.Label L10;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.Label L15;
        private System.Windows.Forms.Label L16;
        private System.Windows.Forms.Label L17;
        private System.Windows.Forms.Label L18;
        private System.Windows.Forms.Label L19;
        private System.Windows.Forms.Label L20;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox R1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox R2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox R3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox R4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox R5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox R6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox R7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox R8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox R9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox R10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox R11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox R12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox R13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox R14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox R15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox R16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox R17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox R18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox R19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox R20;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox R21;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox R22;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox R23;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox R24;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox R25;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}