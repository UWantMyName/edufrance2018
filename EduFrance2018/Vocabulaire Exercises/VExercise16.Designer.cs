﻿namespace EduFrance2018
{
    partial class VExercise16
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise16));
            this.YesEmoji = new System.Windows.Forms.PictureBox();
            this.NoEmoji = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.R1 = new System.Windows.Forms.FlowLayoutPanel();
            this.R2 = new System.Windows.Forms.FlowLayoutPanel();
            this.R3 = new System.Windows.Forms.FlowLayoutPanel();
            this.R4 = new System.Windows.Forms.FlowLayoutPanel();
            this.R5 = new System.Windows.Forms.FlowLayoutPanel();
            this.R6 = new System.Windows.Forms.FlowLayoutPanel();
            this.R7 = new System.Windows.Forms.FlowLayoutPanel();
            this.R8 = new System.Windows.Forms.FlowLayoutPanel();
            this.R9 = new System.Windows.Forms.FlowLayoutPanel();
            this.R10 = new System.Windows.Forms.FlowLayoutPanel();
            this.R11 = new System.Windows.Forms.FlowLayoutPanel();
            this.R12 = new System.Windows.Forms.FlowLayoutPanel();
            this.R13 = new System.Windows.Forms.FlowLayoutPanel();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.YesEmoji)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoEmoji)).BeginInit();
            this.SuspendLayout();
            // 
            // YesEmoji
            // 
            this.YesEmoji.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.YesEmoji.BackColor = System.Drawing.SystemColors.Control;
            this.YesEmoji.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YesEmoji.Location = new System.Drawing.Point(831, 191);
            this.YesEmoji.Name = "YesEmoji";
            this.YesEmoji.Size = new System.Drawing.Size(50, 50);
            this.YesEmoji.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YesEmoji.TabIndex = 0;
            this.YesEmoji.TabStop = false;
            this.YesEmoji.Tag = "YesEmoji";
            this.YesEmoji.MouseDown += new System.Windows.Forms.MouseEventHandler(this.YesEmoji_MouseDown);
            // 
            // NoEmoji
            // 
            this.NoEmoji.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.NoEmoji.BackColor = System.Drawing.SystemColors.Control;
            this.NoEmoji.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NoEmoji.ErrorImage = null;
            this.NoEmoji.Location = new System.Drawing.Point(831, 101);
            this.NoEmoji.Name = "NoEmoji";
            this.NoEmoji.Size = new System.Drawing.Size(50, 50);
            this.NoEmoji.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.NoEmoji.TabIndex = 1;
            this.NoEmoji.TabStop = false;
            this.NoEmoji.Tag = "NoEmoji";
            this.NoEmoji.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NoEmoji_MouseDown);
            // 
            // Title
            // 
            this.Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Title.AutoSize = true;
            this.Title.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Title.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Location = new System.Drawing.Point(13, 9);
            this.Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(909, 81);
            this.Title.TabIndex = 34;
            this.Title.Text = resources.GetString("Title.Text");
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 130);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 21);
            this.label1.TabIndex = 35;
            this.label1.Text = "1) Ça t\'amincit.";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(14, 180);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 21);
            this.label2.TabIndex = 36;
            this.label2.Text = "2) C\'est moche.";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(14, 230);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 21);
            this.label3.TabIndex = 37;
            this.label3.Text = "3) C\'est banal.";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(14, 280);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 21);
            this.label4.TabIndex = 38;
            this.label4.Text = "4) C\'est démodé.";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(14, 330);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(230, 21);
            this.label5.TabIndex = 39;
            this.label5.Text = "5) Ça ne te va pas du tout.";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(14, 380);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 21);
            this.label6.TabIndex = 40;
            this.label6.Text = "6) C\'est affreux.";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(14, 430);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 21);
            this.label7.TabIndex = 41;
            this.label7.Text = "7) C\'est original.";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(465, 380);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 21);
            this.label9.TabIndex = 47;
            this.label9.Text = "13) C\'est chic.";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(465, 330);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 21);
            this.label10.TabIndex = 46;
            this.label10.Text = "12) Ça fait vieux.";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(465, 280);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 21);
            this.label11.TabIndex = 45;
            this.label11.Text = "11) Ça te grossit.";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(465, 230);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(186, 21);
            this.label12.TabIndex = 44;
            this.label12.Text = "10) C\'est très élégant.";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(465, 180);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 21);
            this.label13.TabIndex = 43;
            this.label13.Text = "9) C\'est vulgaire.";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(465, 130);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(146, 21);
            this.label14.TabIndex = 42;
            this.label14.Text = "8) C\'est ringard.";
            // 
            // R1
            // 
            this.R1.AllowDrop = true;
            this.R1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R1.BackColor = System.Drawing.SystemColors.Control;
            this.R1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R1.Location = new System.Drawing.Point(264, 111);
            this.R1.Name = "R1";
            this.R1.Size = new System.Drawing.Size(40, 40);
            this.R1.TabIndex = 48;
            this.R1.DragDrop += new System.Windows.Forms.DragEventHandler(this.R1_DragDrop);
            this.R1.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R2
            // 
            this.R2.AllowDrop = true;
            this.R2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R2.BackColor = System.Drawing.SystemColors.Control;
            this.R2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R2.Location = new System.Drawing.Point(264, 163);
            this.R2.Name = "R2";
            this.R2.Size = new System.Drawing.Size(40, 40);
            this.R2.TabIndex = 49;
            this.R2.DragDrop += new System.Windows.Forms.DragEventHandler(this.R2_DragDrop);
            this.R2.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R3
            // 
            this.R3.AllowDrop = true;
            this.R3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R3.BackColor = System.Drawing.SystemColors.Control;
            this.R3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R3.Location = new System.Drawing.Point(264, 215);
            this.R3.Name = "R3";
            this.R3.Size = new System.Drawing.Size(40, 40);
            this.R3.TabIndex = 49;
            this.R3.DragDrop += new System.Windows.Forms.DragEventHandler(this.R3_DragDrop);
            this.R3.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R4
            // 
            this.R4.AllowDrop = true;
            this.R4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R4.BackColor = System.Drawing.SystemColors.Control;
            this.R4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R4.Location = new System.Drawing.Point(264, 268);
            this.R4.Name = "R4";
            this.R4.Size = new System.Drawing.Size(40, 40);
            this.R4.TabIndex = 49;
            this.R4.DragDrop += new System.Windows.Forms.DragEventHandler(this.R4_DragDrop);
            this.R4.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R5
            // 
            this.R5.AllowDrop = true;
            this.R5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R5.BackColor = System.Drawing.SystemColors.Control;
            this.R5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R5.Location = new System.Drawing.Point(264, 324);
            this.R5.Name = "R5";
            this.R5.Size = new System.Drawing.Size(40, 40);
            this.R5.TabIndex = 49;
            this.R5.DragDrop += new System.Windows.Forms.DragEventHandler(this.R5_DragDrop);
            this.R5.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R6
            // 
            this.R6.AllowDrop = true;
            this.R6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R6.BackColor = System.Drawing.SystemColors.Control;
            this.R6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R6.Location = new System.Drawing.Point(264, 370);
            this.R6.Name = "R6";
            this.R6.Size = new System.Drawing.Size(40, 40);
            this.R6.TabIndex = 49;
            this.R6.DragDrop += new System.Windows.Forms.DragEventHandler(this.R6_DragDrop);
            this.R6.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R7
            // 
            this.R7.AllowDrop = true;
            this.R7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R7.BackColor = System.Drawing.SystemColors.Control;
            this.R7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R7.Location = new System.Drawing.Point(264, 426);
            this.R7.Name = "R7";
            this.R7.Size = new System.Drawing.Size(40, 40);
            this.R7.TabIndex = 49;
            this.R7.DragDrop += new System.Windows.Forms.DragEventHandler(this.R7_DragDrop);
            this.R7.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R8
            // 
            this.R8.AllowDrop = true;
            this.R8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R8.BackColor = System.Drawing.SystemColors.Control;
            this.R8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R8.Location = new System.Drawing.Point(676, 116);
            this.R8.Name = "R8";
            this.R8.Size = new System.Drawing.Size(40, 40);
            this.R8.TabIndex = 50;
            this.R8.DragDrop += new System.Windows.Forms.DragEventHandler(this.R8_DragDrop);
            this.R8.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R9
            // 
            this.R9.AllowDrop = true;
            this.R9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R9.BackColor = System.Drawing.SystemColors.Control;
            this.R9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R9.Location = new System.Drawing.Point(676, 164);
            this.R9.Name = "R9";
            this.R9.Size = new System.Drawing.Size(40, 40);
            this.R9.TabIndex = 51;
            this.R9.DragDrop += new System.Windows.Forms.DragEventHandler(this.R9_DragDrop);
            this.R9.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R10
            // 
            this.R10.AllowDrop = true;
            this.R10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R10.BackColor = System.Drawing.SystemColors.Control;
            this.R10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R10.Location = new System.Drawing.Point(676, 214);
            this.R10.Name = "R10";
            this.R10.Size = new System.Drawing.Size(40, 40);
            this.R10.TabIndex = 51;
            this.R10.DragDrop += new System.Windows.Forms.DragEventHandler(this.R10_DragDrop);
            this.R10.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R11
            // 
            this.R11.AllowDrop = true;
            this.R11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R11.BackColor = System.Drawing.SystemColors.Control;
            this.R11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R11.Location = new System.Drawing.Point(676, 264);
            this.R11.Name = "R11";
            this.R11.Size = new System.Drawing.Size(40, 40);
            this.R11.TabIndex = 51;
            this.R11.DragDrop += new System.Windows.Forms.DragEventHandler(this.R11_DragDrop);
            this.R11.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R12
            // 
            this.R12.AllowDrop = true;
            this.R12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R12.BackColor = System.Drawing.SystemColors.Control;
            this.R12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R12.Location = new System.Drawing.Point(676, 317);
            this.R12.Name = "R12";
            this.R12.Size = new System.Drawing.Size(40, 40);
            this.R12.TabIndex = 51;
            this.R12.DragDrop += new System.Windows.Forms.DragEventHandler(this.R12_DragDrop);
            this.R12.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // R13
            // 
            this.R13.AllowDrop = true;
            this.R13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.R13.BackColor = System.Drawing.SystemColors.Control;
            this.R13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.R13.Location = new System.Drawing.Point(676, 371);
            this.R13.Name = "R13";
            this.R13.Size = new System.Drawing.Size(40, 40);
            this.R13.TabIndex = 51;
            this.R13.DragDrop += new System.Windows.Forms.DragEventHandler(this.R13_DragDrop);
            this.R13.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(824, 439);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 115;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(807, 472);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 114;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // VExercise16
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(932, 511);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.R13);
            this.Controls.Add(this.R12);
            this.Controls.Add(this.R11);
            this.Controls.Add(this.R10);
            this.Controls.Add(this.R9);
            this.Controls.Add(this.R8);
            this.Controls.Add(this.R7);
            this.Controls.Add(this.R6);
            this.Controls.Add(this.R5);
            this.Controls.Add(this.R4);
            this.Controls.Add(this.R3);
            this.Controls.Add(this.R2);
            this.Controls.Add(this.R1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.NoEmoji);
            this.Controls.Add(this.YesEmoji);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise16";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 16";
            this.Load += new System.EventHandler(this.VExercise16_Load);
            ((System.ComponentModel.ISupportInitialize)(this.YesEmoji)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoEmoji)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox YesEmoji;
        private System.Windows.Forms.PictureBox NoEmoji;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.FlowLayoutPanel R1;
        private System.Windows.Forms.FlowLayoutPanel R2;
        private System.Windows.Forms.FlowLayoutPanel R3;
        private System.Windows.Forms.FlowLayoutPanel R4;
        private System.Windows.Forms.FlowLayoutPanel R5;
        private System.Windows.Forms.FlowLayoutPanel R6;
        private System.Windows.Forms.FlowLayoutPanel R7;
        private System.Windows.Forms.FlowLayoutPanel R8;
        private System.Windows.Forms.FlowLayoutPanel R9;
        private System.Windows.Forms.FlowLayoutPanel R10;
        private System.Windows.Forms.FlowLayoutPanel R11;
        private System.Windows.Forms.FlowLayoutPanel R12;
        private System.Windows.Forms.FlowLayoutPanel R13;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}