﻿namespace EduFrance2018
{
    partial class Vocabulaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vocabulaire));
            this.Exercice1_Label = new System.Windows.Forms.Label();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Exercice2_Label = new System.Windows.Forms.Label();
            this.Exercice2prim = new System.Windows.Forms.Label();
            this.Exercice3_Label = new System.Windows.Forms.Label();
            this.Exercise4_Label = new System.Windows.Forms.Label();
            this.Exercise5_Label = new System.Windows.Forms.Label();
            this.Exercice7_Label = new System.Windows.Forms.Label();
            this.Exercice8_Label = new System.Windows.Forms.Label();
            this.Exercice9_Label = new System.Windows.Forms.Label();
            this.Exercice10_Label = new System.Windows.Forms.Label();
            this.Exercice11_Label = new System.Windows.Forms.Label();
            this.Exercice12_Label = new System.Windows.Forms.Label();
            this.Exercice13_Label = new System.Windows.Forms.Label();
            this.Exercice14_Label = new System.Windows.Forms.Label();
            this.Exercice15_Label = new System.Windows.Forms.Label();
            this.Exercice16_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Exercice1_Label
            // 
            this.Exercice1_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice1_Label.AutoSize = true;
            this.Exercice1_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice1_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice1_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice1_Label.Location = new System.Drawing.Point(34, 24);
            this.Exercice1_Label.Name = "Exercice1_Label";
            this.Exercice1_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice1_Label.TabIndex = 5;
            this.Exercice1_Label.Text = "Exercice 1";
            this.Exercice1_Label.Click += new System.EventHandler(this.Exercice1_Label_Click);
            this.Exercice1_Label.MouseEnter += new System.EventHandler(this.Exercice1_Label_MouseEnter);
            this.Exercice1_Label.MouseLeave += new System.EventHandler(this.Exercice1_Label_MouseLeave);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(360, 459);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 6;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // Exercice2_Label
            // 
            this.Exercice2_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice2_Label.AutoSize = true;
            this.Exercice2_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice2_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice2_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice2_Label.Location = new System.Drawing.Point(34, 78);
            this.Exercice2_Label.Name = "Exercice2_Label";
            this.Exercice2_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice2_Label.TabIndex = 7;
            this.Exercice2_Label.Text = "Exercice 2";
            this.Exercice2_Label.Click += new System.EventHandler(this.Exercice2_Label_Click);
            this.Exercice2_Label.MouseEnter += new System.EventHandler(this.Exercice2_Label_MouseEnter);
            this.Exercice2_Label.MouseLeave += new System.EventHandler(this.Exercice2_Label_MouseLeave);
            // 
            // Exercice2prim
            // 
            this.Exercice2prim.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice2prim.AutoSize = true;
            this.Exercice2prim.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice2prim.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice2prim.ForeColor = System.Drawing.Color.Black;
            this.Exercice2prim.Location = new System.Drawing.Point(34, 135);
            this.Exercice2prim.Name = "Exercice2prim";
            this.Exercice2prim.Size = new System.Drawing.Size(124, 27);
            this.Exercice2prim.TabIndex = 8;
            this.Exercice2prim.Text = "Exercice 3";
            this.Exercice2prim.Click += new System.EventHandler(this.Exercice2prim_Click);
            this.Exercice2prim.MouseEnter += new System.EventHandler(this.Exercice2prim_MouseEnter);
            this.Exercice2prim.MouseLeave += new System.EventHandler(this.Exercice2prim_MouseLeave);
            // 
            // Exercice3_Label
            // 
            this.Exercice3_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice3_Label.AutoSize = true;
            this.Exercice3_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice3_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice3_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice3_Label.Location = new System.Drawing.Point(36, 190);
            this.Exercice3_Label.Name = "Exercice3_Label";
            this.Exercice3_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice3_Label.TabIndex = 9;
            this.Exercice3_Label.Text = "Exercice 4";
            this.Exercice3_Label.Click += new System.EventHandler(this.Exercice3_Label_Click);
            this.Exercice3_Label.MouseEnter += new System.EventHandler(this.Exercice3_Label_MouseEnter);
            this.Exercice3_Label.MouseLeave += new System.EventHandler(this.Exercice3_Label_MouseLeave);
            // 
            // Exercise4_Label
            // 
            this.Exercise4_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercise4_Label.AutoSize = true;
            this.Exercise4_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercise4_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercise4_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercise4_Label.Location = new System.Drawing.Point(36, 243);
            this.Exercise4_Label.Name = "Exercise4_Label";
            this.Exercise4_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercise4_Label.TabIndex = 10;
            this.Exercise4_Label.Text = "Exercice 5";
            this.Exercise4_Label.Click += new System.EventHandler(this.Exercise4_Label_Click);
            this.Exercise4_Label.MouseEnter += new System.EventHandler(this.Exercise4_Label_MouseEnter);
            this.Exercise4_Label.MouseLeave += new System.EventHandler(this.Exercise4_Label_MouseLeave);
            // 
            // Exercise5_Label
            // 
            this.Exercise5_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercise5_Label.AutoSize = true;
            this.Exercise5_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercise5_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercise5_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercise5_Label.Location = new System.Drawing.Point(36, 297);
            this.Exercise5_Label.Name = "Exercise5_Label";
            this.Exercise5_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercise5_Label.TabIndex = 11;
            this.Exercise5_Label.Text = "Exercice 6";
            this.Exercise5_Label.Click += new System.EventHandler(this.Exercise5_Label_Click);
            this.Exercise5_Label.MouseEnter += new System.EventHandler(this.Exercise5_Label_MouseEnter);
            this.Exercise5_Label.MouseLeave += new System.EventHandler(this.Exercise5_Label_MouseLeave);
            // 
            // Exercice7_Label
            // 
            this.Exercice7_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice7_Label.AutoSize = true;
            this.Exercice7_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice7_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice7_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice7_Label.Location = new System.Drawing.Point(36, 349);
            this.Exercice7_Label.Name = "Exercice7_Label";
            this.Exercice7_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice7_Label.TabIndex = 12;
            this.Exercice7_Label.Text = "Exercice 7";
            this.Exercice7_Label.Click += new System.EventHandler(this.Exercice7_Label_Click);
            this.Exercice7_Label.MouseEnter += new System.EventHandler(this.Exercice7_Label_MouseEnter);
            this.Exercice7_Label.MouseLeave += new System.EventHandler(this.Exercice7_Label_MouseLeave);
            // 
            // Exercice8_Label
            // 
            this.Exercice8_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice8_Label.AutoSize = true;
            this.Exercice8_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice8_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice8_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice8_Label.Location = new System.Drawing.Point(37, 407);
            this.Exercice8_Label.Name = "Exercice8_Label";
            this.Exercice8_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice8_Label.TabIndex = 13;
            this.Exercice8_Label.Text = "Exercice 8";
            this.Exercice8_Label.Click += new System.EventHandler(this.Exercice8_Label_Click);
            this.Exercice8_Label.MouseEnter += new System.EventHandler(this.Exercice8_Label_MouseEnter);
            this.Exercice8_Label.MouseLeave += new System.EventHandler(this.Exercice8_Label_MouseLeave);
            // 
            // Exercice9_Label
            // 
            this.Exercice9_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice9_Label.AutoSize = true;
            this.Exercice9_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice9_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice9_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice9_Label.Location = new System.Drawing.Point(279, 24);
            this.Exercice9_Label.Name = "Exercice9_Label";
            this.Exercice9_Label.Size = new System.Drawing.Size(124, 27);
            this.Exercice9_Label.TabIndex = 14;
            this.Exercice9_Label.Text = "Exercice 9";
            this.Exercice9_Label.Click += new System.EventHandler(this.Exercice9_Label_Click);
            this.Exercice9_Label.MouseEnter += new System.EventHandler(this.Exercice9_Label_MouseEnter);
            this.Exercice9_Label.MouseLeave += new System.EventHandler(this.Exercice9_Label_MouseLeave);
            // 
            // Exercice10_Label
            // 
            this.Exercice10_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice10_Label.AutoSize = true;
            this.Exercice10_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice10_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice10_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice10_Label.Location = new System.Drawing.Point(276, 77);
            this.Exercice10_Label.Name = "Exercice10_Label";
            this.Exercice10_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice10_Label.TabIndex = 15;
            this.Exercice10_Label.Text = "Exercice 10";
            this.Exercice10_Label.Click += new System.EventHandler(this.Exercise10_Label_Click);
            this.Exercice10_Label.MouseEnter += new System.EventHandler(this.Exercise10_Label_MouseEnter);
            this.Exercice10_Label.MouseLeave += new System.EventHandler(this.Exercise10_Label_MouseLeave);
            // 
            // Exercice11_Label
            // 
            this.Exercice11_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice11_Label.AutoSize = true;
            this.Exercice11_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice11_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice11_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice11_Label.Location = new System.Drawing.Point(280, 131);
            this.Exercice11_Label.Name = "Exercice11_Label";
            this.Exercice11_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice11_Label.TabIndex = 16;
            this.Exercice11_Label.Text = "Exercice 11";
            this.Exercice11_Label.Click += new System.EventHandler(this.Exercice11_Label_Click);
            this.Exercice11_Label.MouseEnter += new System.EventHandler(this.Exercice11_Label_MouseEnter);
            this.Exercice11_Label.MouseLeave += new System.EventHandler(this.Exercice11_Label_MouseLeave);
            // 
            // Exercice12_Label
            // 
            this.Exercice12_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice12_Label.AutoSize = true;
            this.Exercice12_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice12_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice12_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice12_Label.Location = new System.Drawing.Point(276, 188);
            this.Exercice12_Label.Name = "Exercice12_Label";
            this.Exercice12_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice12_Label.TabIndex = 17;
            this.Exercice12_Label.Text = "Exercice 12";
            this.Exercice12_Label.Click += new System.EventHandler(this.Exercice12_Label_Click);
            this.Exercice12_Label.MouseEnter += new System.EventHandler(this.Exercice12_Label_MouseEnter);
            this.Exercice12_Label.MouseLeave += new System.EventHandler(this.Exercice12_Label_MouseLeave);
            // 
            // Exercice13_Label
            // 
            this.Exercice13_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice13_Label.AutoSize = true;
            this.Exercice13_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice13_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice13_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice13_Label.Location = new System.Drawing.Point(276, 243);
            this.Exercice13_Label.Name = "Exercice13_Label";
            this.Exercice13_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice13_Label.TabIndex = 18;
            this.Exercice13_Label.Text = "Exercice 13";
            this.Exercice13_Label.Click += new System.EventHandler(this.Exercice13_Label_Click);
            this.Exercice13_Label.MouseEnter += new System.EventHandler(this.Exercice13_Label_MouseEnter);
            this.Exercice13_Label.MouseLeave += new System.EventHandler(this.Exercice13_Label_MouseLeave);
            // 
            // Exercice14_Label
            // 
            this.Exercice14_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice14_Label.AutoSize = true;
            this.Exercice14_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice14_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice14_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice14_Label.Location = new System.Drawing.Point(276, 296);
            this.Exercice14_Label.Name = "Exercice14_Label";
            this.Exercice14_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice14_Label.TabIndex = 19;
            this.Exercice14_Label.Text = "Exercice 14";
            this.Exercice14_Label.Click += new System.EventHandler(this.Exercice14_Label_Click);
            this.Exercice14_Label.MouseEnter += new System.EventHandler(this.Exercice14_Label_MouseEnter);
            this.Exercice14_Label.MouseLeave += new System.EventHandler(this.Exercice14_Label_MouseLeave);
            // 
            // Exercice15_Label
            // 
            this.Exercice15_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice15_Label.AutoSize = true;
            this.Exercice15_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice15_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice15_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice15_Label.Location = new System.Drawing.Point(276, 350);
            this.Exercice15_Label.Name = "Exercice15_Label";
            this.Exercice15_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice15_Label.TabIndex = 20;
            this.Exercice15_Label.Text = "Exercice 15";
            this.Exercice15_Label.Click += new System.EventHandler(this.Exercice15_Label_Click);
            this.Exercice15_Label.MouseEnter += new System.EventHandler(this.Exercice15_Label_MouseEnter);
            this.Exercice15_Label.MouseLeave += new System.EventHandler(this.Exercice15_Label_MouseLeave);
            // 
            // Exercice16_Label
            // 
            this.Exercice16_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Exercice16_Label.AutoSize = true;
            this.Exercice16_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercice16_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15F);
            this.Exercice16_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercice16_Label.Location = new System.Drawing.Point(276, 402);
            this.Exercice16_Label.Name = "Exercice16_Label";
            this.Exercice16_Label.Size = new System.Drawing.Size(138, 27);
            this.Exercice16_Label.TabIndex = 21;
            this.Exercice16_Label.Text = "Exercice 16";
            this.Exercice16_Label.Click += new System.EventHandler(this.Exercice16_Label_Click);
            this.Exercice16_Label.MouseEnter += new System.EventHandler(this.Exercice16_Label_MouseEnter);
            this.Exercice16_Label.MouseLeave += new System.EventHandler(this.Exercice16_Label_MouseLeave);
            // 
            // Vocabulaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(485, 497);
            this.Controls.Add(this.Exercice16_Label);
            this.Controls.Add(this.Exercice15_Label);
            this.Controls.Add(this.Exercice14_Label);
            this.Controls.Add(this.Exercice13_Label);
            this.Controls.Add(this.Exercice12_Label);
            this.Controls.Add(this.Exercice11_Label);
            this.Controls.Add(this.Exercice10_Label);
            this.Controls.Add(this.Exercice9_Label);
            this.Controls.Add(this.Exercice8_Label);
            this.Controls.Add(this.Exercice7_Label);
            this.Controls.Add(this.Exercise5_Label);
            this.Controls.Add(this.Exercise4_Label);
            this.Controls.Add(this.Exercice3_Label);
            this.Controls.Add(this.Exercice2prim);
            this.Controls.Add(this.Exercice2_Label);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.Exercice1_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Vocabulaire";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercices de Vocabulaire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Exercice1_Label;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Label Exercice2_Label;
        private System.Windows.Forms.Label Exercice2prim;
        private System.Windows.Forms.Label Exercice3_Label;
        private System.Windows.Forms.Label Exercise4_Label;
        private System.Windows.Forms.Label Exercise5_Label;
        private System.Windows.Forms.Label Exercice7_Label;
        private System.Windows.Forms.Label Exercice8_Label;
        private System.Windows.Forms.Label Exercice9_Label;
        private System.Windows.Forms.Label Exercice10_Label;
        private System.Windows.Forms.Label Exercice11_Label;
        private System.Windows.Forms.Label Exercice12_Label;
        private System.Windows.Forms.Label Exercice13_Label;
        private System.Windows.Forms.Label Exercice14_Label;
        private System.Windows.Forms.Label Exercice15_Label;
        private System.Windows.Forms.Label Exercice16_Label;
    }
}