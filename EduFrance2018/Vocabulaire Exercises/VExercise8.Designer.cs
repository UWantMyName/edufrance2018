﻿namespace EduFrance2018
{
    partial class VExercise8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise8));
            this.Back_Button = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Example = new System.Windows.Forms.PictureBox();
            this.Q1 = new System.Windows.Forms.Label();
            this.Q2 = new System.Windows.Forms.Label();
            this.Q3 = new System.Windows.Forms.Label();
            this.Q4 = new System.Windows.Forms.Label();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Example)).BeginInit();
            this.SuspendLayout();
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(675, 411);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 83;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(744, 27);
            this.label4.TabIndex = 84;
            this.label4.Text = "Regardez attentivement l\'image suivante et répondez aux questions.";
            // 
            // Example
            // 
            this.Example.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Example.Location = new System.Drawing.Point(559, 64);
            this.Example.Name = "Example";
            this.Example.Size = new System.Drawing.Size(229, 296);
            this.Example.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Example.TabIndex = 85;
            this.Example.TabStop = false;
            // 
            // Q1
            // 
            this.Q1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Q1.AutoSize = true;
            this.Q1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Q1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q1.ForeColor = System.Drawing.Color.Black;
            this.Q1.Location = new System.Drawing.Point(14, 112);
            this.Q1.Name = "Q1";
            this.Q1.Size = new System.Drawing.Size(417, 19);
            this.Q1.TabIndex = 86;
            this.Q1.Text = "a) De quel type de document s\'agit-il ? D\'où est-il tiré ?";
            // 
            // Q2
            // 
            this.Q2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Q2.AutoSize = true;
            this.Q2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Q2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q2.ForeColor = System.Drawing.Color.Black;
            this.Q2.Location = new System.Drawing.Point(15, 167);
            this.Q2.Name = "Q2";
            this.Q2.Size = new System.Drawing.Size(359, 19);
            this.Q2.TabIndex = 87;
            this.Q2.Text = "b) À qui s\'adresse-t-il ? Qu\'est-ce qu\'il exprime ?";
            // 
            // Q3
            // 
            this.Q3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Q3.AutoSize = true;
            this.Q3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Q3.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q3.ForeColor = System.Drawing.Color.Black;
            this.Q3.Location = new System.Drawing.Point(13, 220);
            this.Q3.Name = "Q3";
            this.Q3.Size = new System.Drawing.Size(412, 19);
            this.Q3.TabIndex = 88;
            this.Q3.Text = "c) Décrivez l`image. Trouvez un titre à ce document.";
            // 
            // Q4
            // 
            this.Q4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Q4.AutoSize = true;
            this.Q4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Q4.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.Q4.ForeColor = System.Drawing.Color.Black;
            this.Q4.Location = new System.Drawing.Point(15, 274);
            this.Q4.Name = "Q4";
            this.Q4.Size = new System.Drawing.Size(492, 19);
            this.Q4.TabIndex = 89;
            this.Q4.Text = "d) Que pensez-vous de l\'impact de ce document ? Le trouvez-vous";
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(675, 378);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 90;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(7, 422);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 19);
            this.label1.TabIndex = 91;
            this.label1.Text = "* Cet exercice est à traviller en classe";
            // 
            // VExercise8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Q4);
            this.Controls.Add(this.Q3);
            this.Controls.Add(this.Q2);
            this.Controls.Add(this.Q1);
            this.Controls.Add(this.Example);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Back_Button);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise8";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 8";
            this.Load += new System.EventHandler(this.VExercise8_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Example)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox Example;
        private System.Windows.Forms.Label Q1;
        private System.Windows.Forms.Label Q2;
        private System.Windows.Forms.Label Q3;
        private System.Windows.Forms.Label Q4;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Label label1;
    }
}