﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise2 : Form
    {
        struct DataS
        {
            public Label lbl;
            public Point loc;
            public FlowLayoutPanel p;
        }

        DataS[] queue = new DataS[30];
        int len = 0;

        public VExercise2()
        {
            InitializeComponent();
        }

        #region ILabel Drag and Drop Controls
        private void ILabel_MouseDown(object sender, MouseEventArgs e)
        {
            ILabel.DoDragDrop(ILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }
        #endregion

        #region IILabel Drag and Drop Controls
        private void IILabel_MouseDown(object sender, MouseEventArgs e)
        {
            IILabel.DoDragDrop(IILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region IIILabel Drag and Drop Controls
        private void IIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            IIILabel.DoDragDrop(IIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region IVLabel Drag and Drop Controls
        private void IVLabel_MouseDown(object sender, MouseEventArgs e)
        {
            IVLabel.DoDragDrop(IVLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region VLabel Drag and Drop Controls
        private void VLabel_MouseDown(object sender, MouseEventArgs e)
        {
            VLabel.DoDragDrop(VLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region VILabel Drag and Drop Controls
        private void VILabel_MouseDown(object sender, MouseEventArgs e)
        {
            VILabel.DoDragDrop(VILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region VIILabel Drag and Drop Controls
        private void VIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            VIILabel.DoDragDrop(VIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region VIIILabel Drag and Drop Controls
        private void VIIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            VIIILabel.DoDragDrop(VIIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region IXLabel Drag and Drop Controls
        private void IXLabel_MouseDown(object sender, MouseEventArgs e)
        {
            IXLabel.DoDragDrop(IXLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XLabel Drag and Drop Controls
        private void XLabel_MouseDown(object sender, MouseEventArgs e)
        {
            XLabel.DoDragDrop(XLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XILabel Drag and Drop Controls
        private void XILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XILabel.DoDragDrop(XILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XIILabel Drag and Drop Controls
        private void XIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XIILabel.DoDragDrop(XIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XIIILabel Drag and Drop Controls
        private void XIIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XIIILabel.DoDragDrop(XIIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XIVLabel Drag and Drop Controls
        private void XIVLabel_MouseDown(object sender, MouseEventArgs e)
        {
            XIVLabel.DoDragDrop(XIVLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XVLabel Drag and Drop Controls
        private void XVLabel_MouseDown(object sender, MouseEventArgs e)
        {
            XVLabel.DoDragDrop(XVLabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XVILabel Drag and Drop Controls
        private void XVILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XVILabel.DoDragDrop(XVILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XVIILabel Drag and Drop Controls
        private void XVIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XVIILabel.DoDragDrop(XVIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region XVIIILabel Drag and Drop Controls
        private void XVIIILabel_MouseDown(object sender, MouseEventArgs e)
        {
            XVIIILabel.DoDragDrop(XVIIILabel.Text, DragDropEffects.Copy | DragDropEffects.Move);
                
        }
        #endregion

        #region Panel Controls
        private void APanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);
                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = APanel;
                    APanel.Controls.Add(c);
                }
        }

        private void BPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);
            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = BPanel;
                    BPanel.Controls.Add(c);
                }
        }

        private void CPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = CPanel;
                    CPanel.Controls.Add(c);
                }
        }

        private void DPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = DPanel;
                    DPanel.Controls.Add(c);
                }
        }

        private void EPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = EPanel;
                    EPanel.Controls.Add(c);
                }
        }

        private void FPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = FPanel;
                    FPanel.Controls.Add(c);
                }
        }

        private void GPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = GPanel;
                    GPanel.Controls.Add(c);
                }
        }

        private void HPanel_DragDrop(object sender, DragEventArgs e)
        {
            string word = (string)e.Data.GetData(DataFormats.Text);

            foreach (Control c in this.Controls)
                if (c.Text == word && c is Label)
                {
                    c.Font = new Font("Lucida Calligraphy", 8);

                    queue[++len].lbl = c as Label;
                    queue[len].loc = c.Location;
                    queue[len].p = HPanel;
                    HPanel.Controls.Add(c);
                }
        }

        private void DragEnterControl(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        #endregion

        private void OpenDoc_Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Ex1Text.docx");
            }
            catch
            {
                MessageBox.Show("Cannot find file.");
            }
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            int nrW = 0;
            bool p1, p2, p3, p4, p5, p6, p7, p8;

            p1 = p2 = p3 = p4 = p5 = p6 = p7 = p8 = false;
            if (APanel.Contains(ILabel) && APanel.Contains(IILabel) && APanel.Contains(IIILabel)) { p1 = true; nrW++; APanel.ForeColor = Color.LightGreen; }
            else APanel.ForeColor = Color.Red;
            if (BPanel.Contains(IVLabel) && BPanel.Contains(VLabel) && BPanel.Contains(VILabel)) { p2 = true; nrW++; BPanel.ForeColor = Color.LightGreen; }
            else BPanel.ForeColor = Color.Red;
            if (CPanel.Contains(VIILabel) && CPanel.Contains(VIIILabel) && CPanel.Contains(IXLabel)) { p3 = true; nrW++; CPanel.ForeColor = Color.LightGreen; }
            else CPanel.ForeColor = Color.Red;
            if (DPanel.Contains(XLabel) && DPanel.Contains(XILabel) && DPanel.Contains(XIILabel)) { p4 = true; nrW++; DPanel.ForeColor = Color.LightGreen; }
            else DPanel.ForeColor = Color.Red;
            if (EPanel.Contains(XIIILabel) && EPanel.Contains(XIVLabel) && EPanel.Contains(XVLabel)) { p5 = true; nrW++; EPanel.ForeColor = Color.LightGreen; }
            else EPanel.ForeColor = Color.Red;
            if (FPanel.Contains(XVILabel)) { p6 = true; nrW++; FPanel.ForeColor = Color.LightGreen; }
            else FPanel.ForeColor = Color.Red;
            if (GPanel.Contains(XVIILabel)) { p7 = true; nrW++; GPanel.ForeColor = Color.LightGreen; }
            else GPanel.ForeColor = Color.Red;
            if (HPanel.Contains(XVIIILabel)) { p8 = true; nrW++; HPanel.ForeColor = Color.LightGreen; }
            else GPanel.ForeColor = Color.Red;

            if (!p1) MessageBox.Show("Il est un faute dans la première boîte.");
            else if (!p2) MessageBox.Show("Il est un faute dans la deuxième boîte.");
            else if (!p3) MessageBox.Show("Il est un faute dans la troisième boîte.");
            else if (!p4) MessageBox.Show("Il est un faute dans la quatrième boîte.");
            else if (!p5) MessageBox.Show("Il est un faute dans la cinqième boîte.");
            else if (!p6) MessageBox.Show("Il est un faute dans la sixième boîte.");
            else if (!p7) MessageBox.Show("Il est un faute dans la septième boîte.");
            else if (!p8) MessageBox.Show("Il est un faute dans la huitième boîte.");
            else MessageBox.Show("Toutes les boîtes sont correctes!");

            Continue_Button.Show();
        }

        private void Undo_Button_Click(object sender, EventArgs e)
        {
            if (len > 0)
            {
                this.Controls.Add(queue[len].lbl);
                foreach (Control c in this.Controls)
                {
                    if (c == queue[len].lbl)
                    {
                        c.Location = queue[len].loc;
                        c.Font = new Font(c.Font.Name, 12);
                    }
                        
                }
                queue[len].p.Controls.Remove(queue[len].lbl);
                len--;
            }
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise3 f = new VExercise3();
            f.Show();
            this.Hide();
        }
    }
}
