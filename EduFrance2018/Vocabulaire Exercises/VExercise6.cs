﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise6 : Form
    {
        public VExercise6()
        {
            InitializeComponent();
        }

        #region Button Controls
        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise7 f = new VExercise7();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (R1.Text != "emploi") R1.BackColor = Color.Red;
            else R1.BackColor = Color.LightGreen;

            if (R2.Text != "retravailler") R2.BackColor = Color.Red;
            else R2.BackColor = Color.LightGreen;

            if (R3.Text != "domaine") R3.BackColor = Color.Red;
            else R3.BackColor = Color.LightGreen;

            if (R4.Text != "travail") R4.BackColor = Color.Red;
            else R4.BackColor = Color.LightGreen;

            if (R5.Text != "qualifications") R5.BackColor = Color.Red;
            else R5.BackColor = Color.LightGreen;

            if (R6.Text != "étudié") R6.BackColor = Color.Red;
            else R6.BackColor = Color.LightGreen;

            if (R7.Text != "comptabilité") R7.BackColor = Color.Red;
            else R7.BackColor = Color.LightGreen;

            if (R8.Text != "au pair") R8.BackColor = Color.Red;
            else R8.BackColor = Color.LightGreen;

            if (R9.Text != "travaillé") R9.BackColor = Color.Red;
            else R9.BackColor = Color.LightGreen;

            if (R10.Text != "entreprise") R10.BackColor = Color.Red;
            else R10.BackColor = Color.LightGreen;

            if (R11.Text != "textile") R11.BackColor = Color.Red;
            else R11.BackColor = Color.LightGreen;

            if (R12.Text != "lettre de recommandation") R12.BackColor = Color.Red;
            else R12.BackColor = Color.LightGreen;

            if (R13.Text != "employeur") R13.BackColor = Color.Red;
            else R13.BackColor = Color.LightGreen;

            if (R14.Text != "diplômes") R14.BackColor = Color.Red;
            else R14.BackColor = Color.LightGreen;

            if (R15.Text != "fiche") R15.BackColor = Color.Red;
            else R15.BackColor = Color.LightGreen;

            if (R16.Text != "employée") R16.BackColor = Color.Red;
            else R16.BackColor = Color.LightGreen;

            if (R17.Text != "textile") R17.BackColor = Color.Red;
            else R17.BackColor = Color.LightGreen;

            if (R18.Text != "chômage") R18.BackColor = Color.Red;
            else R18.BackColor = Color.LightGreen;

            if (R19.Text != "employeur") R19.BackColor = Color.Red;
            else R19.BackColor = Color.LightGreen;

            if (R20.Text != "chinois") R20.BackColor = Color.Red;
            else R20.BackColor = Color.LightGreen;

            if (R21.Text != "chinois") R21.BackColor = Color.Red;
            else R21.BackColor = Color.LightGreen;

            if (R22.Text != "entreprise") R22.BackColor = Color.Red;
            else R22.BackColor = Color.LightGreen;

            if (R23.Text != "textile") R23.BackColor = Color.Red;
            else R23.BackColor = Color.LightGreen;

            if (R24.Text != "mi-temps") R24.BackColor = Color.Red;
            else R24.BackColor = Color.LightGreen;

            if (R25.Text != "entretien d'embauche") R25.BackColor = Color.Red;
            else R25.BackColor = Color.LightGreen;

            Continue_Button.Show();
        }
        #endregion

        #region Label Controls
        private void L1_MouseDown(object sender, MouseEventArgs e)
        {
            L1.DoDragDrop(L1.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L2_MouseDown(object sender, MouseEventArgs e)
        {
            L2.DoDragDrop(L2.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L3_MouseDown(object sender, MouseEventArgs e)
        {
            L3.DoDragDrop(L3.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L4_MouseDown(object sender, MouseEventArgs e)
        {
            L4.DoDragDrop(L4.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L5_MouseDown(object sender, MouseEventArgs e)
        {
            L5.DoDragDrop(L5.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L6_MouseDown(object sender, MouseEventArgs e)
        {
            L6.DoDragDrop(L6.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L7_MouseDown(object sender, MouseEventArgs e)
        {
            L7.DoDragDrop(L7.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L8_MouseDown(object sender, MouseEventArgs e)
        {
            L8.DoDragDrop(L8.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L9_MouseDown(object sender, MouseEventArgs e)
        {
            L9.DoDragDrop(L9.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L10_MouseDown(object sender, MouseEventArgs e)
        {
            L10.DoDragDrop(L10.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L11_MouseDown(object sender, MouseEventArgs e)
        {
            L11.DoDragDrop(L11.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L12_MouseDown(object sender, MouseEventArgs e)
        {
            L12.DoDragDrop(L12.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L13_MouseDown(object sender, MouseEventArgs e)
        {
            L13.DoDragDrop(L13.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L14_MouseDown(object sender, MouseEventArgs e)
        {
            L14.DoDragDrop(L14.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L15_MouseDown(object sender, MouseEventArgs e)
        {
            L15.DoDragDrop(L15.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L16_MouseDown(object sender, MouseEventArgs e)
        {
            L16.DoDragDrop(L16.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L17_MouseDown(object sender, MouseEventArgs e)
        {
            L17.DoDragDrop(L17.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L18_MouseDown(object sender, MouseEventArgs e)
        {
            L18.DoDragDrop(L18.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L19_MouseDown(object sender, MouseEventArgs e)
        {
            L19.DoDragDrop(L19.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void L20_MouseDown(object sender, MouseEventArgs e)
        {
            L20.DoDragDrop(L20.Text, DragDropEffects.Copy | DragDropEffects.Move);
        }
        #endregion

        #region Text Boxes Controls
        private void DragEnterFunction(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text)) e.Effect = DragDropEffects.Copy;
            else e.Effect = DragDropEffects.None;
        }

        private void R1_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R1.Text = txt;
        }

        private void R2_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R2.Text = txt;
        }

        private void R3_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R3.Text = txt;
        }

        private void R4_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R4.Text = txt;
        }

        private void R5_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R5.Text = txt;
        }

        private void R6_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R6.Text = txt;
        }

        private void R7_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R7.Text = txt;
        }

        private void R8_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R8.Text = txt;
        }

        private void R9_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R9.Text = txt;
        }

        private void R10_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R10.Text = txt;
        }

        private void R11_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R11.Text = txt;
        }

        private void R12_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R12.Text = txt;
        }

        private void R13_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R13.Text = txt;
        }

        private void R14_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R14.Text = txt;
        }

        private void R15_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R15.Text = txt;
        }

        private void R16_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R16.Text = txt;
        }

        private void R17_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R17.Text = txt;
        }

        private void R18_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R18.Text = txt;
        }

        private void R19_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R19.Text = txt;
        }

        private void R20_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R20.Text = txt;
        }

        private void R21_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R21.Text = txt;
        }

        private void R22_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R22.Text = txt;
        }

        private void R23_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R23.Text = txt;
        }

        private void R24_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R24.Text = txt;
        }

        private void R25_DragDrop(object sender, DragEventArgs e)
        {
            string txt = (string)e.Data.GetData(DataFormats.Text);
            R25.Text = txt;
        }
        #endregion
    }
}
