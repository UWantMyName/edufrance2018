﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise3 : Form
    {
        public VExercise3()
        {
            InitializeComponent();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Contains("b)")) comboBox1.BackColor = Color.LightGreen;
            else comboBox1.BackColor = Color.Red;

            if (comboBox2.Text.Contains("a)")) comboBox2.BackColor = Color.LightGreen;
            else comboBox2.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise4 f = new VExercise4();
            f.Show();
            this.Hide();
        }
    }
}
