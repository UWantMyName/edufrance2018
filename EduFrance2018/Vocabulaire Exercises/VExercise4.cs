﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise4 : Form
    {
        public VExercise4()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Contains("d)")) comboBox1.BackColor = Color.LightGreen;
            else comboBox1.BackColor = Color.Red;

            if (comboBox2.Text.Contains("c)")) comboBox2.BackColor = Color.LightGreen;
            else comboBox2.BackColor = Color.Red;

            if (comboBox3.Text.Contains("e)")) comboBox3.BackColor = Color.LightGreen;
            else comboBox3.BackColor = Color.Red;

            if (comboBox4.Text.Contains("a)")) comboBox4.BackColor = Color.LightGreen;
            else comboBox4.BackColor = Color.Red;

            if (comboBox5.Text.Contains("b)")) comboBox5.BackColor = Color.LightGreen;
            else comboBox5.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise5 f = new VExercise5();
            f.Show();
            this.Hide();
        }
    }
}
