﻿namespace EduFrance2018
{
    partial class VExercise15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise15));
            this.label4 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Label21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Label31 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Label32 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Label41 = new System.Windows.Forms.Label();
            this.Label42 = new System.Windows.Forms.Label();
            this.Label43 = new System.Windows.Forms.Label();
            this.Label44 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.Label51 = new System.Windows.Forms.Label();
            this.Label102 = new System.Windows.Forms.Label();
            this.Label52 = new System.Windows.Forms.Label();
            this.Label2555 = new System.Windows.Forms.Label();
            this.Label53 = new System.Windows.Forms.Label();
            this.Label255 = new System.Windows.Forms.Label();
            this.Label54 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Label35 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 20F, System.Drawing.FontStyle.Underline);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(245, 36);
            this.label4.TabIndex = 38;
            this.label4.Text = "Chassez l`intrus";
            // 
            // Label11
            // 
            this.Label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label11.AutoSize = true;
            this.Label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label11.ForeColor = System.Drawing.Color.Black;
            this.Label11.Location = new System.Drawing.Point(14, 79);
            this.Label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(123, 42);
            this.Label11.TabIndex = 39;
            this.Label11.Text = "Je peux vous \r\nrenseigner?\r\n";
            this.Label11.Click += new System.EventHandler(this.Label11_Click);
            this.Label11.MouseEnter += new System.EventHandler(this.Label11_MouseEnter);
            this.Label11.MouseLeave += new System.EventHandler(this.Label11_MouseLeave);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(132, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 21);
            this.label1.TabIndex = 40;
            this.label1.Text = "-";
            // 
            // Label12
            // 
            this.Label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label12.AutoSize = true;
            this.Label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label12.ForeColor = System.Drawing.Color.Black;
            this.Label12.Location = new System.Drawing.Point(149, 91);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(125, 21);
            this.Label12.TabIndex = 41;
            this.Label12.Text = "Vous désirez?\r\n";
            this.Label12.Click += new System.EventHandler(this.Label12_Click);
            this.Label12.MouseEnter += new System.EventHandler(this.Label12_MouseEnter);
            this.Label12.MouseLeave += new System.EventHandler(this.Label12_MouseLeave);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(278, 91);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 21);
            this.label3.TabIndex = 42;
            this.label3.Text = "-";
            // 
            // Label13
            // 
            this.Label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label13.AutoSize = true;
            this.Label13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label13.ForeColor = System.Drawing.Color.Black;
            this.Label13.Location = new System.Drawing.Point(298, 85);
            this.Label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(138, 42);
            this.Label13.TabIndex = 43;
            this.Label13.Text = "Vous cherchez \r\nquelque chose?\r\n";
            this.Label13.Click += new System.EventHandler(this.Label13_Click);
            this.Label13.MouseEnter += new System.EventHandler(this.Label13_MouseEnter);
            this.Label13.MouseLeave += new System.EventHandler(this.Label13_MouseLeave);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(441, 93);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 21);
            this.label6.TabIndex = 44;
            this.label6.Text = "-";
            // 
            // Label14
            // 
            this.Label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label14.AutoSize = true;
            this.Label14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label14.ForeColor = System.Drawing.Color.Black;
            this.Label14.Location = new System.Drawing.Point(464, 93);
            this.Label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(115, 21);
            this.Label14.TabIndex = 45;
            this.Label14.Text = "Vous restez?";
            this.Label14.Click += new System.EventHandler(this.Label14_Click);
            this.Label14.MouseEnter += new System.EventHandler(this.Label14_MouseEnter);
            this.Label14.MouseLeave += new System.EventHandler(this.Label14_MouseLeave);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.textBox1.Location = new System.Drawing.Point(645, 92);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(155, 45);
            this.textBox1.TabIndex = 46;
            // 
            // Label21
            // 
            this.Label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label21.AutoSize = true;
            this.Label21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label21.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label21.ForeColor = System.Drawing.Color.Black;
            this.Label21.Location = new System.Drawing.Point(14, 178);
            this.Label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(114, 21);
            this.Label21.TabIndex = 47;
            this.Label21.Text = "Etre élégant";
            this.Label21.Click += new System.EventHandler(this.Label21_Click);
            this.Label21.MouseEnter += new System.EventHandler(this.Label21_MouseEnter);
            this.Label21.MouseLeave += new System.EventHandler(this.Label21_MouseLeave);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(127, 179);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 21);
            this.label5.TabIndex = 48;
            this.label5.Text = "-";
            // 
            // Label22
            // 
            this.Label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label22.AutoSize = true;
            this.Label22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label22.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label22.ForeColor = System.Drawing.Color.Black;
            this.Label22.Location = new System.Drawing.Point(141, 170);
            this.Label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(140, 42);
            this.Label22.TabIndex = 49;
            this.Label22.Text = "    être tiré à \r\nquatre épingles";
            this.Label22.Click += new System.EventHandler(this.Label22_Click);
            this.Label22.MouseEnter += new System.EventHandler(this.Label22_MouseEnter);
            this.Label22.MouseLeave += new System.EventHandler(this.Label22_MouseLeave);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label8.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(280, 178);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 21);
            this.label8.TabIndex = 50;
            this.label8.Text = "-";
            // 
            // Label23
            // 
            this.Label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label23.AutoSize = true;
            this.Label23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label23.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label23.ForeColor = System.Drawing.Color.Black;
            this.Label23.Location = new System.Drawing.Point(297, 170);
            this.Label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(95, 42);
            this.Label23.TabIndex = 51;
            this.Label23.Text = " se mettre\r\n sur son 31";
            this.Label23.Click += new System.EventHandler(this.Label23_Click);
            this.Label23.MouseEnter += new System.EventHandler(this.Label23_MouseEnter);
            this.Label23.MouseLeave += new System.EventHandler(this.Label23_MouseLeave);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(398, 179);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 21);
            this.label10.TabIndex = 52;
            this.label10.Text = "-";
            // 
            // Label24
            // 
            this.Label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label24.AutoSize = true;
            this.Label24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label24.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label24.ForeColor = System.Drawing.Color.Black;
            this.Label24.Location = new System.Drawing.Point(429, 178);
            this.Label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(150, 21);
            this.Label24.TabIndex = 53;
            this.Label24.Text = "être décontracté\r\n";
            this.Label24.Click += new System.EventHandler(this.Label24_Click);
            this.Label24.MouseEnter += new System.EventHandler(this.Label24_MouseEnter);
            this.Label24.MouseLeave += new System.EventHandler(this.Label24_MouseLeave);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.textBox2.Location = new System.Drawing.Point(645, 173);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(155, 45);
            this.textBox2.TabIndex = 54;
            // 
            // Label31
            // 
            this.Label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label31.AutoSize = true;
            this.Label31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label31.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label31.ForeColor = System.Drawing.Color.Black;
            this.Label31.Location = new System.Drawing.Point(31, 250);
            this.Label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(55, 21);
            this.Label31.TabIndex = 55;
            this.Label31.Text = "Serré";
            this.Label31.Click += new System.EventHandler(this.Label31_Click);
            this.Label31.MouseEnter += new System.EventHandler(this.Label31_MouseEnter);
            this.Label31.MouseLeave += new System.EventHandler(this.Label31_MouseLeave);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(103, 250);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 21);
            this.label7.TabIndex = 56;
            this.label7.Text = "-";
            // 
            // Label32
            // 
            this.Label32.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label32.AutoSize = true;
            this.Label32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label32.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label32.ForeColor = System.Drawing.Color.Black;
            this.Label32.Location = new System.Drawing.Point(145, 250);
            this.Label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(54, 21);
            this.Label32.TabIndex = 57;
            this.Label32.Text = "large";
            this.Label32.Click += new System.EventHandler(this.Label32_Click);
            this.Label32.MouseEnter += new System.EventHandler(this.Label32_MouseEnter);
            this.Label32.MouseLeave += new System.EventHandler(this.Label32_MouseLeave);
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label15.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(214, 250);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 21);
            this.label15.TabIndex = 58;
            this.label15.Text = "-";
            // 
            // Label33
            // 
            this.Label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label33.AutoSize = true;
            this.Label33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label33.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label33.ForeColor = System.Drawing.Color.Black;
            this.Label33.Location = new System.Drawing.Point(252, 250);
            this.Label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(66, 21);
            this.Label33.TabIndex = 59;
            this.Label33.Text = "seyant";
            this.Label33.Click += new System.EventHandler(this.Label33_Click);
            this.Label33.MouseEnter += new System.EventHandler(this.Label33_MouseEnter);
            this.Label33.MouseLeave += new System.EventHandler(this.Label33_MouseLeave);
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(342, 250);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 21);
            this.label17.TabIndex = 60;
            this.label17.Text = "-";
            // 
            // Label34
            // 
            this.Label34.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label34.AutoSize = true;
            this.Label34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label34.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label34.ForeColor = System.Drawing.Color.Black;
            this.Label34.Location = new System.Drawing.Point(373, 250);
            this.Label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(80, 21);
            this.Label34.TabIndex = 61;
            this.Label34.Text = "moulant";
            this.Label34.Click += new System.EventHandler(this.Label34_Click);
            this.Label34.MouseEnter += new System.EventHandler(this.Label34_MouseEnter);
            this.Label34.MouseLeave += new System.EventHandler(this.Label34_MouseLeave);
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.textBox3.Location = new System.Drawing.Point(645, 244);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(155, 45);
            this.textBox3.TabIndex = 62;
            // 
            // Label41
            // 
            this.Label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label41.AutoSize = true;
            this.Label41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label41.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label41.ForeColor = System.Drawing.Color.Black;
            this.Label41.Location = new System.Drawing.Point(15, 316);
            this.Label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(122, 21);
            this.Label41.TabIndex = 63;
            this.Label41.Text = "La chaussure";
            this.Label41.Click += new System.EventHandler(this.Label41_Click);
            this.Label41.MouseEnter += new System.EventHandler(this.Label41_MouseEnter);
            this.Label41.MouseLeave += new System.EventHandler(this.Label41_MouseLeave);
            // 
            // Label42
            // 
            this.Label42.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label42.AutoSize = true;
            this.Label42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label42.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label42.ForeColor = System.Drawing.Color.Black;
            this.Label42.Location = new System.Drawing.Point(169, 316);
            this.Label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(74, 21);
            this.Label42.TabIndex = 64;
            this.Label42.Text = "la taille";
            this.Label42.Click += new System.EventHandler(this.Label42_Click);
            this.Label42.MouseEnter += new System.EventHandler(this.Label42_MouseEnter);
            this.Label42.MouseLeave += new System.EventHandler(this.Label42_MouseLeave);
            // 
            // Label43
            // 
            this.Label43.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label43.AutoSize = true;
            this.Label43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label43.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label43.ForeColor = System.Drawing.Color.Black;
            this.Label43.Location = new System.Drawing.Point(280, 316);
            this.Label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(91, 21);
            this.Label43.TabIndex = 65;
            this.Label43.Text = "la semelle";
            this.Label43.Click += new System.EventHandler(this.Label43_Click);
            this.Label43.MouseEnter += new System.EventHandler(this.Label43_MouseEnter);
            this.Label43.MouseLeave += new System.EventHandler(this.Label43_MouseLeave);
            // 
            // Label44
            // 
            this.Label44.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label44.AutoSize = true;
            this.Label44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label44.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label44.ForeColor = System.Drawing.Color.Black;
            this.Label44.Location = new System.Drawing.Point(429, 316);
            this.Label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label44.Name = "Label44";
            this.Label44.Size = new System.Drawing.Size(104, 21);
            this.Label44.TabIndex = 66;
            this.Label44.Text = "la pointure";
            this.Label44.Click += new System.EventHandler(this.Label44_Click);
            this.Label44.MouseEnter += new System.EventHandler(this.Label44_MouseEnter);
            this.Label44.MouseLeave += new System.EventHandler(this.Label44_MouseLeave);
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label27.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(142, 316);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 21);
            this.label27.TabIndex = 67;
            this.label27.Text = "-";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label28.AutoSize = true;
            this.label28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label28.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(252, 316);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 21);
            this.label28.TabIndex = 68;
            this.label28.Text = "-";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label29.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(386, 316);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(15, 21);
            this.label29.TabIndex = 69;
            this.label29.Text = "-";
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.textBox4.Location = new System.Drawing.Point(645, 310);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(155, 45);
            this.textBox4.TabIndex = 70;
            // 
            // Label51
            // 
            this.Label51.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label51.AutoSize = true;
            this.Label51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label51.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label51.ForeColor = System.Drawing.Color.Black;
            this.Label51.Location = new System.Drawing.Point(18, 376);
            this.Label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label51.Name = "Label51";
            this.Label51.Size = new System.Drawing.Size(84, 42);
            this.Label51.TabIndex = 71;
            this.Label51.Text = "Elle vous\r\nva bien\r\n";
            this.Label51.Click += new System.EventHandler(this.Label51_Click);
            this.Label51.MouseEnter += new System.EventHandler(this.Label51_MouseEnter);
            this.Label51.MouseLeave += new System.EventHandler(this.Label51_MouseLeave);
            // 
            // Label102
            // 
            this.Label102.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label102.AutoSize = true;
            this.Label102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label102.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label102.ForeColor = System.Drawing.Color.Black;
            this.Label102.Location = new System.Drawing.Point(114, 387);
            this.Label102.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label102.Name = "Label102";
            this.Label102.Size = new System.Drawing.Size(15, 21);
            this.Label102.TabIndex = 72;
            this.Label102.Text = "-";
            // 
            // Label52
            // 
            this.Label52.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label52.AutoSize = true;
            this.Label52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label52.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label52.ForeColor = System.Drawing.Color.Black;
            this.Label52.Location = new System.Drawing.Point(145, 376);
            this.Label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label52.Name = "Label52";
            this.Label52.Size = new System.Drawing.Size(84, 42);
            this.Label52.TabIndex = 73;
            this.Label52.Text = "Elle vous\r\namincit\r\n";
            this.Label52.Click += new System.EventHandler(this.Label52_Click);
            this.Label52.MouseEnter += new System.EventHandler(this.Label52_MouseEnter);
            this.Label52.MouseLeave += new System.EventHandler(this.Label52_MouseLeave);
            // 
            // Label2555
            // 
            this.Label2555.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label2555.AutoSize = true;
            this.Label2555.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label2555.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label2555.ForeColor = System.Drawing.Color.Black;
            this.Label2555.Location = new System.Drawing.Point(243, 387);
            this.Label2555.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2555.Name = "Label2555";
            this.Label2555.Size = new System.Drawing.Size(15, 21);
            this.Label2555.TabIndex = 74;
            this.Label2555.Text = "-";
            // 
            // Label53
            // 
            this.Label53.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label53.AutoSize = true;
            this.Label53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label53.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label53.ForeColor = System.Drawing.Color.Black;
            this.Label53.Location = new System.Drawing.Point(267, 376);
            this.Label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(106, 42);
            this.Label53.TabIndex = 75;
            this.Label53.Text = "Elle est très\r\n élégante";
            this.Label53.Click += new System.EventHandler(this.Label53_Click);
            this.Label53.MouseEnter += new System.EventHandler(this.Label53_MouseEnter);
            this.Label53.MouseLeave += new System.EventHandler(this.Label53_MouseLeave);
            // 
            // Label255
            // 
            this.Label255.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label255.AutoSize = true;
            this.Label255.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label255.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label255.ForeColor = System.Drawing.Color.Black;
            this.Label255.Location = new System.Drawing.Point(387, 387);
            this.Label255.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label255.Name = "Label255";
            this.Label255.Size = new System.Drawing.Size(15, 21);
            this.Label255.TabIndex = 76;
            this.Label255.Text = "-";
            // 
            // Label54
            // 
            this.Label54.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label54.AutoSize = true;
            this.Label54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label54.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label54.ForeColor = System.Drawing.Color.Black;
            this.Label54.Location = new System.Drawing.Point(416, 376);
            this.Label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label54.Name = "Label54";
            this.Label54.Size = new System.Drawing.Size(106, 42);
            this.Label54.TabIndex = 77;
            this.Label54.Text = "elle ne vous\r\n   va pas";
            this.Label54.Click += new System.EventHandler(this.Label54_Click);
            this.Label54.MouseEnter += new System.EventHandler(this.Label54_MouseEnter);
            this.Label54.MouseLeave += new System.EventHandler(this.Label54_MouseLeave);
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Font = new System.Drawing.Font("Lucida Calligraphy", 10F);
            this.textBox5.Location = new System.Drawing.Point(645, 381);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(155, 45);
            this.textBox5.TabIndex = 78;
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(12, 456);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 116;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(402, 456);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 115;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(728, 456);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 114;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(464, 250);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 21);
            this.label2.TabIndex = 117;
            this.label2.Text = "-";
            // 
            // Label35
            // 
            this.Label35.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label35.AutoSize = true;
            this.Label35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label35.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Label35.ForeColor = System.Drawing.Color.Black;
            this.Label35.Location = new System.Drawing.Point(487, 250);
            this.Label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(63, 21);
            this.Label35.TabIndex = 118;
            this.Label35.Text = "vernis";
            this.Label35.Click += new System.EventHandler(this.Label35_Click);
            this.Label35.MouseEnter += new System.EventHandler(this.Label35_MouseEnter);
            this.Label35.MouseLeave += new System.EventHandler(this.Label35_MouseLeave);
            // 
            // VExercise15
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(853, 495);
            this.Controls.Add(this.Label35);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.Label54);
            this.Controls.Add(this.Label53);
            this.Controls.Add(this.Label52);
            this.Controls.Add(this.Label51);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.Label44);
            this.Controls.Add(this.Label43);
            this.Controls.Add(this.Label42);
            this.Controls.Add(this.Label41);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.Label34);
            this.Controls.Add(this.Label33);
            this.Controls.Add(this.Label32);
            this.Controls.Add(this.Label31);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.Label24);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.Label22);
            this.Controls.Add(this.Label21);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.Label102);
            this.Controls.Add(this.Label2555);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.Label255);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise15";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 15";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Label14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label Label21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Label22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Label23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label Label24;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label Label31;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Label32;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label Label33;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label Label34;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label Label41;
        private System.Windows.Forms.Label Label42;
        private System.Windows.Forms.Label Label43;
        private System.Windows.Forms.Label Label44;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label Label51;
        private System.Windows.Forms.Label Label102;
        private System.Windows.Forms.Label Label52;
        private System.Windows.Forms.Label Label2555;
        private System.Windows.Forms.Label Label53;
        private System.Windows.Forms.Label Label255;
        private System.Windows.Forms.Label Label54;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Label35;
    }
}