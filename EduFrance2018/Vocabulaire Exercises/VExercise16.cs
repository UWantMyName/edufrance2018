﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise16 : Form
    {
        PictureBox YE = new PictureBox();
        PictureBox NE = new PictureBox();

        public VExercise16()
        {
            InitializeComponent();
        }

        private void VExercise16_Load(object sender, EventArgs e)
        {
            YesEmoji.Image = Image.FromFile("YesEmoji.gif");
            NoEmoji.Image = Image.FromFile("NoEmoji.gif");
            YE.Image = Image.FromFile("YesEmoji.gif");
            YE.Tag = "YesEmoji";
            NE.Image = Image.FromFile("NoEmoji.gif");
            NE.Tag = "NoEmoji";
        }

        #region Image Drag and Drop Controls
        private void YesEmoji_MouseDown(object sender, MouseEventArgs e)
        {
            YesEmoji.DoDragDrop(YesEmoji.Image, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void NoEmoji_MouseDown(object sender, MouseEventArgs e)
        {
            NoEmoji.DoDragDrop(NoEmoji.Image, DragDropEffects.Copy | DragDropEffects.Move);
        }
        #endregion

        #region Panel Controls
        private void DragEnterFunction(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Bitmap)) e.Effect = DragDropEffects.Copy;
            else e.Effect = DragDropEffects.None;
        }

        private void R1_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R1.Controls)
                R1.Controls.Remove(c);
            R1.Controls.Add(pb);      
        }

        private void R2_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R2.Controls)
                R2.Controls.Remove(c);
            R2.Controls.Add(pb);
        }

        private void R3_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R3.Controls)
                R3.Controls.Remove(c);
            R3.Controls.Add(pb);
        }

        private void R4_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R4.Controls)
                R4.Controls.Remove(c);
            R4.Controls.Add(pb);
        }

        private void R5_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R5.Controls)
                R5.Controls.Remove(c);
            R5.Controls.Add(pb);
        }

        private void R6_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R6.Controls)
                R6.Controls.Remove(c);
            R6.Controls.Add(pb);
        }

        private void R7_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R7.Controls)
                R7.Controls.Remove(c);
            R7.Controls.Add(pb);
        }

        private void R8_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R8.Controls)
                R8.Controls.Remove(c);
            R8.Controls.Add(pb);
        }

        private void R9_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R9.Controls)
                R9.Controls.Remove(c);
            R9.Controls.Add(pb);
        }

        private void R10_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R10.Controls)
                R10.Controls.Remove(c);
            R10.Controls.Add(pb);
        }

        private void R11_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R11.Controls)
                R11.Controls.Remove(c);
            R11.Controls.Add(pb);
        }

        private void R12_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R12.Controls)
                R12.Controls.Remove(c);
            R12.Controls.Add(pb);
        }

        private void R13_DragDrop(object sender, DragEventArgs e)
        {
            Image img = (Image)e.Data.GetData(DataFormats.Bitmap);

            PictureBox pb = new PictureBox()
            {
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(30, 30),
                Image = img,
            };
            foreach (Control c in R13.Controls)
                R13.Controls.Remove(c);
            R13.Controls.Add(pb);
        }
        #endregion

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            string mess = "Les réponses devraient être:\n";
            mess += "1 = positive\n";
            mess += "2 = négatif\n";
            mess += "3 = négatif\n";
            mess += "4 = négatif\n";
            mess += "5 = négatif\n";
            mess += "6 = négatif\n";
            mess += "7 = positive\n";
            mess += "8 = négatif\n";
            mess += "9 = négatif\n";
            mess += "10 = positive\n";
            mess += "11 = négatif\n";
            mess += "12 = négatif\n";
            mess += "13 = positive\n";
            MessageBox.Show(mess);
        }
    }
}
