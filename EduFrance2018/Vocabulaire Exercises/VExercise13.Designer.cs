﻿namespace EduFrance2018
{
    partial class VExercise13
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VExercise13));
            this.Title = new System.Windows.Forms.Label();
            this.L1 = new System.Windows.Forms.Label();
            this.L2 = new System.Windows.Forms.Label();
            this.L3 = new System.Windows.Forms.Label();
            this.L4 = new System.Windows.Forms.Label();
            this.L5 = new System.Windows.Forms.Label();
            this.L6 = new System.Windows.Forms.Label();
            this.L7 = new System.Windows.Forms.Label();
            this.L8 = new System.Windows.Forms.Label();
            this.L9 = new System.Windows.Forms.Label();
            this.L10 = new System.Windows.Forms.Label();
            this.L11 = new System.Windows.Forms.Label();
            this.L12 = new System.Windows.Forms.Label();
            this.L13 = new System.Windows.Forms.Label();
            this.L14 = new System.Windows.Forms.Label();
            this.L15 = new System.Windows.Forms.Label();
            this.L16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.R1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.R2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.R3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.R4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.R5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.R6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.R7 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.R8 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.R9 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.R10 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.R11 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.R12 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.R13 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.R14 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.R15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Title.AutoSize = true;
            this.Title.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Title.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.Title.ForeColor = System.Drawing.Color.Black;
            this.Title.Location = new System.Drawing.Point(47, 9);
            this.Title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(765, 27);
            this.Title.TabIndex = 35;
            this.Title.Text = "Complétez le texte ci-dessous avec les mots et les expressions suivantes:";
            // 
            // L1
            // 
            this.L1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L1.AutoSize = true;
            this.L1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L1.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L1.ForeColor = System.Drawing.Color.Black;
            this.L1.Location = new System.Drawing.Point(16, 50);
            this.L1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(112, 19);
            this.L1.TabIndex = 36;
            this.L1.Text = "en hors saison";
            this.L1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L1_MouseDown);
            // 
            // L2
            // 
            this.L2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L2.AutoSize = true;
            this.L2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L2.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L2.ForeColor = System.Drawing.Color.Black;
            this.L2.Location = new System.Drawing.Point(148, 50);
            this.L2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(136, 19);
            this.L2.TabIndex = 37;
            this.L2.Text = "pension complète";
            this.L2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L2_MouseDown);
            // 
            // L3
            // 
            this.L3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L3.AutoSize = true;
            this.L3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L3.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L3.ForeColor = System.Drawing.Color.Black;
            this.L3.Location = new System.Drawing.Point(310, 50);
            this.L3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(107, 19);
            this.L3.TabIndex = 38;
            this.L3.Text = "un bungalow";
            this.L3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L3_MouseDown);
            // 
            // L4
            // 
            this.L4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L4.AutoSize = true;
            this.L4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L4.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L4.ForeColor = System.Drawing.Color.Black;
            this.L4.Location = new System.Drawing.Point(438, 50);
            this.L4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(105, 19);
            this.L4.TabIndex = 39;
            this.L4.Text = "des vacances";
            this.L4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L4_MouseDown);
            // 
            // L5
            // 
            this.L5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L5.AutoSize = true;
            this.L5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L5.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L5.ForeColor = System.Drawing.Color.Black;
            this.L5.Location = new System.Drawing.Point(565, 50);
            this.L5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L5.Name = "L5";
            this.L5.Size = new System.Drawing.Size(68, 19);
            this.L5.TabIndex = 40;
            this.L5.Text = "dépaysé";
            this.L5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L5_MouseDown);
            // 
            // L6
            // 
            this.L6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L6.AutoSize = true;
            this.L6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L6.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L6.ForeColor = System.Drawing.Color.Black;
            this.L6.Location = new System.Drawing.Point(652, 50);
            this.L6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L6.Name = "L6";
            this.L6.Size = new System.Drawing.Size(57, 19);
            this.L6.TabIndex = 41;
            this.L6.Text = "le visa";
            this.L6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L6_MouseDown);
            // 
            // L7
            // 
            this.L7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L7.AutoSize = true;
            this.L7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L7.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L7.ForeColor = System.Drawing.Color.Black;
            this.L7.Location = new System.Drawing.Point(724, 50);
            this.L7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L7.Name = "L7";
            this.L7.Size = new System.Drawing.Size(108, 19);
            this.L7.TabIndex = 42;
            this.L7.Text = "en promotion";
            this.L7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L7_MouseDown);
            // 
            // L8
            // 
            this.L8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L8.AutoSize = true;
            this.L8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L8.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L8.ForeColor = System.Drawing.Color.Black;
            this.L8.Location = new System.Drawing.Point(13, 84);
            this.L8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L8.Name = "L8";
            this.L8.Size = new System.Drawing.Size(115, 19);
            this.L8.TabIndex = 43;
            this.L8.Text = "une excursion";
            this.L8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L8_MouseDown);
            // 
            // L9
            // 
            this.L9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L9.AutoSize = true;
            this.L9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L9.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L9.ForeColor = System.Drawing.Color.Black;
            this.L9.Location = new System.Drawing.Point(149, 84);
            this.L9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L9.Name = "L9";
            this.L9.Size = new System.Drawing.Size(145, 19);
            this.L9.TabIndex = 44;
            this.L9.Text = "le vol aller/retour";
            this.L9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L9_MouseDown);
            // 
            // L10
            // 
            this.L10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L10.AutoSize = true;
            this.L10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L10.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L10.ForeColor = System.Drawing.Color.Black;
            this.L10.Location = new System.Drawing.Point(318, 84);
            this.L10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L10.Name = "L10";
            this.L10.Size = new System.Drawing.Size(75, 19);
            this.L10.TabIndex = 45;
            this.L10.Text = "un guide";
            this.L10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L10_MouseDown);
            // 
            // L11
            // 
            this.L11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L11.AutoSize = true;
            this.L11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L11.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L11.ForeColor = System.Drawing.Color.Black;
            this.L11.Location = new System.Drawing.Point(410, 84);
            this.L11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(161, 19);
            this.L11.TabIndex = 46;
            this.L11.Text = "une chambre double";
            this.L11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L11_MouseDown);
            // 
            // L12
            // 
            this.L12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L12.AutoSize = true;
            this.L12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L12.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L12.ForeColor = System.Drawing.Color.Black;
            this.L12.Location = new System.Drawing.Point(599, 84);
            this.L12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(205, 19);
            this.L12.TabIndex = 47;
            this.L12.Text = "formalités administratives";
            this.L12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L12_MouseDown);
            // 
            // L13
            // 
            this.L13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L13.AutoSize = true;
            this.L13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L13.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L13.ForeColor = System.Drawing.Color.Black;
            this.L13.Location = new System.Drawing.Point(13, 121);
            this.L13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(106, 19);
            this.L13.TabIndex = 48;
            this.L13.Text = "les transferts";
            this.L13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L13_MouseDown);
            // 
            // L14
            // 
            this.L14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L14.AutoSize = true;
            this.L14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L14.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L14.ForeColor = System.Drawing.Color.Black;
            this.L14.Location = new System.Drawing.Point(141, 121);
            this.L14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(136, 19);
            this.L14.TabIndex = 49;
            this.L14.Text = "pension complète";
            this.L14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L14_MouseDown);
            // 
            // L15
            // 
            this.L15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L15.AutoSize = true;
            this.L15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L15.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L15.ForeColor = System.Drawing.Color.Black;
            this.L15.Location = new System.Drawing.Point(303, 121);
            this.L15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L15.Name = "L15";
            this.L15.Size = new System.Drawing.Size(143, 19);
            this.L15.TabIndex = 50;
            this.L15.Text = "activités sportives";
            this.L15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L15_MouseDown);
            // 
            // L16
            // 
            this.L16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.L16.AutoSize = true;
            this.L16.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.L16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.L16.Font = new System.Drawing.Font("Lucida Calligraphy", 10F, System.Drawing.FontStyle.Italic);
            this.L16.ForeColor = System.Drawing.Color.Black;
            this.L16.Location = new System.Drawing.Point(475, 121);
            this.L16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L16.Name = "L16";
            this.L16.Size = new System.Drawing.Size(91, 19);
            this.L16.TabIndex = 51;
            this.L16.Text = "les boissons";
            this.L16.MouseDown += new System.Windows.Forms.MouseEventHandler(this.L16_MouseDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 183);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 21);
            this.label1.TabIndex = 52;
            this.label1.Text = "Guillaume veut organiser";
            // 
            // R1
            // 
            this.R1.AllowDrop = true;
            this.R1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R1.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R1.Location = new System.Drawing.Point(245, 185);
            this.R1.Name = "R1";
            this.R1.Size = new System.Drawing.Size(132, 22);
            this.R1.TabIndex = 53;
            this.R1.DragDrop += new System.Windows.Forms.DragEventHandler(this.R1_DragDrop);
            this.R1.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(384, 183);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 21);
            this.label2.TabIndex = 54;
            this.label2.Text = "pour sa femme et lui . Il choisit un séjour";
            // 
            // R2
            // 
            this.R2.AllowDrop = true;
            this.R2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R2.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R2.Location = new System.Drawing.Point(738, 185);
            this.R2.Name = "R2";
            this.R2.Size = new System.Drawing.Size(132, 22);
            this.R2.TabIndex = 55;
            this.R2.DragDrop += new System.Windows.Forms.DragEventHandler(this.R2_DragDrop);
            this.R2.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(13, 219);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(531, 21);
            this.label3.TabIndex = 56;
            this.label3.Text = "à Agadir au Maroc. Le séjour est moins cher parce qu\'il part ";
            // 
            // R3
            // 
            this.R3.AllowDrop = true;
            this.R3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R3.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R3.Location = new System.Drawing.Point(540, 221);
            this.R3.Name = "R3";
            this.R3.Size = new System.Drawing.Size(137, 22);
            this.R3.TabIndex = 57;
            this.R3.DragDrop += new System.Windows.Forms.DragEventHandler(this.R3_DragDrop);
            this.R3.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(679, 222);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 21);
            this.label4.TabIndex = 58;
            this.label4.Text = ". Il souhaite réserver";
            // 
            // R4
            // 
            this.R4.AllowDrop = true;
            this.R4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R4.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R4.Location = new System.Drawing.Point(17, 253);
            this.R4.Name = "R4";
            this.R4.Size = new System.Drawing.Size(151, 22);
            this.R4.TabIndex = 59;
            this.R4.DragDrop += new System.Windows.Forms.DragEventHandler(this.R4_DragDrop);
            this.R4.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(175, 254);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 21);
            this.label5.TabIndex = 60;
            this.label5.Text = "dans";
            // 
            // R5
            // 
            this.R5.AllowDrop = true;
            this.R5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R5.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R5.Location = new System.Drawing.Point(226, 254);
            this.R5.Name = "R5";
            this.R5.Size = new System.Drawing.Size(151, 22);
            this.R5.TabIndex = 61;
            this.R5.DragDrop += new System.Windows.Forms.DragEventHandler(this.R5_DragDrop);
            this.R5.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(384, 254);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(326, 21);
            this.label6.TabIndex = 62;
            this.label6.Text = ". Il choisit une formule qui comprend ";
            // 
            // R6
            // 
            this.R6.AllowDrop = true;
            this.R6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R6.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R6.Location = new System.Drawing.Point(717, 256);
            this.R6.Name = "R6";
            this.R6.Size = new System.Drawing.Size(137, 22);
            this.R6.TabIndex = 63;
            this.R6.DragDrop += new System.Windows.Forms.DragEventHandler(this.R6_DragDrop);
            this.R6.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(856, 257);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 21);
            this.label7.TabIndex = 64;
            this.label7.Text = ",";
            // 
            // R7
            // 
            this.R7.AllowDrop = true;
            this.R7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R7.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R7.Location = new System.Drawing.Point(17, 293);
            this.R7.Name = "R7";
            this.R7.Size = new System.Drawing.Size(137, 22);
            this.R7.TabIndex = 65;
            this.R7.DragDrop += new System.Windows.Forms.DragEventHandler(this.R7_DragDrop);
            this.R7.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label8.AutoSize = true;
            this.label8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label8.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(161, 293);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 21);
            this.label8.TabIndex = 66;
            this.label8.Text = "vers le village et la";
            // 
            // R8
            // 
            this.R8.AllowDrop = true;
            this.R8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R8.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R8.Location = new System.Drawing.Point(337, 292);
            this.R8.Name = "R8";
            this.R8.Size = new System.Drawing.Size(137, 22);
            this.R8.TabIndex = 67;
            this.R8.DragDrop += new System.Windows.Forms.DragEventHandler(this.R8_DragDrop);
            this.R8.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label9.AutoSize = true;
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(477, 293);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 21);
            this.label9.TabIndex = 68;
            this.label9.Text = ".";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label10.AutoSize = true;
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(488, 293);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(252, 21);
            this.label10.TabIndex = 69;
            this.label10.Text = "La formule ne comprend pas";
            // 
            // R9
            // 
            this.R9.AllowDrop = true;
            this.R9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R9.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R9.Location = new System.Drawing.Point(740, 292);
            this.R9.Name = "R9";
            this.R9.Size = new System.Drawing.Size(137, 22);
            this.R9.TabIndex = 70;
            this.R9.DragDrop += new System.Windows.Forms.DragEventHandler(this.R9_DragDrop);
            this.R9.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(13, 329);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 21);
            this.label11.TabIndex = 71;
            this.label11.Text = "et certaines";
            // 
            // R10
            // 
            this.R10.AllowDrop = true;
            this.R10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R10.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R10.Location = new System.Drawing.Point(129, 328);
            this.R10.Name = "R10";
            this.R10.Size = new System.Drawing.Size(137, 22);
            this.R10.TabIndex = 72;
            this.R10.DragDrop += new System.Windows.Forms.DragEventHandler(this.R10_DragDrop);
            this.R10.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(269, 329);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 21);
            this.label12.TabIndex = 73;
            this.label12.Text = ".";
            // 
            // R11
            // 
            this.R11.AllowDrop = true;
            this.R11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R11.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R11.Location = new System.Drawing.Point(291, 329);
            this.R11.Name = "R11";
            this.R11.Size = new System.Drawing.Size(137, 22);
            this.R11.TabIndex = 74;
            this.R11.DragDrop += new System.Windows.Forms.DragEventHandler(this.R11_DragDrop);
            this.R11.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label13.AutoSize = true;
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(438, 329);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(327, 21);
            this.label13.TabIndex = 75;
            this.label13.Text = "n\'est pas nécessaire et le club règle les";
            // 
            // R12
            // 
            this.R12.AllowDrop = true;
            this.R12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R12.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R12.Location = new System.Drawing.Point(765, 328);
            this.R12.Name = "R12";
            this.R12.Size = new System.Drawing.Size(137, 22);
            this.R12.TabIndex = 76;
            this.R12.DragDrop += new System.Windows.Forms.DragEventHandler(this.R12_DragDrop);
            this.R12.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(902, 329);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 21);
            this.label14.TabIndex = 77;
            this.label14.Text = ".";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label15.AutoSize = true;
            this.label15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label15.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(16, 365);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(223, 21);
            this.label15.TabIndex = 78;
            this.label15.Text = "Guillaume voudrait faire";
            // 
            // R13
            // 
            this.R13.AllowDrop = true;
            this.R13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R13.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R13.Location = new System.Drawing.Point(246, 364);
            this.R13.Name = "R13";
            this.R13.Size = new System.Drawing.Size(137, 22);
            this.R13.TabIndex = 79;
            this.R13.DragDrop += new System.Windows.Forms.DragEventHandler(this.R13_DragDrop);
            this.R13.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label16.AutoSize = true;
            this.label16.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label16.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(390, 365);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(330, 21);
            this.label16.TabIndex = 80;
            this.label16.Text = "dans un parc national, peut-être avec";
            // 
            // R14
            // 
            this.R14.AllowDrop = true;
            this.R14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R14.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R14.Location = new System.Drawing.Point(727, 364);
            this.R14.Name = "R14";
            this.R14.Size = new System.Drawing.Size(137, 22);
            this.R14.TabIndex = 81;
            this.R14.DragDrop += new System.Windows.Forms.DragEventHandler(this.R14_DragDrop);
            this.R14.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label17.AutoSize = true;
            this.label17.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(868, 365);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 21);
            this.label17.TabIndex = 82;
            this.label17.Text = ".";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label18.AutoSize = true;
            this.label18.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label18.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(16, 400);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(277, 21);
            this.label18.TabIndex = 83;
            this.label18.Text = "Le conseiller lui dit qu\'il va être";
            // 
            // R15
            // 
            this.R15.AllowDrop = true;
            this.R15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.R15.Font = new System.Drawing.Font("Lucida Calligraphy", 8F);
            this.R15.Location = new System.Drawing.Point(291, 399);
            this.R15.Name = "R15";
            this.R15.Size = new System.Drawing.Size(137, 22);
            this.R15.TabIndex = 84;
            this.R15.DragDrop += new System.Windows.Forms.DragEventHandler(this.R15_DragDrop);
            this.R15.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterFunction);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label19.AutoSize = true;
            this.label19.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label19.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(435, 400);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 21);
            this.label19.TabIndex = 85;
            this.label19.Text = ".";
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Continue_Button.BackColor = System.Drawing.Color.Yellow;
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(12, 479);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 104;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = false;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.BackColor = System.Drawing.Color.Yellow;
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(428, 479);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(96, 27);
            this.Check_Button.TabIndex = 103;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = false;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.BackColor = System.Drawing.Color.Yellow;
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(785, 479);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 102;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = false;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // VExercise13
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(910, 518);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.R15);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.R14);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.R13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.R12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.R11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.R10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.R9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.R8);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.R7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.R6);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.R5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.R4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.R3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.R2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.R1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.L16);
            this.Controls.Add(this.L15);
            this.Controls.Add(this.L14);
            this.Controls.Add(this.L13);
            this.Controls.Add(this.L12);
            this.Controls.Add(this.L11);
            this.Controls.Add(this.L10);
            this.Controls.Add(this.L9);
            this.Controls.Add(this.L8);
            this.Controls.Add(this.L7);
            this.Controls.Add(this.L6);
            this.Controls.Add(this.L5);
            this.Controls.Add(this.L4);
            this.Controls.Add(this.L3);
            this.Controls.Add(this.L2);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.Title);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VExercise13";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vocabulaire Exercice 13";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.Label L5;
        private System.Windows.Forms.Label L6;
        private System.Windows.Forms.Label L7;
        private System.Windows.Forms.Label L8;
        private System.Windows.Forms.Label L9;
        private System.Windows.Forms.Label L10;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.Label L15;
        private System.Windows.Forms.Label L16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox R1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox R2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox R3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox R4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox R5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox R6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox R7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox R8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox R9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox R10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox R11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox R12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox R13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox R14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox R15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button Continue_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}