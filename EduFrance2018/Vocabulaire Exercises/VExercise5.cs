﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class VExercise5 : Form
    {
        public VExercise5()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Une réponse possible est la suivante:\n2, 4, 1, 3, 8, 6, 7, 9, 5.");
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            VExercise6 f = new VExercise6();
            f.Show();
            this.Hide();
        }
    }
}
