﻿namespace EduFrance2018
{
    partial class MainMenu_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu_Form));
            this.EduFrance_Label = new System.Windows.Forms.Label();
            this.Grammaire_Label = new System.Windows.Forms.Label();
            this.Vocabulaire_Label = new System.Windows.Forms.Label();
            this.RealisePar_Label = new System.Windows.Forms.Label();
            this.Close_Button = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.References_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // EduFrance_Label
            // 
            this.EduFrance_Label.AutoSize = true;
            this.EduFrance_Label.Font = new System.Drawing.Font("Lucida Handwriting", 25F);
            this.EduFrance_Label.Location = new System.Drawing.Point(189, 9);
            this.EduFrance_Label.Name = "EduFrance_Label";
            this.EduFrance_Label.Size = new System.Drawing.Size(339, 44);
            this.EduFrance_Label.TabIndex = 0;
            this.EduFrance_Label.Text = "5MB de Français";
            // 
            // Grammaire_Label
            // 
            this.Grammaire_Label.AutoSize = true;
            this.Grammaire_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Grammaire_Label.Font = new System.Drawing.Font("Lucida Handwriting", 20F);
            this.Grammaire_Label.Location = new System.Drawing.Point(271, 179);
            this.Grammaire_Label.Name = "Grammaire_Label";
            this.Grammaire_Label.Size = new System.Drawing.Size(192, 36);
            this.Grammaire_Label.TabIndex = 1;
            this.Grammaire_Label.Text = "Grammaire";
            this.Grammaire_Label.Click += new System.EventHandler(this.Grammaire_Label_Click);
            this.Grammaire_Label.MouseEnter += new System.EventHandler(this.Grammaire_Label_MouseEnter);
            this.Grammaire_Label.MouseLeave += new System.EventHandler(this.Grammaire_Label_MouseLeave);
            // 
            // Vocabulaire_Label
            // 
            this.Vocabulaire_Label.AutoSize = true;
            this.Vocabulaire_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Vocabulaire_Label.Font = new System.Drawing.Font("Lucida Handwriting", 20F);
            this.Vocabulaire_Label.Location = new System.Drawing.Point(271, 271);
            this.Vocabulaire_Label.Name = "Vocabulaire_Label";
            this.Vocabulaire_Label.Size = new System.Drawing.Size(201, 36);
            this.Vocabulaire_Label.TabIndex = 2;
            this.Vocabulaire_Label.Text = "Vocabulaire";
            this.Vocabulaire_Label.Click += new System.EventHandler(this.Vocabulaire_Label_Click);
            this.Vocabulaire_Label.MouseEnter += new System.EventHandler(this.Vocabulaire_Label_MouseEnter);
            this.Vocabulaire_Label.MouseLeave += new System.EventHandler(this.Vocabulaire_Label_MouseLeave);
            // 
            // RealisePar_Label
            // 
            this.RealisePar_Label.AutoSize = true;
            this.RealisePar_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RealisePar_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.RealisePar_Label.ForeColor = System.Drawing.Color.Navy;
            this.RealisePar_Label.Location = new System.Drawing.Point(589, 25);
            this.RealisePar_Label.Name = "RealisePar_Label";
            this.RealisePar_Label.Size = new System.Drawing.Size(107, 21);
            this.RealisePar_Label.TabIndex = 3;
            this.RealisePar_Label.Text = "Réalisé par";
            this.RealisePar_Label.Click += new System.EventHandler(this.RealisePar_Label_Click);
            this.RealisePar_Label.MouseEnter += new System.EventHandler(this.RealisePar_Label_MouseEnter);
            this.RealisePar_Label.MouseLeave += new System.EventHandler(this.RealisePar_Label_MouseLeave);
            // 
            // Close_Button
            // 
            this.Close_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Close_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Close_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Close_Button.Location = new System.Drawing.Point(612, 308);
            this.Close_Button.Name = "Close_Button";
            this.Close_Button.Size = new System.Drawing.Size(84, 48);
            this.Close_Button.TabIndex = 8;
            this.Close_Button.Text = "À bientôt!";
            this.Close_Button.UseVisualStyleBackColor = true;
            this.Close_Button.Click += new System.EventHandler(this.Close_Button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(22, 125);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 221);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(282, 27);
            this.label1.TabIndex = 11;
            this.label1.Text = "Choisissez une catégorie:";
            // 
            // References_Label
            // 
            this.References_Label.AutoSize = true;
            this.References_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.References_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.References_Label.ForeColor = System.Drawing.Color.Navy;
            this.References_Label.Location = new System.Drawing.Point(12, 25);
            this.References_Label.Name = "References_Label";
            this.References_Label.Size = new System.Drawing.Size(126, 21);
            this.References_Label.TabIndex = 12;
            this.References_Label.Text = "Bibliographie";
            this.References_Label.Click += new System.EventHandler(this.References_Label_Click);
            this.References_Label.MouseEnter += new System.EventHandler(this.References_Label_MouseEnter);
            this.References_Label.MouseLeave += new System.EventHandler(this.References_Label_MouseLeave);
            // 
            // MainMenu_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(708, 368);
            this.Controls.Add(this.References_Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Close_Button);
            this.Controls.Add(this.RealisePar_Label);
            this.Controls.Add(this.Vocabulaire_Label);
            this.Controls.Add(this.Grammaire_Label);
            this.Controls.Add(this.EduFrance_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EduFrance";
            this.Load += new System.EventHandler(this.MainMenu_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label EduFrance_Label;
        private System.Windows.Forms.Label Grammaire_Label;
        private System.Windows.Forms.Label Vocabulaire_Label;
        private System.Windows.Forms.Label RealisePar_Label;
        private System.Windows.Forms.Button Close_Button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label References_Label;
    }
}

