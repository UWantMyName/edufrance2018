﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class MainMenu_Form : Form
    {
        public MainMenu_Form()
        {
            InitializeComponent();
        }

        private void RealisePar_Label_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Credits.docx");
            }
            catch
            {
                MessageBox.Show("Impossible d'ouvrir le fichier.");
            }
        }

        private void Grammaire_Label_Click(object sender, EventArgs e)
        {
            GrammarExercises f = new GrammarExercises();
            f.Show();
            this.Hide();
        }

        private void RealisePar_Label_MouseEnter(object sender, EventArgs e)
        {
            RealisePar_Label.Font = new Font(RealisePar_Label.Font, FontStyle.Bold);
        }

        private void RealisePar_Label_MouseLeave(object sender, EventArgs e)
        {
            RealisePar_Label.Font = new Font(RealisePar_Label.Font, FontStyle.Regular);
        }

        private void Grammaire_Label_MouseEnter(object sender, EventArgs e)
        {
            Grammaire_Label.Font = new Font(Grammaire_Label.Font, FontStyle.Bold);
        }

        private void Grammaire_Label_MouseLeave(object sender, EventArgs e)
        {
            Grammaire_Label.Font = new Font(Grammaire_Label.Font, FontStyle.Regular);
        }

        private void Vocabulaire_Label_MouseEnter(object sender, EventArgs e)
        {
            Vocabulaire_Label.Font = new Font(Vocabulaire_Label.Font, FontStyle.Bold);
        }

        private void Vocabulaire_Label_MouseLeave(object sender, EventArgs e)
        {
            Vocabulaire_Label.Font = new Font(Vocabulaire_Label.Font, FontStyle.Regular);
        }

        private void Close_Button_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Vocabulaire_Label_Click(object sender, EventArgs e)
        {
            Vocabulaire f = new Vocabulaire();
            f.Show();
            this.Hide();
        }

        private void MainMenu_Form_Load(object sender, EventArgs e)
        {
            if (!File.Exists("Credits.docx"))
            {
                MessageBox.Show("Cannot find credits file. Closing applicaton.");
                Application.Exit();
            }

            if (!File.Exists("Bibliographie.docx"))
            {
                MessageBox.Show("Cannot find reference file. Closing application.");
                Application.Exit();
            }
        }

        private void References_Label_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Bibliographie.docx");
            }
            catch
            {
                MessageBox.Show("Cannot open file. Closing application.");
                Application.Exit();
            }
        }

        private void References_Label_MouseEnter(object sender, EventArgs e)
        {
            References_Label.Font = new Font(References_Label.Font, FontStyle.Bold);
        }

        private void References_Label_MouseLeave(object sender, EventArgs e)
        {
            References_Label.Font = new Font(References_Label.Font, FontStyle.Regular);
        }
    }
}
