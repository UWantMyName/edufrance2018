﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018.Grammar_Exercises.Reported_Speech
{
    public partial class RP2 : Form
    {
        public RP2()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            ReportedSpeech f = new ReportedSpeech();
            f.Show();
            this.Hide();
        }
    }
}
