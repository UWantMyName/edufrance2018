﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018.Grammar_Exercises.Reported_Speech
{
    public partial class ReportedSpeech : Form
    {
        public ReportedSpeech()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            GrammarExercises f = new GrammarExercises();
            f.Show();
            this.Hide();
        }

        private void Exercise1_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise1_Label.ForeColor = Color.Blue;
            Exercise1_Label.Font = new Font(Exercise1_Label.Font, FontStyle.Bold);
        }

        private void Exercise1_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise1_Label.ForeColor = Color.Black;
            Exercise1_Label.Font = new Font(Exercise1_Label.Font, FontStyle.Regular);
        }

        private void Exercise2_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise2_Label.ForeColor = Color.Blue;
            Exercise2_Label.Font = new Font(Exercise2_Label.Font, FontStyle.Bold);
        }

        private void Exercise2_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise2_Label.ForeColor = Color.Black;
            Exercise2_Label.Font = new Font(Exercise2_Label.Font, FontStyle.Regular);
        }

        private void Exercise3_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise3_Label.ForeColor = Color.Blue;
            Exercise3_Label.Font = new Font(Exercise3_Label.Font, FontStyle.Bold);
        }

        private void Exercise3_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise3_Label.ForeColor = Color.Black;
            Exercise3_Label.Font = new Font(Exercise3_Label.Font, FontStyle.Regular);
        }

        private void Exercise1_Label_Click(object sender, EventArgs e)
        {
            RP1 f = new RP1();
            f.Show();
            this.Hide();
        }

        private void Exercise2_Label_Click(object sender, EventArgs e)
        {
            RP2 f = new RP2();
            f.Show();
            this.Hide();
        }

        private void Exercise3_Label_Click(object sender, EventArgs e)
        {
            RP3 f = new RP3();
            f.Show();
            this.Hide();
        }

        private void Theory_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Discours direct.doc");
            }
            catch
            {
                MessageBox.Show("Impossible d'ouvrir le fichier.");
            }
        }

        private void Theory_MouseEnter(object sender, EventArgs e)
        {
            Theory.ForeColor = Color.Blue;
            Theory.Font = new Font(Theory.Font, FontStyle.Bold);
        }

        private void Theory_MouseLeave(object sender, EventArgs e)
        {
            Theory.ForeColor = Color.Black;
            Theory.Font = new Font(Theory.Font, FontStyle.Regular);
        }
    }
}
