﻿namespace EduFrance2018.Grammar_Exercises.Reported_Speech
{
    partial class ReportedSpeech
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportedSpeech));
            this.Ex1_Label = new System.Windows.Forms.Label();
            this.Exercise1_Label = new System.Windows.Forms.Label();
            this.Exercise2_Label = new System.Windows.Forms.Label();
            this.Exercise3_Label = new System.Windows.Forms.Label();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Theory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Ex1_Label
            // 
            this.Ex1_Label.AutoSize = true;
            this.Ex1_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ex1_Label.ForeColor = System.Drawing.Color.Black;
            this.Ex1_Label.Location = new System.Drawing.Point(67, 9);
            this.Ex1_Label.Name = "Ex1_Label";
            this.Ex1_Label.Size = new System.Drawing.Size(253, 27);
            this.Ex1_Label.TabIndex = 3;
            this.Ex1_Label.Text = "Choisissez un exercice";
            // 
            // Exercise1_Label
            // 
            this.Exercise1_Label.AutoSize = true;
            this.Exercise1_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercise1_Label.Font = new System.Drawing.Font("Lucida Handwriting", 14F);
            this.Exercise1_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercise1_Label.Location = new System.Drawing.Point(12, 125);
            this.Exercise1_Label.Name = "Exercise1_Label";
            this.Exercise1_Label.Size = new System.Drawing.Size(115, 24);
            this.Exercise1_Label.TabIndex = 7;
            this.Exercise1_Label.Text = "Exercice 1";
            this.Exercise1_Label.Click += new System.EventHandler(this.Exercise1_Label_Click);
            this.Exercise1_Label.MouseEnter += new System.EventHandler(this.Exercise1_Label_MouseEnter);
            this.Exercise1_Label.MouseLeave += new System.EventHandler(this.Exercise1_Label_MouseLeave);
            // 
            // Exercise2_Label
            // 
            this.Exercise2_Label.AutoSize = true;
            this.Exercise2_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercise2_Label.Font = new System.Drawing.Font("Lucida Handwriting", 14F);
            this.Exercise2_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercise2_Label.Location = new System.Drawing.Point(12, 187);
            this.Exercise2_Label.Name = "Exercise2_Label";
            this.Exercise2_Label.Size = new System.Drawing.Size(115, 24);
            this.Exercise2_Label.TabIndex = 8;
            this.Exercise2_Label.Text = "Exercice 2";
            this.Exercise2_Label.Click += new System.EventHandler(this.Exercise2_Label_Click);
            this.Exercise2_Label.MouseEnter += new System.EventHandler(this.Exercise2_Label_MouseEnter);
            this.Exercise2_Label.MouseLeave += new System.EventHandler(this.Exercise2_Label_MouseLeave);
            // 
            // Exercise3_Label
            // 
            this.Exercise3_Label.AutoSize = true;
            this.Exercise3_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exercise3_Label.Font = new System.Drawing.Font("Lucida Handwriting", 14F);
            this.Exercise3_Label.ForeColor = System.Drawing.Color.Black;
            this.Exercise3_Label.Location = new System.Drawing.Point(12, 248);
            this.Exercise3_Label.Name = "Exercise3_Label";
            this.Exercise3_Label.Size = new System.Drawing.Size(115, 24);
            this.Exercise3_Label.TabIndex = 9;
            this.Exercise3_Label.Text = "Exercice 3";
            this.Exercise3_Label.Click += new System.EventHandler(this.Exercise3_Label_Click);
            this.Exercise3_Label.MouseEnter += new System.EventHandler(this.Exercise3_Label_MouseEnter);
            this.Exercise3_Label.MouseLeave += new System.EventHandler(this.Exercise3_Label_MouseLeave);
            // 
            // Back_Button
            // 
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(289, 299);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 10;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // Theory
            // 
            this.Theory.AutoSize = true;
            this.Theory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Theory.Font = new System.Drawing.Font("Lucida Handwriting", 14F);
            this.Theory.ForeColor = System.Drawing.Color.Black;
            this.Theory.Location = new System.Drawing.Point(12, 64);
            this.Theory.Name = "Theory";
            this.Theory.Size = new System.Drawing.Size(206, 24);
            this.Theory.TabIndex = 11;
            this.Theory.Text = "Notions théoriques";
            this.Theory.Click += new System.EventHandler(this.Theory_Click);
            this.Theory.MouseEnter += new System.EventHandler(this.Theory_MouseEnter);
            this.Theory.MouseLeave += new System.EventHandler(this.Theory_MouseLeave);
            // 
            // ReportedSpeech
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(414, 338);
            this.Controls.Add(this.Theory);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.Exercise3_Label);
            this.Controls.Add(this.Exercise2_Label);
            this.Controls.Add(this.Exercise1_Label);
            this.Controls.Add(this.Ex1_Label);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportedSpeech";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Le passage du discours direct au discours indirect";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Ex1_Label;
        private System.Windows.Forms.Label Exercise1_Label;
        private System.Windows.Forms.Label Exercise2_Label;
        private System.Windows.Forms.Label Exercise3_Label;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Label Theory;
    }
}