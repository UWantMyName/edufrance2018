﻿namespace EduFrance2018
{
    partial class GrammarExercises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GrammarExercises));
            this.Choose_Label = new System.Windows.Forms.Label();
            this.Verbs_Label = new System.Windows.Forms.Label();
            this.ReportedSpeech_Label = new System.Windows.Forms.Label();
            this.Back_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Choose_Label
            // 
            this.Choose_Label.AutoSize = true;
            this.Choose_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Choose_Label.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Choose_Label.Location = new System.Drawing.Point(71, 9);
            this.Choose_Label.Name = "Choose_Label";
            this.Choose_Label.Size = new System.Drawing.Size(276, 27);
            this.Choose_Label.TabIndex = 1;
            this.Choose_Label.Text = "Choisissez une catégorie";
            // 
            // Verbs_Label
            // 
            this.Verbs_Label.AutoSize = true;
            this.Verbs_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Verbs_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F);
            this.Verbs_Label.ForeColor = System.Drawing.Color.Black;
            this.Verbs_Label.Location = new System.Drawing.Point(12, 62);
            this.Verbs_Label.Name = "Verbs_Label";
            this.Verbs_Label.Size = new System.Drawing.Size(373, 27);
            this.Verbs_Label.TabIndex = 2;
            this.Verbs_Label.Text = "L\'imparfait et le passé composé";
            this.Verbs_Label.Click += new System.EventHandler(this.Verbs_Label_Click);
            this.Verbs_Label.MouseEnter += new System.EventHandler(this.Verbs_Label_MouseEnter);
            this.Verbs_Label.MouseLeave += new System.EventHandler(this.Verbs_Label_MouseLeave);
            // 
            // ReportedSpeech_Label
            // 
            this.ReportedSpeech_Label.AutoSize = true;
            this.ReportedSpeech_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportedSpeech_Label.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F);
            this.ReportedSpeech_Label.ForeColor = System.Drawing.Color.Black;
            this.ReportedSpeech_Label.Location = new System.Drawing.Point(12, 116);
            this.ReportedSpeech_Label.Name = "ReportedSpeech_Label";
            this.ReportedSpeech_Label.Size = new System.Drawing.Size(248, 27);
            this.ReportedSpeech_Label.TabIndex = 3;
            this.ReportedSpeech_Label.Text = "Le discours rapporté";
            this.ReportedSpeech_Label.Click += new System.EventHandler(this.ReportedSpeech_Label_Click);
            this.ReportedSpeech_Label.MouseEnter += new System.EventHandler(this.ReportedSpeech_Label_MouseEnter);
            this.ReportedSpeech_Label.MouseLeave += new System.EventHandler(this.ReportedSpeech_Label_MouseLeave);
            // 
            // Back_Button
            // 
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(303, 162);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 4;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // GrammarExercises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(428, 201);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.ReportedSpeech_Label);
            this.Controls.Add(this.Verbs_Label);
            this.Controls.Add(this.Choose_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GrammarExercises";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Activités de Grammaire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Choose_Label;
        private System.Windows.Forms.Label Verbs_Label;
        private System.Windows.Forms.Label ReportedSpeech_Label;
        private System.Windows.Forms.Button Back_Button;
    }
}