﻿namespace EduFrance2018
{
    partial class GExercise1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GExercise1));
            this.Task_Label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Answer1_ComboBox = new System.Windows.Forms.ComboBox();
            this.Answer2_ComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Answer3_ComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Answer4_ComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Answer5_ComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Answer6_ComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Answer7_ComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Answer8_ComboBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Answer9_ComboBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Answer10_ComboBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Check_Button = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.Answer11_ComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Task_Label
            // 
            this.Task_Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Task_Label.AutoSize = true;
            this.Task_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 16F, System.Drawing.FontStyle.Underline);
            this.Task_Label.Location = new System.Drawing.Point(225, 9);
            this.Task_Label.Name = "Task_Label";
            this.Task_Label.Size = new System.Drawing.Size(350, 28);
            this.Task_Label.TabIndex = 2;
            this.Task_Label.Text = "Mettre le temps qui convient";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label1.Location = new System.Drawing.Point(12, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "a) Il";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label2.Location = new System.Drawing.Point(226, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "à Lyon quand ";
            // 
            // Answer1_ComboBox
            // 
            this.Answer1_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer1_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer1_ComboBox.FormattingEnabled = true;
            this.Answer1_ComboBox.Items.AddRange(new object[] {
            "n\'habitait plus",
            "n\'a plus habité"});
            this.Answer1_ComboBox.Location = new System.Drawing.Point(62, 81);
            this.Answer1_ComboBox.Name = "Answer1_ComboBox";
            this.Answer1_ComboBox.Size = new System.Drawing.Size(158, 29);
            this.Answer1_ComboBox.TabIndex = 5;
            // 
            // Answer2_ComboBox
            // 
            this.Answer2_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer2_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer2_ComboBox.FormattingEnabled = true;
            this.Answer2_ComboBox.Items.AddRange(new object[] {
            "j\'y allais",
            "j\'y suis allé"});
            this.Answer2_ComboBox.Location = new System.Drawing.Point(364, 81);
            this.Answer2_ComboBox.Name = "Answer2_ComboBox";
            this.Answer2_ComboBox.Size = new System.Drawing.Size(124, 29);
            this.Answer2_ComboBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label3.Location = new System.Drawing.Point(494, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "pour étudier.";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label4.Location = new System.Drawing.Point(12, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "b) Nous";
            // 
            // Answer3_ComboBox
            // 
            this.Answer3_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer3_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer3_ComboBox.FormattingEnabled = true;
            this.Answer3_ComboBox.Items.AddRange(new object[] {
            "ne réservions pas encore",
            "n\'avons pas encore réservé"});
            this.Answer3_ComboBox.Location = new System.Drawing.Point(92, 147);
            this.Answer3_ComboBox.Name = "Answer3_ComboBox";
            this.Answer3_ComboBox.Size = new System.Drawing.Size(266, 29);
            this.Answer3_ComboBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label5.Location = new System.Drawing.Point(364, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 21);
            this.label5.TabIndex = 10;
            this.label5.Text = "nos billets pour le Japon.";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label6.Location = new System.Drawing.Point(12, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 21);
            this.label6.TabIndex = 11;
            this.label6.Text = "c) Tu";
            // 
            // Answer4_ComboBox
            // 
            this.Answer4_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer4_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer4_ComboBox.FormattingEnabled = true;
            this.Answer4_ComboBox.Items.AddRange(new object[] {
            "visitais déjà",
            "as déjà visité"});
            this.Answer4_ComboBox.Location = new System.Drawing.Point(70, 207);
            this.Answer4_ComboBox.Name = "Answer4_ComboBox";
            this.Answer4_ComboBox.Size = new System.Drawing.Size(150, 29);
            this.Answer4_ComboBox.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label7.Location = new System.Drawing.Point(226, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "le Québec?";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label8.Location = new System.Drawing.Point(12, 270);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 21);
            this.label8.TabIndex = 14;
            this.label8.Text = "d) Il";
            // 
            // Answer5_ComboBox
            // 
            this.Answer5_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer5_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer5_ComboBox.FormattingEnabled = true;
            this.Answer5_ComboBox.Items.AddRange(new object[] {
            "voulait toujours",
            "a toujours voulu"});
            this.Answer5_ComboBox.Location = new System.Drawing.Point(70, 267);
            this.Answer5_ComboBox.Name = "Answer5_ComboBox";
            this.Answer5_ComboBox.Size = new System.Drawing.Size(172, 29);
            this.Answer5_ComboBox.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label9.Location = new System.Drawing.Point(248, 270);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(368, 21);
            this.label9.TabIndex = 16;
            this.label9.Text = "apprendre une nouvelle langue étrangère.";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label10.Location = new System.Drawing.Point(12, 329);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 21);
            this.label10.TabIndex = 17;
            this.label10.Text = "e) Sa soeur";
            // 
            // Answer6_ComboBox
            // 
            this.Answer6_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer6_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer6_ComboBox.FormattingEnabled = true;
            this.Answer6_ComboBox.Items.AddRange(new object[] {
            "ne votait jamais",
            "n\'a jamais voté"});
            this.Answer6_ComboBox.Location = new System.Drawing.Point(118, 326);
            this.Answer6_ComboBox.Name = "Answer6_ComboBox";
            this.Answer6_ComboBox.Size = new System.Drawing.Size(177, 29);
            this.Answer6_ComboBox.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label11.Location = new System.Drawing.Point(301, 329);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(203, 21);
            this.label11.TabIndex = 19;
            this.label11.Text = "car elle n`a pas 18 ans.";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label12.Location = new System.Drawing.Point(12, 383);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 21);
            this.label12.TabIndex = 20;
            this.label12.Text = "f) Nous";
            // 
            // Answer7_ComboBox
            // 
            this.Answer7_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer7_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer7_ComboBox.FormattingEnabled = true;
            this.Answer7_ComboBox.Items.AddRange(new object[] {
            "aimions",
            "avons aimé "});
            this.Answer7_ComboBox.Location = new System.Drawing.Point(92, 380);
            this.Answer7_ComboBox.Name = "Answer7_ComboBox";
            this.Answer7_ComboBox.Size = new System.Drawing.Size(137, 29);
            this.Answer7_ComboBox.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label13.Location = new System.Drawing.Point(235, 383);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(431, 21);
            this.label13.TabIndex = 22;
            this.label13.Text = "construire des cabanes dans le jardin quand nous ";
            // 
            // Answer8_ComboBox
            // 
            this.Answer8_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer8_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer8_ComboBox.FormattingEnabled = true;
            this.Answer8_ComboBox.Items.AddRange(new object[] {
            "étions",
            "avons été"});
            this.Answer8_ComboBox.Location = new System.Drawing.Point(662, 380);
            this.Answer8_ComboBox.Name = "Answer8_ComboBox";
            this.Answer8_ComboBox.Size = new System.Drawing.Size(137, 29);
            this.Answer8_ComboBox.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label14.Location = new System.Drawing.Point(805, 383);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 21);
            this.label14.TabIndex = 24;
            this.label14.Text = "enfants.";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label15.Location = new System.Drawing.Point(12, 445);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 21);
            this.label15.TabIndex = 25;
            this.label15.Text = "g) Je";
            // 
            // Answer9_ComboBox
            // 
            this.Answer9_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer9_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer9_ComboBox.FormattingEnabled = true;
            this.Answer9_ComboBox.Items.AddRange(new object[] {
            "ne prenais plus",
            "n\'ai plus pris"});
            this.Answer9_ComboBox.Location = new System.Drawing.Point(70, 442);
            this.Answer9_ComboBox.Name = "Answer9_ComboBox";
            this.Answer9_ComboBox.Size = new System.Drawing.Size(177, 29);
            this.Answer9_ComboBox.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label16.Location = new System.Drawing.Point(253, 445);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(326, 21);
            this.label16.TabIndex = 27;
            this.label16.Text = "le bus pour aller à l`université quand";
            // 
            // Answer10_ComboBox
            // 
            this.Answer10_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer10_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer10_ComboBox.FormattingEnabled = true;
            this.Answer10_ComboBox.Items.AddRange(new object[] {
            "j`achetais",
            "j\'ai acheté"});
            this.Answer10_ComboBox.Location = new System.Drawing.Point(594, 437);
            this.Answer10_ComboBox.Name = "Answer10_ComboBox";
            this.Answer10_ComboBox.Size = new System.Drawing.Size(137, 29);
            this.Answer10_ComboBox.TabIndex = 28;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label17.Location = new System.Drawing.Point(737, 442);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 21);
            this.label17.TabIndex = 29;
            this.label17.Text = "mon vélo.";
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(767, 554);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 30;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(631, 554);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(113, 27);
            this.Check_Button.TabIndex = 31;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label18.Location = new System.Drawing.Point(12, 496);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(141, 21);
            this.label18.TabIndex = 32;
            this.label18.Text = "h) Les employés";
            // 
            // Answer11_ComboBox
            // 
            this.Answer11_ComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Answer11_ComboBox.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Answer11_ComboBox.FormattingEnabled = true;
            this.Answer11_ComboBox.Items.AddRange(new object[] {
            "n\'étaient jamais augmentés",
            "n\'ont jamais été augmentés"});
            this.Answer11_ComboBox.Location = new System.Drawing.Point(159, 488);
            this.Answer11_ComboBox.Name = "Answer11_ComboBox";
            this.Answer11_ComboBox.Size = new System.Drawing.Size(329, 29);
            this.Answer11_ComboBox.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.label19.Location = new System.Drawing.Point(494, 491);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(347, 21);
            this.label19.TabIndex = 34;
            this.label19.Text = "dans cette entreprise depuis sa creation";
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(498, 553);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 92;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // GExercise1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.ClientSize = new System.Drawing.Size(892, 589);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Answer11_ComboBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.Answer10_ComboBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Answer9_ComboBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Answer8_ComboBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Answer7_ComboBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Answer6_ComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Answer5_ComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Answer4_ComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Answer3_ComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Answer2_ComboBox);
            this.Controls.Add(this.Answer1_ComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Task_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GExercise1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grammaire Exercice 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Task_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Answer1_ComboBox;
        private System.Windows.Forms.ComboBox Answer2_ComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Answer3_ComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Answer4_ComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Answer5_ComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Answer6_ComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Answer7_ComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox Answer8_ComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox Answer9_ComboBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox Answer10_ComboBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Answer11_ComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button Continue_Button;
    }
}