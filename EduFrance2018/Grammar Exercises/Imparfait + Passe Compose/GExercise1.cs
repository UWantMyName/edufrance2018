﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class GExercise1 : Form
    {
        public GExercise1()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            ImparfaitPC f = new ImparfaitPC();
            f.Show();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (Answer1_ComboBox.Text == "n'habitait plus") Answer1_ComboBox.BackColor = Color.LightGreen;
            else Answer1_ComboBox.BackColor = Color.Red;

            if (Answer2_ComboBox.Text == "j'y suis allé") Answer2_ComboBox.BackColor = Color.LightGreen;
            else Answer2_ComboBox.BackColor = Color.Red;

            if (Answer11_ComboBox.Text == "n'ont jamais été augmentés") Answer11_ComboBox.BackColor = Color.LightGreen;
            else Answer11_ComboBox.BackColor = Color.Red;

            if (Answer3_ComboBox.Text == "n'avons pas encore réservé") Answer3_ComboBox.BackColor = Color.LightGreen;
            else Answer3_ComboBox.BackColor = Color.Red;

            if (Answer4_ComboBox.Text == "as déjà visité") Answer4_ComboBox.BackColor = Color.LightGreen;
            else Answer4_ComboBox.BackColor = Color.Red;

            if (Answer5_ComboBox.Text == "a toujours voulu") Answer5_ComboBox.BackColor = Color.LightGreen;
            else Answer5_ComboBox.BackColor = Color.Red;

            if (Answer6_ComboBox.Text == "n'a jamais voté") Answer6_ComboBox.BackColor = Color.LightGreen;
            else Answer6_ComboBox.BackColor = Color.Red;

            if (Answer7_ComboBox.Text == "aimions") Answer7_ComboBox.BackColor = Color.LightGreen;
            else Answer7_ComboBox.BackColor = Color.Red;

            if (Answer8_ComboBox.Text == "étions") Answer8_ComboBox.BackColor = Color.LightGreen;
            else Answer8_ComboBox.BackColor = Color.Red;

            if (Answer9_ComboBox.Text == "n'ai plus pris") Answer9_ComboBox.BackColor = Color.LightGreen;
            else Answer9_ComboBox.BackColor = Color.Red;

            if (Answer10_ComboBox.Text == "j'ai acheté") Answer10_ComboBox.BackColor = Color.LightGreen;
            else Answer10_ComboBox.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            GExercise2 f = new GExercise2();
            f.Show();
            this.Hide();
        }
    }
}
