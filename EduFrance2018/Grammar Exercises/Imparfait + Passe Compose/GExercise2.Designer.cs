﻿namespace EduFrance2018
{
    partial class GExercise2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GExercise2));
            this.Ex1_Label = new System.Windows.Forms.Label();
            this.Text_Label = new System.Windows.Forms.Label();
            this.Question_Label = new System.Windows.Forms.Label();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ex1_Label
            // 
            this.Ex1_Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Ex1_Label.AutoSize = true;
            this.Ex1_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ex1_Label.ForeColor = System.Drawing.Color.Black;
            this.Ex1_Label.Location = new System.Drawing.Point(189, 9);
            this.Ex1_Label.Name = "Ex1_Label";
            this.Ex1_Label.Size = new System.Drawing.Size(394, 27);
            this.Ex1_Label.TabIndex = 3;
            this.Ex1_Label.Text = "Racontez des événements au passé";
            // 
            // Text_Label
            // 
            this.Text_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Text_Label.AutoSize = true;
            this.Text_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text_Label.ForeColor = System.Drawing.Color.Black;
            this.Text_Label.Location = new System.Drawing.Point(12, 96);
            this.Text_Label.Name = "Text_Label";
            this.Text_Label.Size = new System.Drawing.Size(110, 27);
            this.Text_Label.TabIndex = 4;
            this.Text_Label.Text = "Example";
            this.Text_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Question_Label
            // 
            this.Question_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Question_Label.AutoSize = true;
            this.Question_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Question_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 30F);
            this.Question_Label.ForeColor = System.Drawing.Color.Navy;
            this.Question_Label.Location = new System.Drawing.Point(745, 9);
            this.Question_Label.Name = "Question_Label";
            this.Question_Label.Size = new System.Drawing.Size(43, 52);
            this.Question_Label.TabIndex = 5;
            this.Question_Label.Text = "?";
            this.Question_Label.Click += new System.EventHandler(this.Question_Label_Click);
            this.Question_Label.MouseEnter += new System.EventHandler(this.Question_Label_MouseEnter);
            this.Question_Label.MouseLeave += new System.EventHandler(this.Question_Label_MouseLeave);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(675, 411);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 6;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(530, 411);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 92;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // GExercise2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.Question_Label);
            this.Controls.Add(this.Text_Label);
            this.Controls.Add(this.Ex1_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GExercise2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grammaire Exercice 2";
            this.Load += new System.EventHandler(this.GExercise2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Ex1_Label;
        private System.Windows.Forms.Label Text_Label;
        private System.Windows.Forms.Label Question_Label;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button Continue_Button;
    }
}