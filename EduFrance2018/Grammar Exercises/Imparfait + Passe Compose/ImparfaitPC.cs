﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class ImparfaitPC : Form
    {
        public ImparfaitPC()
        {
            InitializeComponent();
        }

        private void Exercise1_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise1_Label.Font = new Font(Exercise1_Label.Font, FontStyle.Bold);
        }

        private void Exercise1_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise1_Label.Font = new Font(Exercise1_Label.Font, FontStyle.Regular);
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            GrammarExercises f = new GrammarExercises();
            this.Hide();
            f.Show();
        }

        private void Exercise1_Label_Click(object sender, EventArgs e)
        {
            GExercise0 f = new GExercise0();
            f.Show();
            this.Hide();
        }

        private void Exercise2_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise2_Label.Font = new Font(Exercise2_Label.Font, FontStyle.Bold);
        }

        private void Exercise2_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise2_Label.Font = new Font(Exercise2_Label.Font, FontStyle.Regular);
        }

        private void Exercise2_Label_Click(object sender, EventArgs e)
        {
            GExercise1 f = new GExercise1();
            this.Hide();
            f.Show();
        }

        private void Exercise3_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise3_Label.Font = new Font(Exercise3_Label.Font, FontStyle.Bold);
        }

        private void Exercise3_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise3_Label.Font = new Font(Exercise3_Label.Font, FontStyle.Regular);
        }

        private void Exercise4_Label_MouseEnter(object sender, EventArgs e)
        {
            Exercise4_Label.Font = new Font(Exercise4_Label.Font, FontStyle.Bold);
        }

        private void Exercise4_Label_MouseLeave(object sender, EventArgs e)
        {
            Exercise4_Label.Font = new Font(Exercise4_Label.Font, FontStyle.Regular);
        }

        private void Exercise3_Label_Click(object sender, EventArgs e)
        {
            GExercise2 f = new GExercise2();
            this.Hide();
            f.Show();
        }

        private void Exercise4_Label_Click(object sender, EventArgs e)
        {
            GExercise3 f = new GExercise3();
            f.Show();
            this.Hide();
        }
    }
}
