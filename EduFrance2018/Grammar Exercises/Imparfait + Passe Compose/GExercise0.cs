﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class GExercise0 : Form
    {
        public GExercise0()
        {
            InitializeComponent();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            ImparfaitPC f = new ImparfaitPC();
            this.Hide();
            f.Show();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (Options1_ComboBox.Text == "Situation passée qui n'existe plus.") Options1_ComboBox.BackColor = Color.LightGreen;
            else Options1_ComboBox.BackColor = Color.Red;

            if (Options2_ComboBox.Text == "Action ponctuelle dans le passé.") Options2_ComboBox.BackColor = Color.LightGreen;
            else Options2_ComboBox.BackColor = Color.Red;

            if (Options3_ComboBox.Text == "Action réalisée ou non réalisée dans le passé.") Options3_ComboBox.BackColor = Color.LightGreen;
            else Options3_ComboBox.BackColor = Color.Red;

            if (Options4_ComboBox.Text == "Habitude dans le passé.") Options4_ComboBox.BackColor = Color.LightGreen;
            else Options4_ComboBox.BackColor = Color.Red;

            if (Options5_ComboBox.Text == "Événement passé.") Options5_ComboBox.BackColor = Color.LightGreen;
            else Options5_ComboBox.BackColor = Color.Red;

            if (Options6_ComboBox.Text == "Situation passée qui a cessé d'exister.") Options6_ComboBox.BackColor = Color.LightGreen;
            else Options6_ComboBox.BackColor = Color.Red;

            if (Options7_ComboBox.Text == "Contexte d'une histoire du passé.") Options7_ComboBox.BackColor = Color.LightGreen;
            else Options7_ComboBox.BackColor = Color.Red;

            if (Options8_ComboBox.Text == "Action ponctuelle dans le passé.") Options8_ComboBox.BackColor = Color.LightGreen;
            else Options8_ComboBox.BackColor = Color.Red;

            if (Options9_ComboBox.Text == "Action passée qui modifie une situation.") Options9_ComboBox.BackColor = Color.LightGreen;
            else Options9_ComboBox.BackColor = Color.Red;

            if (Options10_ComboBox.Text == "Habitude dans le passé.") Options10_ComboBox.BackColor = Color.LightGreen;
            else Options10_ComboBox.BackColor = Color.Red;

            Continue_Button.Show();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            GExercise1 f = new GExercise1();
            f.Show();
            this.Hide();
        }
    }
}
