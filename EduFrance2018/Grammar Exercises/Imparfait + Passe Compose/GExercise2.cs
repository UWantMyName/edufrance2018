﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class GExercise2 : Form
    {
        public GExercise2()
        {
            InitializeComponent();
        }

        private void GExercise2_Load(object sender, EventArgs e)
        {
            string text = "   C''est le mois d'août. Aujourd'hui, il fait un temps magnifique.\n";
            text += "Je suis assise sur un banc dans un parc. C'est l'heure du déjeuner.\n";
            text += "Je regarde les enfants s'amuser. Je reconnais une fille de mon lycée.\n";
            text += "Elle joue avec son petit frère. Elle a à peu près le même âge que moi.\n";
            text += "Elle est blonde et mince. Par erreur, son petit frère lance un ballon\n";
            text += "dans ma direction et j'ai à peine le temps de l'éviter. En courant,\n";
            text += "elle se dirige vers moi pour s'excuser. Elle dit son nom et nous\n";
            text += "échangeons un sourire. Nous commençons à parler et voilà le début\n";
            text += "d'une belle amitié.";
            Text_Label.Text = text;
        }

        private void Question_Label_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cet exercice a plusieurs façons de le résoudre.\nVous pouvez vérifier avec votre enseignant ou votre enseignant.");
        }

        private void Question_Label_MouseEnter(object sender, EventArgs e)
        {
            Question_Label.Font = new Font(Question_Label.Font, FontStyle.Bold);
        }

        private void Question_Label_MouseLeave(object sender, EventArgs e)
        {
            Question_Label.Font = new Font(Question_Label.Font, FontStyle.Regular);
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            ImparfaitPC f = new ImparfaitPC();
            f.Show();
            this.Hide();
        }

        private void Continue_Button_Click(object sender, EventArgs e)
        {
            GExercise3 f = new GExercise3();
            f.Show();
            this.Hide();
        }
    }
}
