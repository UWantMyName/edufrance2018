﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class GExercise3 : Form
    {

        public GExercise3()
        {
            InitializeComponent();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            ImparfaitPC f = new ImparfaitPC();
            f.Show();
            this.Hide();
        }

        private void Check_Button_Click(object sender, EventArgs e)
        {
            if (Answer1_ComboBox.Text == "je suis arrivé") Answer1_ComboBox.BackColor = Color.LightGreen;
            else Answer1_ComboBox.BackColor = Color.Red;

            if (Answer2_ComboBox.Text == "j'ai sorti") Answer2_ComboBox.BackColor = Color.LightGreen;
            else Answer2_ComboBox.BackColor = Color.Red;

            if (Answer3_ComboBox.Text == "j'ai commencé") Answer3_ComboBox.BackColor = Color.LightGreen;
            else Answer3_ComboBox.BackColor = Color.Red;

            if (Answer4_ComboBox.Text == "j'ai decidé") Answer4_ComboBox.BackColor = Color.LightGreen;
            else Answer4_ComboBox.BackColor = Color.Red;

            if (Answer5_ComboBox.Text == "Je suis sorti") Answer5_ComboBox.BackColor = Color.LightGreen;
            else Answer5_ComboBox.BackColor = Color.Red;

            if (Answer6_ComboBox.Text == "je suis monté") Answer6_ComboBox.BackColor = Color.LightGreen;
            else Answer6_ComboBox.BackColor = Color.Red;

            if (Answer7_ComboBox.Text == "était") Answer7_ComboBox.BackColor = Color.LightGreen;
            else Answer7_ComboBox.BackColor = Color.Red;

            if (Answer8_ComboBox.Text == "j'ai été") Answer8_ComboBox.BackColor = Color.LightGreen;
            else Answer8_ComboBox.BackColor = Color.Red;

            if (Answer9_ComboBox.Text == "j'ai traversé") Answer9_ComboBox.BackColor = Color.LightGreen;
            else Answer9_ComboBox.BackColor = Color.Red;

            if (Answer10_ComboBox.Text == "a roulé") Answer10_ComboBox.BackColor = Color.LightGreen;
            else Answer10_ComboBox.BackColor = Color.Red;

            if (Answer11_ComboBox.Text == "je me suis retrouvé") Answer11_ComboBox.BackColor = Color.LightGreen;
            else Answer11_ComboBox.BackColor = Color.Red;

            if (Answer12_ComboBox.Text == "j'ai pu") Answer12_ComboBox.BackColor = Color.LightGreen;
            else Answer12_ComboBox.BackColor = Color.Red;

            if (Answer13_ComboBox.Text == "m'a proposé") Answer13_ComboBox.BackColor = Color.LightGreen;
            else Answer13_ComboBox.BackColor = Color.Red;

            if (Answer14_ComboBox.Text == "Nous sommes arrivés") Answer14_ComboBox.BackColor = Color.LightGreen;
            else Answer14_ComboBox.BackColor = Color.Red;

            if (Answer15_ComboBox.Text == "Il était") Answer15_ComboBox.BackColor = Color.LightGreen;
            else Answer15_ComboBox.BackColor = Color.Red;

            if (Answer16_ComboBox.Text == "j'ai prévenu") Answer16_ComboBox.BackColor = Color.LightGreen;
            else Answer16_ComboBox.BackColor = Color.Red;

            if (Answer17_ComboBox.Text == "je ne suis pas allé") Answer17_ComboBox.BackColor = Color.LightGreen;
            else Answer17_ComboBox.BackColor = Color.Red;
        }
    }
}
