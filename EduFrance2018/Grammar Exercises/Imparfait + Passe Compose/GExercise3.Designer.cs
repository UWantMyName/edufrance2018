﻿namespace EduFrance2018
{
    partial class GExercise3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GExercise3));
            this.Task_Label = new System.Windows.Forms.Label();
            this.Text_Label = new System.Windows.Forms.Label();
            this.Answer1_ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Answer2_ComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Answer3_ComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Answer4_ComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Answer5_ComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Answer6_ComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Answer7_ComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Answer8_ComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Answer9_ComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Answer10_ComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Answer11_ComboBox = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Answer12_ComboBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Answer13_ComboBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Answer14_ComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Answer15_ComboBox = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Answer16_ComboBox = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Answer17_ComboBox = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Task_Label
            // 
            resources.ApplyResources(this.Task_Label, "Task_Label");
            this.Task_Label.Name = "Task_Label";
            // 
            // Text_Label
            // 
            resources.ApplyResources(this.Text_Label, "Text_Label");
            this.Text_Label.ForeColor = System.Drawing.Color.Black;
            this.Text_Label.Name = "Text_Label";
            // 
            // Answer1_ComboBox
            // 
            resources.ApplyResources(this.Answer1_ComboBox, "Answer1_ComboBox");
            this.Answer1_ComboBox.FormattingEnabled = true;
            this.Answer1_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer1_ComboBox.Items"),
            resources.GetString("Answer1_ComboBox.Items1"),
            resources.GetString("Answer1_ComboBox.Items2")});
            this.Answer1_ComboBox.Name = "Answer1_ComboBox";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Name = "label1";
            // 
            // Answer2_ComboBox
            // 
            resources.ApplyResources(this.Answer2_ComboBox, "Answer2_ComboBox");
            this.Answer2_ComboBox.FormattingEnabled = true;
            this.Answer2_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer2_ComboBox.Items"),
            resources.GetString("Answer2_ComboBox.Items1"),
            resources.GetString("Answer2_ComboBox.Items2")});
            this.Answer2_ComboBox.Name = "Answer2_ComboBox";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Name = "label3";
            // 
            // Answer3_ComboBox
            // 
            resources.ApplyResources(this.Answer3_ComboBox, "Answer3_ComboBox");
            this.Answer3_ComboBox.FormattingEnabled = true;
            this.Answer3_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer3_ComboBox.Items"),
            resources.GetString("Answer3_ComboBox.Items1"),
            resources.GetString("Answer3_ComboBox.Items2")});
            this.Answer3_ComboBox.Name = "Answer3_ComboBox";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Name = "label6";
            // 
            // Answer4_ComboBox
            // 
            resources.ApplyResources(this.Answer4_ComboBox, "Answer4_ComboBox");
            this.Answer4_ComboBox.FormattingEnabled = true;
            this.Answer4_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer4_ComboBox.Items"),
            resources.GetString("Answer4_ComboBox.Items1"),
            resources.GetString("Answer4_ComboBox.Items2")});
            this.Answer4_ComboBox.Name = "Answer4_ComboBox";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Name = "label7";
            // 
            // Answer5_ComboBox
            // 
            resources.ApplyResources(this.Answer5_ComboBox, "Answer5_ComboBox");
            this.Answer5_ComboBox.FormattingEnabled = true;
            this.Answer5_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer5_ComboBox.Items"),
            resources.GetString("Answer5_ComboBox.Items1"),
            resources.GetString("Answer5_ComboBox.Items2")});
            this.Answer5_ComboBox.Name = "Answer5_ComboBox";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Name = "label8";
            // 
            // Answer6_ComboBox
            // 
            resources.ApplyResources(this.Answer6_ComboBox, "Answer6_ComboBox");
            this.Answer6_ComboBox.FormattingEnabled = true;
            this.Answer6_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer6_ComboBox.Items"),
            resources.GetString("Answer6_ComboBox.Items1"),
            resources.GetString("Answer6_ComboBox.Items2")});
            this.Answer6_ComboBox.Name = "Answer6_ComboBox";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Name = "label9";
            // 
            // Answer7_ComboBox
            // 
            resources.ApplyResources(this.Answer7_ComboBox, "Answer7_ComboBox");
            this.Answer7_ComboBox.FormattingEnabled = true;
            this.Answer7_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer7_ComboBox.Items"),
            resources.GetString("Answer7_ComboBox.Items1"),
            resources.GetString("Answer7_ComboBox.Items2")});
            this.Answer7_ComboBox.Name = "Answer7_ComboBox";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Name = "label10";
            // 
            // Answer8_ComboBox
            // 
            resources.ApplyResources(this.Answer8_ComboBox, "Answer8_ComboBox");
            this.Answer8_ComboBox.FormattingEnabled = true;
            this.Answer8_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer8_ComboBox.Items"),
            resources.GetString("Answer8_ComboBox.Items1"),
            resources.GetString("Answer8_ComboBox.Items2")});
            this.Answer8_ComboBox.Name = "Answer8_ComboBox";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Name = "label11";
            // 
            // Answer9_ComboBox
            // 
            resources.ApplyResources(this.Answer9_ComboBox, "Answer9_ComboBox");
            this.Answer9_ComboBox.FormattingEnabled = true;
            this.Answer9_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer9_ComboBox.Items"),
            resources.GetString("Answer9_ComboBox.Items1"),
            resources.GetString("Answer9_ComboBox.Items2")});
            this.Answer9_ComboBox.Name = "Answer9_ComboBox";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Name = "label12";
            // 
            // Answer10_ComboBox
            // 
            this.Answer10_ComboBox.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.Answer10_ComboBox, "Answer10_ComboBox");
            this.Answer10_ComboBox.FormattingEnabled = true;
            this.Answer10_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer10_ComboBox.Items"),
            resources.GetString("Answer10_ComboBox.Items1"),
            resources.GetString("Answer10_ComboBox.Items2")});
            this.Answer10_ComboBox.Name = "Answer10_ComboBox";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Name = "label14";
            // 
            // Answer11_ComboBox
            // 
            resources.ApplyResources(this.Answer11_ComboBox, "Answer11_ComboBox");
            this.Answer11_ComboBox.FormattingEnabled = true;
            this.Answer11_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer11_ComboBox.Items"),
            resources.GetString("Answer11_ComboBox.Items1"),
            resources.GetString("Answer11_ComboBox.Items2")});
            this.Answer11_ComboBox.Name = "Answer11_ComboBox";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Name = "label15";
            // 
            // Answer12_ComboBox
            // 
            resources.ApplyResources(this.Answer12_ComboBox, "Answer12_ComboBox");
            this.Answer12_ComboBox.FormattingEnabled = true;
            this.Answer12_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer12_ComboBox.Items"),
            resources.GetString("Answer12_ComboBox.Items1"),
            resources.GetString("Answer12_ComboBox.Items2")});
            this.Answer12_ComboBox.Name = "Answer12_ComboBox";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Name = "label16";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Name = "label17";
            // 
            // Answer13_ComboBox
            // 
            resources.ApplyResources(this.Answer13_ComboBox, "Answer13_ComboBox");
            this.Answer13_ComboBox.FormattingEnabled = true;
            this.Answer13_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer13_ComboBox.Items"),
            resources.GetString("Answer13_ComboBox.Items1"),
            resources.GetString("Answer13_ComboBox.Items2")});
            this.Answer13_ComboBox.Name = "Answer13_ComboBox";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Name = "label18";
            // 
            // Answer14_ComboBox
            // 
            resources.ApplyResources(this.Answer14_ComboBox, "Answer14_ComboBox");
            this.Answer14_ComboBox.FormattingEnabled = true;
            this.Answer14_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer14_ComboBox.Items"),
            resources.GetString("Answer14_ComboBox.Items1"),
            resources.GetString("Answer14_ComboBox.Items2"),
            resources.GetString("Answer14_ComboBox.Items3")});
            this.Answer14_ComboBox.Name = "Answer14_ComboBox";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Name = "label19";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Name = "label20";
            // 
            // Answer15_ComboBox
            // 
            resources.ApplyResources(this.Answer15_ComboBox, "Answer15_ComboBox");
            this.Answer15_ComboBox.FormattingEnabled = true;
            this.Answer15_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer15_ComboBox.Items"),
            resources.GetString("Answer15_ComboBox.Items1"),
            resources.GetString("Answer15_ComboBox.Items2")});
            this.Answer15_ComboBox.Name = "Answer15_ComboBox";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Name = "label21";
            // 
            // Answer16_ComboBox
            // 
            resources.ApplyResources(this.Answer16_ComboBox, "Answer16_ComboBox");
            this.Answer16_ComboBox.FormattingEnabled = true;
            this.Answer16_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer16_ComboBox.Items"),
            resources.GetString("Answer16_ComboBox.Items1"),
            resources.GetString("Answer16_ComboBox.Items2")});
            this.Answer16_ComboBox.Name = "Answer16_ComboBox";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Name = "label22";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Name = "label23";
            // 
            // Answer17_ComboBox
            // 
            resources.ApplyResources(this.Answer17_ComboBox, "Answer17_ComboBox");
            this.Answer17_ComboBox.FormattingEnabled = true;
            this.Answer17_ComboBox.Items.AddRange(new object[] {
            resources.GetString("Answer17_ComboBox.Items"),
            resources.GetString("Answer17_ComboBox.Items1"),
            resources.GetString("Answer17_ComboBox.Items2")});
            this.Answer17_ComboBox.Name = "Answer17_ComboBox";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Name = "label24";
            // 
            // Check_Button
            // 
            resources.ApplyResources(this.Check_Button, "Check_Button");
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            resources.ApplyResources(this.Back_Button, "Back_Button");
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // GExercise3
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Answer17_ComboBox);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.Answer16_ComboBox);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Answer15_ComboBox);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Answer14_ComboBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.Answer13_ComboBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Answer12_ComboBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Answer11_ComboBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Answer10_ComboBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Answer9_ComboBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Answer8_ComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Answer7_ComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Answer6_ComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Answer5_ComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Answer4_ComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Answer3_ComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Answer2_ComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Answer1_ComboBox);
            this.Controls.Add(this.Text_Label);
            this.Controls.Add(this.Task_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GExercise3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Task_Label;
        private System.Windows.Forms.Label Text_Label;
        private System.Windows.Forms.ComboBox Answer1_ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Answer2_ComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Answer3_ComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Answer4_ComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox Answer5_ComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox Answer6_ComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Answer7_ComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Answer8_ComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Answer9_ComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox Answer10_ComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox Answer11_ComboBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox Answer12_ComboBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox Answer13_ComboBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox Answer14_ComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox Answer15_ComboBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox Answer16_ComboBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox Answer17_ComboBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
    }
}