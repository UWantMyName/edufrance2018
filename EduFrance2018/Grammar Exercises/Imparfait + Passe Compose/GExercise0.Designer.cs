﻿namespace EduFrance2018
{
    partial class GExercise0
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GExercise0));
            this.Task_Label = new System.Windows.Forms.Label();
            this.Phrase1_Label = new System.Windows.Forms.Label();
            this.Phrase2_Label = new System.Windows.Forms.Label();
            this.Phrase3_Label = new System.Windows.Forms.Label();
            this.Phrase4_Label = new System.Windows.Forms.Label();
            this.Phrase5_Label = new System.Windows.Forms.Label();
            this.Phrase6_Label = new System.Windows.Forms.Label();
            this.Phrase7_Label = new System.Windows.Forms.Label();
            this.Phrase8_Label = new System.Windows.Forms.Label();
            this.Phrase9_Label = new System.Windows.Forms.Label();
            this.Phrase10_Label = new System.Windows.Forms.Label();
            this.Options1_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options2_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options3_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options4_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options5_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options6_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options7_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options8_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options9_ComboBox = new System.Windows.Forms.ComboBox();
            this.Options10_ComboBox = new System.Windows.Forms.ComboBox();
            this.Check_Button = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.Continue_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Task_Label
            // 
            this.Task_Label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Task_Label.AutoSize = true;
            this.Task_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Underline);
            this.Task_Label.Location = new System.Drawing.Point(12, 9);
            this.Task_Label.Name = "Task_Label";
            this.Task_Label.Size = new System.Drawing.Size(795, 54);
            this.Task_Label.TabIndex = 1;
            this.Task_Label.Text = "Complétez chaque phrase avec les valeurs correspondantes de l\'imparfait \r\net du p" +
    "assé composé";
            this.Task_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Phrase1_Label
            // 
            this.Phrase1_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase1_Label.AutoSize = true;
            this.Phrase1_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase1_Label.Location = new System.Drawing.Point(12, 100);
            this.Phrase1_Label.Name = "Phrase1_Label";
            this.Phrase1_Label.Size = new System.Drawing.Size(352, 21);
            this.Phrase1_Label.TabIndex = 2;
            this.Phrase1_Label.Text = "1) \"Je voulais devenir un acteur célèbre.\"";
            // 
            // Phrase2_Label
            // 
            this.Phrase2_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase2_Label.AutoSize = true;
            this.Phrase2_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase2_Label.Location = new System.Drawing.Point(12, 140);
            this.Phrase2_Label.Name = "Phrase2_Label";
            this.Phrase2_Label.Size = new System.Drawing.Size(443, 21);
            this.Phrase2_Label.TabIndex = 3;
            this.Phrase2_Label.Text = "2) \"J\'ai voulu lui rendre visite la semaine dernière.\"";
            // 
            // Phrase3_Label
            // 
            this.Phrase3_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase3_Label.AutoSize = true;
            this.Phrase3_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase3_Label.Location = new System.Drawing.Point(12, 180);
            this.Phrase3_Label.Name = "Phrase3_Label";
            this.Phrase3_Label.Size = new System.Drawing.Size(461, 21);
            this.Phrase3_Label.TabIndex = 4;
            this.Phrase3_Label.Text = "3) \"Je ne suis jamais resté au bureau après 18 heures.\"";
            // 
            // Phrase4_Label
            // 
            this.Phrase4_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase4_Label.AutoSize = true;
            this.Phrase4_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase4_Label.Location = new System.Drawing.Point(12, 220);
            this.Phrase4_Label.Name = "Phrase4_Label";
            this.Phrase4_Label.Size = new System.Drawing.Size(474, 21);
            this.Phrase4_Label.TabIndex = 5;
            this.Phrase4_Label.Text = "4) \"Je ne restais jamais longtemps à discuter avec eux.\"";
            // 
            // Phrase5_Label
            // 
            this.Phrase5_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase5_Label.AutoSize = true;
            this.Phrase5_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase5_Label.Location = new System.Drawing.Point(12, 260);
            this.Phrase5_Label.Name = "Phrase5_Label";
            this.Phrase5_Label.Size = new System.Drawing.Size(548, 21);
            this.Phrase5_Label.TabIndex = 6;
            this.Phrase5_Label.Text = "5) \"Rafael Nadal a gagné le tournois de Roland Garros en 2014.\"";
            // 
            // Phrase6_Label
            // 
            this.Phrase6_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase6_Label.AutoSize = true;
            this.Phrase6_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase6_Label.Location = new System.Drawing.Point(12, 300);
            this.Phrase6_Label.Name = "Phrase6_Label";
            this.Phrase6_Label.Size = new System.Drawing.Size(507, 21);
            this.Phrase6_Label.TabIndex = 7;
            this.Phrase6_Label.Text = "6) \"Il gagnait tous les matchs avant de se blesser au genou.\"";
            // 
            // Phrase7_Label
            // 
            this.Phrase7_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase7_Label.AutoSize = true;
            this.Phrase7_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase7_Label.Location = new System.Drawing.Point(12, 340);
            this.Phrase7_Label.Name = "Phrase7_Label";
            this.Phrase7_Label.Size = new System.Drawing.Size(324, 21);
            this.Phrase7_Label.TabIndex = 8;
            this.Phrase7_Label.Text = "7) \"Il était à Londres pour ses études.\"";
            // 
            // Phrase8_Label
            // 
            this.Phrase8_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase8_Label.AutoSize = true;
            this.Phrase8_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase8_Label.Location = new System.Drawing.Point(12, 380);
            this.Phrase8_Label.Name = "Phrase8_Label";
            this.Phrase8_Label.Size = new System.Drawing.Size(490, 21);
            this.Phrase8_Label.TabIndex = 9;
            this.Phrase8_Label.Text = "8) \"Il a été déçu de ne pas pouvoir rester plus longtemps.\"";
            // 
            // Phrase9_Label
            // 
            this.Phrase9_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase9_Label.AutoSize = true;
            this.Phrase9_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase9_Label.Location = new System.Drawing.Point(12, 420);
            this.Phrase9_Label.Name = "Phrase9_Label";
            this.Phrase9_Label.Size = new System.Drawing.Size(400, 21);
            this.Phrase9_Label.TabIndex = 10;
            this.Phrase9_Label.Text = "9) \"Il a décidé de partir vivre à la campagne.\"";
            // 
            // Phrase10_Label
            // 
            this.Phrase10_Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Phrase10_Label.AutoSize = true;
            this.Phrase10_Label.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Phrase10_Label.Location = new System.Drawing.Point(12, 460);
            this.Phrase10_Label.Name = "Phrase10_Label";
            this.Phrase10_Label.Size = new System.Drawing.Size(434, 21);
            this.Phrase10_Label.TabIndex = 11;
            this.Phrase10_Label.Text = "10) \"Elle décidait pour toute la famille en général.\"";
            // 
            // Options1_ComboBox
            // 
            this.Options1_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options1_ComboBox.FormattingEnabled = true;
            this.Options1_ComboBox.Items.AddRange(new object[] {
            "Situation passée qui n\'existe plus.",
            "Action ponctuelle dans le passé."});
            this.Options1_ComboBox.Location = new System.Drawing.Point(601, 100);
            this.Options1_ComboBox.Name = "Options1_ComboBox";
            this.Options1_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options1_ComboBox.TabIndex = 12;
            // 
            // Options2_ComboBox
            // 
            this.Options2_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options2_ComboBox.FormattingEnabled = true;
            this.Options2_ComboBox.Items.AddRange(new object[] {
            "Situation passée qui n\'existe plus.",
            "Action ponctuelle dans le passé."});
            this.Options2_ComboBox.Location = new System.Drawing.Point(601, 142);
            this.Options2_ComboBox.Name = "Options2_ComboBox";
            this.Options2_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options2_ComboBox.TabIndex = 13;
            // 
            // Options3_ComboBox
            // 
            this.Options3_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options3_ComboBox.FormattingEnabled = true;
            this.Options3_ComboBox.Items.AddRange(new object[] {
            "Action réalisée ou non réalisée dans le passé.",
            "Habitude dans le passé."});
            this.Options3_ComboBox.Location = new System.Drawing.Point(601, 180);
            this.Options3_ComboBox.Name = "Options3_ComboBox";
            this.Options3_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options3_ComboBox.TabIndex = 14;
            // 
            // Options4_ComboBox
            // 
            this.Options4_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options4_ComboBox.FormattingEnabled = true;
            this.Options4_ComboBox.Items.AddRange(new object[] {
            "Action réalisée ou non réalisée dans le passé.",
            "Habitude dans le passé."});
            this.Options4_ComboBox.Location = new System.Drawing.Point(601, 220);
            this.Options4_ComboBox.Name = "Options4_ComboBox";
            this.Options4_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options4_ComboBox.TabIndex = 15;
            // 
            // Options5_ComboBox
            // 
            this.Options5_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options5_ComboBox.FormattingEnabled = true;
            this.Options5_ComboBox.Items.AddRange(new object[] {
            "Événement passé.",
            "Situation passée qui a cessé d\'exister."});
            this.Options5_ComboBox.Location = new System.Drawing.Point(601, 262);
            this.Options5_ComboBox.Name = "Options5_ComboBox";
            this.Options5_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options5_ComboBox.TabIndex = 16;
            // 
            // Options6_ComboBox
            // 
            this.Options6_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options6_ComboBox.FormattingEnabled = true;
            this.Options6_ComboBox.Items.AddRange(new object[] {
            "Événement passé.",
            "Situation passée qui a cessé d\'exister."});
            this.Options6_ComboBox.Location = new System.Drawing.Point(601, 300);
            this.Options6_ComboBox.Name = "Options6_ComboBox";
            this.Options6_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options6_ComboBox.TabIndex = 17;
            // 
            // Options7_ComboBox
            // 
            this.Options7_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options7_ComboBox.FormattingEnabled = true;
            this.Options7_ComboBox.Items.AddRange(new object[] {
            "Contexte d\'une histoire du passé.",
            "Action ponctuelle dans le passé."});
            this.Options7_ComboBox.Location = new System.Drawing.Point(601, 342);
            this.Options7_ComboBox.Name = "Options7_ComboBox";
            this.Options7_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options7_ComboBox.TabIndex = 18;
            // 
            // Options8_ComboBox
            // 
            this.Options8_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options8_ComboBox.FormattingEnabled = true;
            this.Options8_ComboBox.Items.AddRange(new object[] {
            "Contexte d\'une histoire du passé.",
            "Action ponctuelle dans le passé."});
            this.Options8_ComboBox.Location = new System.Drawing.Point(601, 380);
            this.Options8_ComboBox.Name = "Options8_ComboBox";
            this.Options8_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options8_ComboBox.TabIndex = 19;
            // 
            // Options9_ComboBox
            // 
            this.Options9_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options9_ComboBox.FormattingEnabled = true;
            this.Options9_ComboBox.Items.AddRange(new object[] {
            "Action passée qui modifie une situation.",
            "Habitude dans le passé."});
            this.Options9_ComboBox.Location = new System.Drawing.Point(601, 422);
            this.Options9_ComboBox.Name = "Options9_ComboBox";
            this.Options9_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options9_ComboBox.TabIndex = 20;
            // 
            // Options10_ComboBox
            // 
            this.Options10_ComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Options10_ComboBox.FormattingEnabled = true;
            this.Options10_ComboBox.Items.AddRange(new object[] {
            "Action réalisée qui modifie une situation.",
            "Habitude dans le passé."});
            this.Options10_ComboBox.Location = new System.Drawing.Point(601, 460);
            this.Options10_ComboBox.Name = "Options10_ComboBox";
            this.Options10_ComboBox.Size = new System.Drawing.Size(216, 21);
            this.Options10_ComboBox.TabIndex = 21;
            // 
            // Check_Button
            // 
            this.Check_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Check_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Check_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Check_Button.Location = new System.Drawing.Point(569, 505);
            this.Check_Button.Name = "Check_Button";
            this.Check_Button.Size = new System.Drawing.Size(113, 27);
            this.Check_Button.TabIndex = 22;
            this.Check_Button.Text = "Vérifier";
            this.Check_Button.UseVisualStyleBackColor = true;
            this.Check_Button.Click += new System.EventHandler(this.Check_Button_Click);
            // 
            // Back_Button
            // 
            this.Back_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Back_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Back_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Back_Button.Location = new System.Drawing.Point(704, 505);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(113, 27);
            this.Back_Button.TabIndex = 23;
            this.Back_Button.Text = "Retournez";
            this.Back_Button.UseVisualStyleBackColor = true;
            this.Back_Button.Click += new System.EventHandler(this.Back_Click);
            // 
            // Continue_Button
            // 
            this.Continue_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Continue_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Continue_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Continue_Button.Font = new System.Drawing.Font("Lucida Calligraphy", 12F);
            this.Continue_Button.Location = new System.Drawing.Point(425, 505);
            this.Continue_Button.Name = "Continue_Button";
            this.Continue_Button.Size = new System.Drawing.Size(113, 27);
            this.Continue_Button.TabIndex = 91;
            this.Continue_Button.Text = "Continuez";
            this.Continue_Button.UseVisualStyleBackColor = true;
            this.Continue_Button.Visible = false;
            this.Continue_Button.Click += new System.EventHandler(this.Continue_Button_Click);
            // 
            // GExercise0
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.ClientSize = new System.Drawing.Size(829, 544);
            this.Controls.Add(this.Continue_Button);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.Check_Button);
            this.Controls.Add(this.Options10_ComboBox);
            this.Controls.Add(this.Options9_ComboBox);
            this.Controls.Add(this.Options8_ComboBox);
            this.Controls.Add(this.Options7_ComboBox);
            this.Controls.Add(this.Options6_ComboBox);
            this.Controls.Add(this.Options5_ComboBox);
            this.Controls.Add(this.Options4_ComboBox);
            this.Controls.Add(this.Options3_ComboBox);
            this.Controls.Add(this.Options2_ComboBox);
            this.Controls.Add(this.Options1_ComboBox);
            this.Controls.Add(this.Phrase10_Label);
            this.Controls.Add(this.Phrase9_Label);
            this.Controls.Add(this.Phrase8_Label);
            this.Controls.Add(this.Phrase7_Label);
            this.Controls.Add(this.Phrase6_Label);
            this.Controls.Add(this.Phrase5_Label);
            this.Controls.Add(this.Phrase4_Label);
            this.Controls.Add(this.Phrase3_Label);
            this.Controls.Add(this.Phrase2_Label);
            this.Controls.Add(this.Phrase1_Label);
            this.Controls.Add(this.Task_Label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GExercise0";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercice pour approfondir la théorie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Task_Label;
        private System.Windows.Forms.Label Phrase1_Label;
        private System.Windows.Forms.Label Phrase2_Label;
        private System.Windows.Forms.Label Phrase3_Label;
        private System.Windows.Forms.Label Phrase4_Label;
        private System.Windows.Forms.Label Phrase5_Label;
        private System.Windows.Forms.Label Phrase6_Label;
        private System.Windows.Forms.Label Phrase7_Label;
        private System.Windows.Forms.Label Phrase8_Label;
        private System.Windows.Forms.Label Phrase9_Label;
        private System.Windows.Forms.Label Phrase10_Label;
        private System.Windows.Forms.ComboBox Options1_ComboBox;
        private System.Windows.Forms.ComboBox Options2_ComboBox;
        private System.Windows.Forms.ComboBox Options3_ComboBox;
        private System.Windows.Forms.ComboBox Options4_ComboBox;
        private System.Windows.Forms.ComboBox Options5_ComboBox;
        private System.Windows.Forms.ComboBox Options6_ComboBox;
        private System.Windows.Forms.ComboBox Options7_ComboBox;
        private System.Windows.Forms.ComboBox Options8_ComboBox;
        private System.Windows.Forms.ComboBox Options9_ComboBox;
        private System.Windows.Forms.ComboBox Options10_ComboBox;
        private System.Windows.Forms.Button Check_Button;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button Continue_Button;
    }
}