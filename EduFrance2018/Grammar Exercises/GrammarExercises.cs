﻿using EduFrance2018.Grammar_Exercises.Reported_Speech;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EduFrance2018
{
    public partial class GrammarExercises : Form
    {
        public GrammarExercises()
        {
            InitializeComponent();
        }

        private void Verbs_Label_MouseEnter(object sender, EventArgs e)
        {
            Verbs_Label.ForeColor = Color.Blue;
            Verbs_Label.Font = new Font(Verbs_Label.Font, FontStyle.Bold);
        }

        private void Verbs_Label_MouseLeave(object sender, EventArgs e)
        {
            Verbs_Label.ForeColor = Color.Black;
            Verbs_Label.Font = new Font(Verbs_Label.Font, FontStyle.Regular);
        }

        private void ReportedSpeech_Label_MouseLeave(object sender, EventArgs e)
        {
            ReportedSpeech_Label.ForeColor = Color.Black;
            ReportedSpeech_Label.Font = new Font(ReportedSpeech_Label.Font, FontStyle.Regular);
        }

        private void ReportedSpeech_Label_MouseEnter(object sender, EventArgs e)
        {
            ReportedSpeech_Label.ForeColor = Color.Blue;
            ReportedSpeech_Label.Font = new Font(ReportedSpeech_Label.Font, FontStyle.Bold);
        }

        private void Verbs_Label_Click(object sender, EventArgs e)
        {
            ImparfaitPC f = new ImparfaitPC();
            f.Show();
            this.Close();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu_Form f = new MainMenu_Form();
            f.Show();
        }

        private void ReportedSpeech_Label_Click(object sender, EventArgs e)
        {
            ReportedSpeech f = new ReportedSpeech();
            f.Show();
            this.Hide();
        }
    }
}
